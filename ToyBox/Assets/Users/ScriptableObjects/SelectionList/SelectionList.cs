using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects.SelectionList
{
    /// <summary>
    /// 選択肢データのエントリ名リストScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public class SelectionList : ScriptableObject
    {
        // 選択肢データのエントリ名リスト
        [SerializeField]
        private List<string> _selectionNameList;

        public List<string> SelectionNameList
        {
            get { return _selectionNameList; }

#if UNITY_EDITOR
            set { _selectionNameList = value; }
#endif
        }
    }
}
using System;
using Parameter;
using UnityEngine;

namespace ScriptableObjects.SelectionData
{
    /// <summary>
    /// 選択肢データのScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [Serializable]
    public sealed class SelectionData : ScriptableObject,IData
    {
        // 選択肢によるパラメーター変動値
        [Serializable]
        public sealed class SelectionFluctuationValue
        {
            // パラメーター変動値のタイプ
            [Serializable]
            public enum ValueType
            {
                [InspectorName("固定値")]
                Fixed,
                [InspectorName("割合(%)")]
                Percentage
            }

            [SerializeField, Header("変動値のタイプ")]
            private ValueType _valueType = ValueType.Fixed;

            [SerializeField, Header("変動値")]
            private int _value = 0;

            public ValueType Type => _valueType;

            public int Value => _value;
        }

        [Serializable]
        public sealed class SelectionFluctuationValueDictionary : SerializableDictionary<ParameterType, SelectionFluctuationValue>
        {
        }

        // デフォルトの選択肢テキスト
        private static readonly string _defaultEventName = "新しい選択肢";

        // 選択肢テキスト
        [SerializeField, Header("選択肢テキスト")]
        private string _selectionText = _defaultEventName;

        // パラメーター変化値
        [SerializeField, Header("パラメーター変化値")]
        private SelectionFluctuationValueDictionary _parameterIncreaseValue = new SelectionFluctuationValueDictionary();

        public string DataName => _selectionText;

        public UnityEngine.Object Object => this;

        public string SelectionText
        {
            get { return _selectionText; }

#if UNITY_EDITOR
            set { _selectionText = value; }
#endif
        }

        public SelectionFluctuationValueDictionary ParameterIncreaseValue
        {
            get { return _parameterIncreaseValue; }

#if UNITY_EDITOR
            set { _parameterIncreaseValue = value; }
#endif
        }

        private void OnEnable()
        {
            // 直接編集できないようにする
            hideFlags = HideFlags.NotEditable;
        }
    }
}

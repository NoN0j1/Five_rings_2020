using UnityEngine;

namespace ScriptableObjects
{
    /// <summary>
    /// データのインターフェイス<br/>
    /// 制作者：宮川
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// データ名
        /// </summary>
        public string DataName { get; }

        public Object Object { get; }
    }
}

namespace ScriptableObjects
{
    /// <summary>
    /// データのAddressableグループ名の定数クラス
    /// 制作者：宮川
    /// </summary>
    public static class DataAddressablesGroupName
    {
        // イベントデータのAddressableグループ名
        private static readonly string _eventDataGroupName = "EventDataGroup";

        // 選択肢データのAddressableグループ名
        private static readonly string _selectionDataGroupName = "SelectionDataGroup";

        // 目標イベントデータのAddressableグループ名
        private static readonly string _targetEventDataGroupName = "TargetEventDataGroup";

        public static string EventDataGroupName => _eventDataGroupName;

        public static string SelectionDataGroupName => _selectionDataGroupName;

        public static string TargetEventDataGroup => _targetEventDataGroupName;
    }
}
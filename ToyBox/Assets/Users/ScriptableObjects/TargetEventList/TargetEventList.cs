using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects.TargetEventList
{
    /// <summary>
    /// 目標イベントデータのエントリ名リストScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public class TargetEventList : ScriptableObject
    {
        // 目標イベントデータのエントリ名リスト
        [SerializeField]
        private List<string> _targetEventNameList;

        public List<string> TargetEventNameList
        {
            get { return _targetEventNameList; }

#if UNITY_EDITOR
            set { _targetEventNameList = value; }
#endif
        }
    }
}
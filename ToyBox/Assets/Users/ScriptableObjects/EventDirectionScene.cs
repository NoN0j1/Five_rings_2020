using System;
using Attribute;
using UnityEngine;

namespace ScriptableObjects
{
    // イベント演出シーン名
    [Serializable]
    public sealed class EventDirectionScene
    {
        // 背景シーン名
        [SerializeField, DirectionScenePopup("背景シーン名", "BackgroundSceneGroup")]
        private string _background = "Dummy";

        // 人物シーン名
        [SerializeField, DirectionScenePopup("人物シーン名", "HumanSceneGroup")]
        private string _foreground = "Dummy";

        // 手前の小物シーン名
        [SerializeField, DirectionScenePopup("手前の小物シーン名", "ForegroundSceneGroup")]
        private string _effectDirection = "Dummy";

        public string Background
        {
            get { return _background; }
            set { _background = value; }
        }

        public string Foreground
        {
            get { return _foreground; }
            set { _foreground = value; }
        }

        public string EffectDirection
        {
            get { return _effectDirection; }
            set { _effectDirection = value; }
        }
    }
}
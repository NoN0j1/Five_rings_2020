using System;
using System.Collections.Generic;
using Parameter;
using UnityEngine;

namespace ScriptableObjects.TargetData
{
    /// <summary>
    /// 目標イベントデータのScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [Serializable]
    public sealed class TargetEventData : ScriptableObject,IData
    {
        [Serializable]
        public sealed class TargetParameterDictionary : SerializableDictionary<ParameterType, int>
        {
        }

        [SerializeField, Header("目標イベント名")]
        private string _targetName = "新しい目標";

        [SerializeField, Header("目標イベント開始テキスト")]
        private List<string> _targetTextList = new List<string>();

        [SerializeField, Header("目標イベント成功テキスト")]
        private List<string> _successTextList = new List<string>();

        [SerializeField, Header("目標イベント失敗テキスト")]
        private List<string> _failureTextList = new List<string>();

        // 最大ターン数
        private const int _turnMax = 48;

        [SerializeField, Header("目標ターン"),Range(0, _turnMax)]
        private int _targetTurn = 0;

        [SerializeField, Header("目標パラメーター")]
        private TargetParameterDictionary _targetParameter = new TargetParameterDictionary();

        [SerializeField, Header("演出シーン")]
        private EventDirectionScene _eventDirectionSceneName;

        public string DataName => _targetName;

        public UnityEngine.Object Object => this;

        public List<string> TextList
        {
            get { return _targetTextList; }

#if UNITY_EDITOR
            set { _targetTextList = value; }
#endif
        }

        public List<string> SuccessTextList
        {
            get { return _successTextList; }

#if UNITY_EDITOR
            set { _successTextList = value; }
#endif
        }

        public List<string> FailureTextList
        {
            get { return _failureTextList; }

#if UNITY_EDITOR
            set { _failureTextList = value; }
#endif
        }

        public int Turn
        {
            get { return _targetTurn; }

#if UNITY_EDITOR
            set { _targetTurn = value; }
#endif
        }

        public TargetParameterDictionary Parameter
        {
            get { return _targetParameter; }

#if UNITY_EDITOR
            set { _targetParameter = value; }
#endif
        }

        public EventDirectionScene EventDirectionScene
        {
            get { return _eventDirectionSceneName; }

#if UNITY_EDITOR
            set { _eventDirectionSceneName = value; }
#endif
        }

        private void OnEnable()
        {
            // 直接編集できないようにする
            hideFlags = HideFlags.NotEditable;
        }
    }
}
using System;
using System.Collections.Generic;
using ScriptableObjects.EventData;
using UnityEngine;

namespace ScriptableObjects.EventTable
{
    /// <summary>
    /// イベントデータリストのScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [CreateAssetMenu(fileName = "EventTable", menuName = "ScriptableObjects/EventTable")]
    [Serializable]
    public sealed class EventTable : ScriptableObject
    {
        [Serializable]
        public sealed class EventDataListStorage : SerializableDictionary.Storage<List<EventData.EventData>>
        {
        }

        [Serializable]
        public sealed class ListByEventTagDictionary : SerializableDictionary<EventTag, List<EventData.EventData>,EventDataListStorage>
        {
        }

        // イベントタグ別のリスト
        [SerializeField, Header("イベントタグ別のリスト")]
        private ListByEventTagDictionary _listByTag = new ListByEventTagDictionary();

        // イベントデータのエントリ名リスト
        [SerializeField]
        private List<string> _eventNameList;

        public ListByEventTagDictionary ListByTag
        {
            get { return _listByTag; }

#if UNITY_EDITOR
            set { _listByTag = value; }
#endif
        }

        public List<string> EventNameList
        {
            get { return _eventNameList; }

#if UNITY_EDITOR
            set { _eventNameList = value; }
#endif
        }

        public void Awake()
        {
            SetingListByTagKey();
        }

#if UNITY_EDITOR
        /// <summary>
        /// イベントタグ別のリストをクリア
        /// </summary>
        public void ClearListByTag()
        {
            SetingListByTagKey();

            foreach (var list in _listByTag)
            {
                list.Value.Clear();
            }
        }
#endif

        /// <summary>
        /// イベントタグ別のリストのキーを設定
        /// </summary>
        private void SetingListByTagKey()
        {
            foreach (EventTag tag in Enum.GetValues(typeof(EventTag)))
            {
                if (!_listByTag.ContainsKey(tag))
                {
                    _listByTag.Add(tag, new List<EventData.EventData>());
                }
            }
        }
    }
}
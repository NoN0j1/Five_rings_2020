using System;
using System.Collections.Generic;
using Attribute;
using UnityEngine;

namespace ScriptableObjects.EventData
{
    [Serializable]
    // イベントタグ
    public enum EventTag
    {
        [InspectorName("無し")]
        None = 0,
        [InspectorName("序盤")]
        Opening = 1 << 0,
        [InspectorName("中盤")]
        Middle = 1 << 1,
        [InspectorName("終盤")]
        Final = 1 << 2,
        [InspectorName("汎用")]
        GeneralPurpose = Opening | Middle | Final,
    }

    /// <summary>
    /// イベントデータのScriptableObjectクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [Serializable]
    public sealed class EventData : ScriptableObject, IData
    {
        // デフォルトのイベント名
        private static readonly string _defaultEventName = "新しいイベント";

        // イベントタグ（英語）
        [SerializeField, Header("イベントタグ"),EventTag]
        private EventTag _tag = EventTag.None;

        // イベント名
        [SerializeField, Header("イベント名")]
        private string _eventName = _defaultEventName;

        // イベントテキスト
        [SerializeField, Header("イベントテキスト")]
        private List<string> _eventTextList = new List<string>();

        // 選択肢リスト
        [SerializeField, Header("選択肢リスト")]
        private List<SelectionData.SelectionData> _selectionDataList = new List<SelectionData.SelectionData>();

        // イベント演出シーン情報
        [SerializeField, Header("演出シーン")]
        private EventDirectionScene _eventDirectionSceneName;

        public string DataName => _eventName;

        public UnityEngine.Object Object => this;

        public static string DefaultEventName => _defaultEventName;

        public EventTag Tag
        {
            get { return _tag; }

#if UNITY_EDITOR
            set { _tag = value; }
#endif
        }

        public string EventName
        {
            get { return _eventName; }

#if UNITY_EDITOR
            set { _eventName = value; }
#endif
        }

        public List<string> EventTextList
        {
            get { return _eventTextList; }

#if UNITY_EDITOR
            set { _eventTextList = value; }
#endif
        }

        public List<SelectionData.SelectionData> SelectionDataList
        {
            get { return _selectionDataList; }

#if UNITY_EDITOR
            set { _selectionDataList = value; }
#endif
        }

        public EventDirectionScene EventDirectionSceneName
        {
            get { return _eventDirectionSceneName; }

#if UNITY_EDITOR
            set { _eventDirectionSceneName = value; }
#endif
        }

        private void OnEnable()
        {
            // 直接編集できないようにする
            hideFlags = HideFlags.NotEditable;
        }
    }
}

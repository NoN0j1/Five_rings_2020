using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace TouchEffect
{
    /// <summary>
    /// タッチ演出を行うViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Image))]
    public sealed class TouchEffectView : MonoBehaviour
    {
        [SerializeField]
        private RectTransform _rectTransform = null;

        [SerializeField]
        private Image _image = null;

        [SerializeField,Header("拡大後スケール")]
        private Vector3 _endScale = new Vector3(5.0f, 5.0f, 1.0f);

        [SerializeField,Header("拡大時間")]
        private float _scaleTime = 1.0f;

        [SerializeField, Header("フェード補間用のイーズ曲線")]
        private Ease _fadeEase = Ease.InSine;

        // アニメーション制御用
        private Sequence _sequence = null;

        private void Reset()
        {
            TryGetComponent(out _rectTransform);
            TryGetComponent(out _image);
        }

        void Start()
        {
            _rectTransform.localScale = Vector3.zero;

            SettingEffect();
        }

        private void OnDestroy()
        {
            _sequence.Kill();
        }

        /// <summary>
        /// エフェクトを発生
        /// </summary>
        /// <param name="pos"> 発生座標</param>
        public void StartEffect(in Vector3 pos)
        {
            gameObject.SetActive(true);

            _rectTransform.position = pos;
            _rectTransform.localScale = Vector3.zero;

            _sequence.Restart();
        }

        /// <summary>
        /// エフェクトのアニメーションを設定
        /// </summary>
        private void SettingEffect()
        {
            _sequence = DOTween.Sequence();

            _sequence.Prepend(_rectTransform.DOScale(_endScale, _scaleTime));

            _sequence.Join(_image.DOFade(0.0f, _scaleTime).SetEase(_fadeEase));

            _sequence.OnComplete(() => { gameObject.SetActive(false); });

            _sequence.SetAutoKill(false);
        }
    }
}
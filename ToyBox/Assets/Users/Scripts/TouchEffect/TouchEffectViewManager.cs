using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace TouchEffect
{
    /// <summary>
    /// タッチ演出の管理を行うクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class TouchEffectViewManager : MonoBehaviour
    {
        // タッチエフェクトのリスト
        [SerializeField]
        private List<TouchEffectView> _touchEffectList = new List<TouchEffectView>();

        // タッチエフェクトのリストのインデックス
        private int _touchEffectIndex = 0;

        private void Start()
        {
            this.UpdateAsObservable().Where(unit => Input.GetMouseButtonDown(0))
                .Subscribe(unit => EmissionTouchEffect(GetClickPos()))
                .AddTo(this);
        }

        /// <summary>
        /// エフェクトを発生
        /// </summary>
        /// <param name="pos"> 発生座標</param>
        public void EmissionTouchEffect(in Vector3 pos)
        {
            _touchEffectList[_touchEffectIndex].StartEffect(pos);

            NextTouchEffectIndex();
        }

        /// <summary>
        /// タッチエフェクトのインデックスを更新
        /// </summary>
        private void NextTouchEffectIndex()
        {
            _touchEffectIndex++;
            _touchEffectIndex %= _touchEffectList.Count;
        }

        /// <summary>
        /// 押した座標を取得
        /// </summary>
        /// <returns> 座標</returns>
        private Vector3 GetClickPos()
        {
            Vector3 ret = Vector3.zero;
#if UNITY_ANDROID
            ret = Input.GetTouch(0).position;
#else
            ret = Input.mousePosition;
#endif
            return ret;
        }
    }
}

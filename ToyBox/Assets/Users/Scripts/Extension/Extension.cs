using System;
using System.Collections.Generic;
using Parameter;
using ScriptableObjects.EventData;

namespace Extension
{
    public static class EnumExtension
    {
        /// <summary>
        /// Enumの要素数を取得
        /// </summary>
        /// <returns> Enumの要素数</returns>
        public static int GetMax(this Enum @enum)
        {
            return Enum.GetValues(@enum.GetType()).Length;
        }
    }

    public static class EventTagExtension
    {
        /// <summary>
        /// 対象のイベントタグが含まれているかチェック
        /// </summary>
        /// <param name="eventTag"> 対象のイベントタグ</param>
        /// <returns> true : 含まれている, false : 含まれていない</returns>
        public static bool Included(this EventTag enumValue, in EventTag eventTag)
        {
            if (eventTag == 0)
            {
                return enumValue == 0;
            }

            return (enumValue & eventTag) == eventTag;
        }

        /// <summary>
        /// 要素の表示名を取得
        /// </summary>
        /// <returns> 要素の表示名</returns>
        public static string GetDisplayName(this EventTag eventTag)
        {
            Dictionary<EventTag, string> displayName = new Dictionary<EventTag, string> {
                { EventTag.None,"無し" },
                { EventTag.Opening,"序盤" },
                { EventTag.Middle,"中盤" },
                { EventTag.Final,"終盤" },
                { EventTag.GeneralPurpose,"汎用" },
            };

            return displayName[eventTag];
        }
    }

    public static class ParameterTypeExtension
    {
        /// <summary>
        /// 要素の表示名を取得
        /// </summary>
        /// <returns> 要素の表示名</returns>
        public static string GetDisplayName(this ParameterType parameter)
        {
            Dictionary<ParameterType, string> displayName = new Dictionary<ParameterType, string> {
                { ParameterType.Money,"お金" },
                { ParameterType.Influence,"影響力" },
                { ParameterType.Exitement,"期待度" },
                { ParameterType.Leadership,"統率力" },
                { ParameterType.PoliticsPower,"政治力" },
                { ParameterType.RespondLevel,"対応力" },
                { ParameterType.Morality,"モラル" },
                { ParameterType.MediaReputation,"マスコミ評価" }
            };

            return displayName[parameter];
        }
    }
}

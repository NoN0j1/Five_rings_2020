using Common;
using Cysharp.Threading.Tasks;
using TitleScene.Saving;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

// タイトルシーンの内部情報更新処理をする
namespace TitleScene.Model
{
    public sealed class TitleModel : MonoBehaviour
    {
        // スクリーン番号が変わった時のイベント
        public ScreenEvent OnScreenNumberChanged = new ScreenEvent();
        // 続きからフラグが変わった時のイベント
        public BoolEvent OnContinueFlagChanged = new BoolEvent();
        // ユーザー名が書き換わった時のイベント
        public StringEvent OnUserNameChanged = new StringEvent();

        private bool _isGameStart = false;

        /// <summary>
        /// 音声再生用
        /// </summary>
        private CriAtomSource _criAtomSource = null;

        /// <summary>
        /// フェードなど用
        /// </summary>
        private CommonSceneCanvas _sceneEffecter = null;

        // 入力されたユーザー名
        [SerializeField]
        private TMP_Text _inputNameText;

        // タイトルの各スクリーンの番号
        private SceneState _screenNumber = SceneState.Title;
        // 続きからフラグ
        private bool _continueFlag = false;   
        

        // 更新前に呼ばれるスタート処理
        private void Start()
        {
            _isGameStart = false;
            _sceneEffecter = GameObject.Find("Fade").GetComponent<CommonSceneCanvas>();
            // セーブデータがある場合は、続きからフラグを真
            if (SaveSystem.PubInstance.Load() == null)
            {
                _continueFlag = false;
                OnContinueFlagChanged.Invoke(_continueFlag);
            }
            else
            {
                _continueFlag = true;
                OnContinueFlagChanged.Invoke(_continueFlag);
            }

            _sceneEffecter.WhiteFadeIn(2);
            _criAtomSource = GetComponent<CriAtomSource>();
            _criAtomSource.volume = 0.1f;
        }

        // リターンボタンを押したときの処理
        public void ReturnScreen()
        {
            _criAtomSource.Play("システム決定音_9_3");
            switch (_screenNumber)
            {
                case SceneState.Title:
                    break;
                case SceneState.Mode:
                    _screenNumber = SceneState.Title;
                    break;
                case SceneState.Create:
                    _screenNumber = SceneState.Mode;
                    break;
                case SceneState.Start:
                    _screenNumber = SceneState.Create;
                    break;
                case SceneState.Continue:
                    _screenNumber = SceneState.Create;
                    break;
                default:
                    break;
            }
            OnScreenNumberChanged.Invoke(_screenNumber);
        }

        // シングルモードボタンの処理
        public void CreateData()
        {
            _criAtomSource.Play("button");
            _screenNumber = SceneState.Create;
            OnScreenNumberChanged.Invoke(_screenNumber);
        }

        // 続きからボタンの処理
        public void ContinueGame()
        {
            _criAtomSource.Play("button");
            /// ゲームシーンをロード
            _screenNumber = SceneState.Continue;
            Debug.Log("ロードが完了しました。");
            Debug.Log("User Name：" + SaveSystem.PubInstance.PubUserData.UserName);
            OnScreenNumberChanged.Invoke(_screenNumber);
            OnUserNameChanged.Invoke(SaveSystem.PubInstance.PubUserData.UserName);
            StartGame().Forget();
        }

        // ユーザー情報決定ボタンの処理
        public void InputName()
        {
            _criAtomSource.Play("システム決定音_9_3");
            if (_inputNameText.text.Length >= 1)
            {
                _screenNumber = SceneState.Start;
                OnUserNameChanged.Invoke(_inputNameText.text);
            }
            OnScreenNumberChanged.Invoke(_screenNumber);
        }

        // ゲーム開始ボタンの処理
        public void StartNewGame()
        {
            _criAtomSource.Play("button");
            /// ゲームシーンをロード
            SaveSystem.PubInstance.NewData();
            SaveSystem.PubInstance.PubUserData.UserName = _inputNameText.text;
            SaveSystem.PubInstance.Save();
            Debug.Log("セーブが完了しました。");
            Debug.Log("User Name：" + SaveSystem.PubInstance.PubUserData.UserName);
            StartGame().Forget();
        }

        /// <summary>
        /// ゲームスタート処理
        /// </summary>
        /// <returns></returns>
        private async UniTask StartGame()
        {
            if(_isGameStart == true)
            {
                return;
            }
            _sceneEffecter.WhiteFadeOut(1);

            while(_sceneEffecter.IsFinishFadeOut == false)
            {
                await UniTask.NextFrame();
            }
            SceneManager.LoadScene("GameScene");
        }

        // タイトル画面に振れた処理
        public void TouchTitleScreen()
        {
            if(!_sceneEffecter.IsFinishFadeIn)
            {
                return;
            }
            _criAtomSource.Play("button");
            _screenNumber = SceneState.Mode;
            OnScreenNumberChanged.Invoke(_screenNumber);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TitleScene.Saving
{
    // セーブ・ロード機能実行部
    public sealed class SaveCtl : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SaveSystem.PubInstance.PubUserData.UserName = "ウルトラマンタロウ";
                //SaveSystem.PubInstance.PubUserData.StartCnt += 1;
                SaveSystem.PubInstance.Save();
                Debug.Log("セーブが完了しました。");
            }

            if (Input.GetKeyDown(KeyCode.L))
            {
                SaveSystem.PubInstance.Load();
                Debug.Log("User Name =" + SaveSystem.PubInstance.PubUserData.UserName);
            }
        }
    }
}
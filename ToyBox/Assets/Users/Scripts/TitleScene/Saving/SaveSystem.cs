using System.IO;
using UnityEngine;

namespace TitleScene.Saving
{
    // ユーザー情報を保持するシングルトン
    public sealed class SaveSystem
    {
        #region Singleton
        private static SaveSystem _instance = new SaveSystem();
        public static SaveSystem PubInstance => _instance;
        #endregion
        private SaveSystem() { }

        // 保存先のパス
        public string Path => Application.dataPath + "/data.json";
        public string testPath => Application.dataPath + "/savedata.json";
        private UserData _userData = new UserData();
        public UserData PubUserData => _userData;

        public void NewData()
        {
            _userData = new UserData();
        }

        public void DeleteData()
        {
            if(File.Exists(Path))
            {
                File.Delete(Path);
            }
            if(File.Exists(testPath))
            {
                File.Delete(testPath);
            }
        }

        // ユーザー情報の保存処理
        public void Save()
        {
            string jsonData = JsonUtility.ToJson(_userData);
            StreamWriter writer = new StreamWriter(Path, false);
            writer.WriteLine(jsonData);
            writer.Flush();
            writer.Close();
        }

        /// <summary>
        /// ユーザー情報の読み込み処理
        /// </summary>
        /// <returns>true =読み込み成功</returns>
        public UserData Load()
        {
            if (!File.Exists(Path))
            {
                Debug.LogWarning("データがありません。");
                return null;
            }

            StreamReader reader = new StreamReader(Path);
            string jsonData = reader.ReadLine();
            _userData = JsonUtility.FromJson<UserData>(jsonData);
            reader.Close();
            return _userData;
        }
    }
}
using System;
using System.Collections.Generic;
using GameScene.Parameter;
using Parameter;
using UnityEngine;

namespace TitleScene.Saving
{
    // セーブされるユーザー情報を保持
    [System.Serializable]
    public sealed class UserData
    {
        /// <summary>
        /// 名前
        /// </summary>
        public string UserName = "NO_NAME";

        /// <summary>
        /// 現在のターン数
        /// </summary>
        public uint Turn = 0;

        /// <summary>
        /// 発生中イベント情報
        /// </summary>
        public SaveEventInfo PickedEvent = null;

        /// <summary>
        /// 現在ステータス
        /// </summary>
        public SaveParameter Parameter = new SaveParameter();

        /// <summary>
        /// 現在の目標イベントの状況
        /// </summary>
        public SaveTargetEvent TargetEventInfo = new SaveTargetEvent();

        /// <summary>
        /// もう発生しないイベント
        /// </summary>
        public List<string> IgnoreEvent = new List<string>();
    }

    /// <summary>
    /// セーブするイベントに関する情報クラス
    /// 作成者 : 川野<br/>
    /// </summary>
    [Serializable]
    public sealed class SaveEventInfo: ISerializationCallbackReceiver
    {
        /// <summary>
        /// イベント種別
        /// </summary>
        [SerializeField]
        private GameEventType _type = GameEventType.Other;

        /// <summary>
        /// ScriptableObject名
        /// </summary>
        [SerializeField]
        private string _dataName = string.Empty;

        /// <summary>
        /// イベント内の未実行関数
        /// </summary>
        [SerializeField]
        private List<string> _functionQueueData = new List<string>();

        /// <summary>
        /// 選んだ選択肢、もしくは目標イベントの結果、 -1の場合は未選択
        /// </summary>
        [SerializeField]
        private int _selection = -1;

        /// <summary>
        /// 実行関数
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// イベント種別を取得する
        /// </summary>
        public GameEventType EventType => _type;

        /// <summary>
        /// Addlessableイベントデータ名
        /// </summary>
        public string EventDataName => _dataName;

        /// <summary>
        /// そのイベント内で未実行の関数リスト
        /// </summary>
        public Queue<string> FunctionQueue => _functionQueue;

        /// <summary>
        /// 通常イベントの場合---選ばれた選択肢番号<br/>
        /// 目標イベントの場合---抽籤済みの結果<br/>
        /// エンディングの場合---エンディング番号
        /// </summary>
        public int Result => _selection;


        
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dataName">イベントデータ名</param>
        /// <param name="type">イベント種別</param>
        public SaveEventInfo(string dataName, GameEventType type)
        {
            _dataName = dataName;
            _type = type;
        }

        /// <summary>
        /// セーブする項目の更新
        /// </summary>
        public void UpdateValue(Queue<string> function, int select)
        {
            _functionQueueData.Clear();
            _functionQueueData.AddRange(function.ToArray());
            _selection = select;
        }

        /// <summary>
        /// クラスの情報からJsonデータを作成する
        /// </summary>
        public void OnBeforeSerialize()
        {
            
        }

        /// <summary>
        /// Jsonデータからクラス情報を作成する
        /// </summary>
        public void OnAfterDeserialize()
        {
            _functionQueue = new Queue<string>();
            foreach (string s in _functionQueueData)
            {
                _functionQueue.Enqueue(s);
            }
        }
    }

    /// <summary>
    /// セーブ用能力値データクラス<br/>
    /// 作成者 : 川野<br/>
    /// </summary>
    [Serializable]
    public sealed class SaveParameter :ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<ParameterType> _keys;
        [SerializeField]
        private List<int> _values;

        private Dictionary<ParameterType, int> _parameters = new Dictionary<ParameterType, int>();
        public Dictionary<ParameterType, int> Parameter => _parameters;

        /// <summary>
        /// ステータス値を更新する
        /// </summary>
        /// <param name="saveParameters">更新する値</param>
        public void UpdateValues(List<Status> saveParameters)
        {
            foreach (Status s in saveParameters)
            {
                _parameters[s.ParameterType] = s.Value;
            }
        }

        /// <summary>
        /// Jsonデータからクラス情報を作成する
        /// </summary>
        public void OnAfterDeserialize()
        {
            var count = Math.Min(_keys.Count, _values.Count);
            _parameters = new Dictionary<ParameterType, int>(count);
            for (var i = 0; i < count; ++i)
            {
                _parameters.Add(_keys[i], _values[i]);
            }
        }

        /// <summary>
        /// クラスの情報からJsonデータを作成する
        /// </summary>
        public void OnBeforeSerialize()
        {
            _keys = new List<ParameterType>(_parameters.Keys);
            _values = new List<int>(_parameters.Values);
        }

    }


    /// <summary>
    /// セーブ用目標イベントデータクラス<br/>
    /// 作成者 : 川野<br/>
    /// </summary>

    [Serializable]
    public sealed class SaveTargetEvent : ISerializationCallbackReceiver
    {
        /// <summary>
        /// 目標イベント1つずつの情報
        /// </summary>
        public sealed class TargetEventInfo
        {
            private string _eventName = string.Empty;
            private TargetEventStatus _eventStatus = TargetEventStatus.NotStart;
            public string EventName => _eventName;
            public TargetEventStatus EventStatus => _eventStatus;

            public TargetEventInfo(string name, TargetEventStatus status)
            {
                _eventName = name;
                _eventStatus = status;
            }
        }
        [SerializeField]
        private List<string> _eventKeys;
        [SerializeField]
        private List<TargetEventStatus> _statuses;

        private List<TargetEventInfo> _infoList = new List<TargetEventInfo>();

        /// <summary>
        /// 目標イベント情報リスト
        /// </summary>
        public List<TargetEventInfo> EventList => _infoList;

        public void UpdateEventParameter(List<string> eventNames, List<TargetEventStatus> eventStatus)
        {
            _eventKeys = eventNames;
            _statuses = eventStatus;
            Convert();
        }

        /// <summary>
        /// Jsonデータからクラス情報を作成する
        /// </summary>
        public void OnAfterDeserialize()
        {
            Convert();
        }

        /// <summary>
        /// クラスの情報からJsonデータを作成する
        /// </summary>
        public void OnBeforeSerialize()
        {
            _eventKeys = new List<string>();
            _statuses = new List<TargetEventStatus>();
            foreach (var s in _infoList)
            {
                _eventKeys.Add(s.EventName);
                _statuses.Add(s.EventStatus);
            }
        }

        /// <summary>
        /// keyとstatusからイベントリストに変換する
        /// </summary>
        private void Convert()
        {
            _infoList = new List<TargetEventInfo>();
            var count = Math.Min(_eventKeys.Count, _statuses.Count);
            for (var i = 0; i < count; ++i)
            {
                _infoList.Add(new TargetEventInfo(_eventKeys[i], _statuses[i]));
            }
        }
    }
}
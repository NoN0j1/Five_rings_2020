using TitleScene.ViewModel;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// 新規スタート画面の描画処理
namespace TitleScene.View
{
    public sealed class StartScreenView : MonoBehaviour
    {
        // 始めるボタン
        [SerializeField]
        private Button _startButton;

        // ユーザー名
        [SerializeField]
        private TMP_Text _nameText;

        // スタートボタンが押された情報
        public Button.ButtonClickedEvent IsStart => _startButton.onClick;

        // 入力されたユーザー名を描画に反映
        public void ControlStartScreen(string name)
        {
            _nameText.text = name;
        }
    }
}

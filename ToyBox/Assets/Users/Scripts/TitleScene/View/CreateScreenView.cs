using UnityEngine;
using UnityEngine.UI;
using TMPro;

// ユーザーデータ作成画面の描画処理
public sealed class CreateScreenView : MonoBehaviour
{
    // 決定ボタン
    [SerializeField]
    private Button _goButton;
    // ユーザー名入力バー
    [SerializeField]
    private TMP_InputField _inputNameField;

    // 決定ボタンが押された情報
    public Button.ButtonClickedEvent IsCreate => _goButton.onClick;
}

using UnityEngine;
using UnityEngine.UI;


// タイトル画面の描画処理
namespace TitleScene.View
{
    public sealed class TitleScreenView : MonoBehaviour
    {
        [SerializeField]
        private Image _gameLogo;
        [SerializeField]
        private Button _seeThroughButton;
    }
}

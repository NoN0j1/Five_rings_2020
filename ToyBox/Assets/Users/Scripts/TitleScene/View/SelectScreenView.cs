using TitleScene.ViewModel;
using UnityEngine;
using UnityEngine.UI;


// モード選択画面の描画
namespace TitleScene.View
{
    public sealed class SelectScreenView : MonoBehaviour
    {
        // シングルモードボタン
        [SerializeField]
        private Button _singleButton;
        // 続きからボタン
        [SerializeField]
        private Button _continueButton;
        // ランキングボタン
        [SerializeField]
        private Button _rankButton;

        // 続きからフラグ
        private bool _continueFlag = false;
        // シングルボタンが押された情報
        public Button.ButtonClickedEvent IsSingle => _singleButton.onClick;
        // 続きからボタンが押された情報
        public Button.ButtonClickedEvent IsContinue => _continueButton.onClick;
        // ランキングボタンが押された情報
        public Button.ButtonClickedEvent IsRanking => _rankButton.onClick;

        // 選択画面のUI処理
        public void ControlSelectScreen(bool continueFlag)
        {
            // セーブデータがない場合は、続きからボタンを非アクティブ化
            if (!continueFlag)
            {
                _continueButton.interactable = false;
            }
            else
            {
                _continueButton.interactable = true;
            }
        }
    }
}
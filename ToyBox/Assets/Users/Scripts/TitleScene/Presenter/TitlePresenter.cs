using TitleScene.Model;
using TitleScene.ViewModel;
using UnityEngine;


// 表示スクリーン用
public enum SceneState
{
    Title,
    Mode,
    Create,
    Start,
    Continue,
    MAX
}

// Model・View間の受け渡しの処理
namespace TitleScene.Presenter
{
    public sealed class TitlePresenter : MonoBehaviour
    {
        [SerializeField]
        private TitleModel _model;
        [SerializeField]
        private ScreenController _view;         

        private void Awake()
        {
            #region Model→View
            _model.OnScreenNumberChanged.AddListener(screenNumber => _view.SetScreenNumber(screenNumber));
            _model.OnContinueFlagChanged.AddListener(flag => _view.SetContinueFlag(flag));
            _model.OnUserNameChanged.AddListener(name => _view.SetInputtedName(name));
            #endregion

            #region View→Model
            _view.OnClickReturn.AddListener(() => _model.ReturnScreen());
            _view.OnClickSingle.AddListener(() => _model.CreateData());
            _view.OnClickContinue.AddListener(() => _model.ContinueGame());
            _view.OnClickGo.AddListener(() => _model.InputName());
            _view.OnClickStart.AddListener(() => _model.StartNewGame());
            #endregion
        }
    }
}
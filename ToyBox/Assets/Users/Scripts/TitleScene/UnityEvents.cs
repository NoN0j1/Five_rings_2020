using System;
using UnityEngine.Events;


// タイトルシーンで使うイベントを管理
namespace TitleScene
{
    public sealed class ScreenEvent : UnityEvent<SceneState>
    {
    }

    public sealed class BoolEvent : UnityEvent<bool>
    {
    }

    public sealed class StringEvent : UnityEvent<String>
    {
    }

}
using System;
using TitleScene.View;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


// Modelから受け取った情報で各画面を管理
namespace TitleScene.ViewModel
{
    public sealed class ScreenController : MonoBehaviour
    {
        // タイトル画面
        [SerializeField]
        private TitleScreenView _titleScreenView;
        // モード選択画面
        [SerializeField]
        private SelectScreenView _selectScreenView;
        // ユーザー作成画面
        [SerializeField]
        private CreateScreenView _createScreenView;
        // 新規ゲーム開始画面
        [SerializeField]
        private StartScreenView _startScreenView;
        // 続きから画面
        [SerializeField]
        private ContinueScreenView _continueScreenView;
        // 戻るボタン
        [SerializeField]
        private Button _returnButton;


        public UnityEvent OnClickReturn = new UnityEvent();
        public UnityEvent OnClickSingle = new UnityEvent();
        public UnityEvent OnClickContinue = new UnityEvent();
        public UnityEvent OnClickGo = new UnityEvent();
        public UnityEvent OnClickStart = new UnityEvent();

        // 各画面の番号
        private SceneState _screenNumber = SceneState.Title;
        // 続きからフラグ
        private bool _continueFlag = false;
        // ユーザー名
        private string _nameText;

        private void Awake()
        {
            _returnButton.onClick.AddListener(() => OnClickReturn.Invoke());
            _selectScreenView.IsSingle.AddListener(() => OnClickSingle.Invoke());
            _selectScreenView.IsContinue.AddListener(() => OnClickContinue.Invoke());
            _createScreenView.IsCreate.AddListener(() => OnClickGo.Invoke());
            _startScreenView.IsStart.AddListener(() => OnClickStart.Invoke());
        }

        // 入力された名前をSet
        public void SetInputtedName(string name)
        {
            _nameText = name;
        }

        // 表示画面番号をSet
        public void SetScreenNumber(SceneState screenNumber)
        {
            _screenNumber = screenNumber;
            ChangeSceneControl();
        }

        // 初見フラグをSet
        public void SetContinueFlag(bool flag)
        {
            _continueFlag = flag;
        }

        // 画面転換の処理
        public void ChangeSceneControl()
        {
            switch (_screenNumber)
            {
                // タイトル画面
                case SceneState.Title:
                    _titleScreenView.gameObject.SetActive(true);
                    _selectScreenView.gameObject.SetActive(false);
                    _returnButton.gameObject.SetActive(false);
                    break;

                // モード選択画面
                case SceneState.Mode:
                    _selectScreenView.ControlSelectScreen(_continueFlag);
                    _titleScreenView.gameObject.SetActive(false);
                    _selectScreenView.gameObject.SetActive(true);
                    _continueScreenView.gameObject.SetActive(false);
                    _createScreenView.gameObject.SetActive(false);
                    _returnButton.gameObject.SetActive(true);
                    break;

                // データ作成画面
                case SceneState.Create:
                    _selectScreenView.gameObject.SetActive(false);
                    _createScreenView.gameObject.SetActive(true);
                    _startScreenView.gameObject.SetActive(false);
                    break;

                // ゲーム開始画面
                case SceneState.Start:
                    _startScreenView.ControlStartScreen(_nameText);
                    _createScreenView.gameObject.SetActive(false);
                    _startScreenView.gameObject.SetActive(true);
                    break;

                // 続きから画面
                case SceneState.Continue:
                    break;

                default:
                    break;
            }
        }
    }
}


using ResultScene.Ranking;
using ResultScene.View;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


namespace ResultScene.ViewModel
{
    public sealed class ResultScreenController : MonoBehaviour
    {
        [SerializeField]
        private RankingScreenView _rankingScreenView;
        [SerializeField]
        private TargetScreenView _targetScreenView;

        [SerializeField]
        private Button _nextButton;

        public UnityEvent OnClickNext = new UnityEvent();

        private ResultState _scrNum = ResultState.Ranking;
        private RankData _rankData;


        private void Awake()
        {
            _nextButton.onClick.AddListener(() => OnClickNext.Invoke());
        }


        // ランキングデータをUIに反映
        public void SetRankData(RankData data)
        {
            _rankData = data;
            _rankingScreenView.ReflectRankName(_rankData);
        }

        // 表示画面番号をSet
        public void SetScreenNumber(ResultState scrNum)
        {
            _scrNum = scrNum;
            ChangeScreen();
        }

        // 画面切り替え処理
        public void ChangeScreen()
        {
            switch (_scrNum)
            {
                // 達成状況画面
                case ResultState.ClearCount:
                    _targetScreenView.gameObject.SetActive(true);
                    _rankingScreenView.gameObject.SetActive(false);
                    break;

                // ランキング画面
                case ResultState.Ranking:
                    _targetScreenView.gameObject.SetActive(false);
                    _rankingScreenView.gameObject.SetActive(true);
                    _nextButton.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using ResultScene.Ranking;


namespace ResultScene
{
    public sealed class ScreenEvent : UnityEvent<ResultState>
    {
    }

    public sealed class BoolEvent : UnityEvent<bool>
    {
    }

    public sealed class RankDataEvent : UnityEvent<RankData>
    {
    }

}
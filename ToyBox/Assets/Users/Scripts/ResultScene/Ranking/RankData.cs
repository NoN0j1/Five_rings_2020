using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ResultScene.Ranking
{
    // セーブされるユーザー情報を保持
    [System.Serializable]
    public sealed class RankData
    {
        public string[] UserName = new string[3];
        public int[] AllStatusValue = new int[3];
    }
}
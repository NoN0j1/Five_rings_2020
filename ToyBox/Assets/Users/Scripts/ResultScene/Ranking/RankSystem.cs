using System.IO;
using UnityEngine;
using TitleScene.Saving;
using Unity.VisualScripting;

namespace ResultScene.Ranking
{
    // ランキング情報を保持するシングルトン
    public sealed class RankSystem
    {
        #region Singleton
        private static RankSystem _instance = new RankSystem();
        public static RankSystem PubInstance => _instance;
        #endregion
        private RankSystem() { }

        // 保存先のパス
        public string RankDataPath => Application.dataPath + "/rank.json";
        private RankData _rankData = new RankData();
        public RankData PubRankData => _rankData;

        // 現プレイデータのパス
        public string UserDataPath => Application.dataPath + "/data.json";
        private UserData _userData = new UserData();
        public UserData PubUserData => _userData;

        // ランキング情報の保存処理
        public void SaveHighScore()
        {
            string jsonData = JsonUtility.ToJson(_rankData);
            StreamWriter writer = new StreamWriter(RankDataPath, false);
            writer.WriteLine(jsonData);
            writer.Flush();
            writer.Close();
        }

        // ランキング情報の読み込み処理
        public RankData LoadHighScore()
        {
            if (!File.Exists(RankDataPath))
            {
                Debug.Log("データがありません。新規に作成します。");
                SaveHighScore();
            }

            StreamReader reader = new StreamReader(RankDataPath);
            string jsonData = reader.ReadToEnd();
            _rankData = JsonUtility.FromJson<RankData>(jsonData);
            reader.Close();
            Debug.Log("遊んだ数：");
            return _rankData;
        }

        // ハイスコア更新チェック
        public int CheckHighScore()
        {
            //int rankResult = -1;
            //for(int i = 0; i < 3; i++)
            //{
            //    if(_rankData.AllStatusValue[i] < SaveSystem.PubInstance.PubUserData.StartCnt)
            //    {
            //        rankResult = i;
            //        for(int j = 1; j >= i; j--)
            //        {
            //            _rankData.AllStatusValue[j + 1] = _rankData.AllStatusValue[j];
            //            _rankData.UserName[j + 1] = _rankData.UserName[j];
            //        }
            //        _rankData.AllStatusValue[i] = SaveSystem.PubInstance.PubUserData.StartCnt;
            //        _rankData.UserName[i] = SaveSystem.PubInstance.PubUserData.UserName;
            //        for(int g = 0; g < 3; g++)
            //        {
            //            Debug.Log((g+1) + "位：" + _rankData.UserName[g]);
            //        }
            //        break;
            //    }
            //}
            //return rankResult;
            return 0;
        }
    }
}
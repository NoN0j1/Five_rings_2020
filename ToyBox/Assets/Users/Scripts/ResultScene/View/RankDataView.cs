using TMPro;
using UnityEngine;

namespace ResultScene.View
{
    public sealed class RankDataView : MonoBehaviour
    {
        private TMP_Text _rankScore = null;
        private TMP_Text _userName = null;
        private bool _isInit = false;
        private void Start()
        {
            if (!_isInit)
            {
                Init();
            }
        }

        private void Init()
        {
            _rankScore = transform.Find("RankText").GetComponent<TMP_Text>();
            _userName = transform.Find("UserNameText").GetComponent<TMP_Text>();
            _isInit = true;
        }

        public void UpdateRankData(in string name, in int score)
        {
            if(!_isInit)
            {
                Init();
            }

            _userName.text = name;

            // �]���_���X�V
            if (score < 1)
            {
                _rankScore.text = "G";
            }
            else if (score >= 1 && score < 10)
            {
                _rankScore.text = "F";
            }
            else if (score >= 10 && score < 20)
            {
                _rankScore.text = "E";
            }
            else if (score >= 20 && score < 30)
            {
                _rankScore.text = "D";
            }
            else if (score >= 30 && score < 40)
            {
                _rankScore.text = "C";
            }
            else if (score >= 40 && score < 50)
            {
                _rankScore.text = "B";
            }
            else if (score >= 50 && score < 60)
            {
                _rankScore.text = "A";
            }
        }
    }
}

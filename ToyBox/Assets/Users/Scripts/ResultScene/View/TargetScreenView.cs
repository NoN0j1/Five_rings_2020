using Parameter;
using TitleScene.Saving;
using UnityEngine;
using UnityEngine.UI;

namespace ResultScene.View
{
    // 目標達成状況の画面処理
    public sealed class TargetScreenView : MonoBehaviour
    {
        // 複製する目標イベントの枠
        [SerializeField]
        private GameObject _targetFrame;
        [SerializeField]
        private GameObject _parentObject;

        // Start is called before the first frame update
        void Start()
        {
            UserData userData = SaveSystem.PubInstance.PubUserData;
            Debug.Log("目標イベント数::" + userData.TargetEventInfo.EventList.Count);
            // セーブデータにある目標イベント名をすべて検索する
            foreach (var data in userData.TargetEventInfo.EventList)
            {
                Debug.Log(data.EventName);
                //プレハブからボタンを生成
                GameObject targetFrame = Instantiate(_targetFrame) as GameObject;
                //Vertical Layout Group の子にする
                targetFrame.transform.SetParent(_parentObject.transform, false);
                targetFrame.transform.Find("EventNameText").GetComponent<Text>().text = data.EventName;
                // 達成できなかったとき
                if (data.EventStatus == TargetEventStatus.Failed)
                {
                    // 明度を維持　未達成を表示
                    targetFrame.transform.Find("FaildImage").GetComponent<Image>().enabled = true;
                    targetFrame.transform.Find("ClearImage").GetComponent<Image>().enabled = false;
                }
                // クリアしたとき
                else if (data.EventStatus == TargetEventStatus.Completed
                    || data.EventStatus == TargetEventStatus.Success
                    || data.EventStatus == TargetEventStatus.GreatSuccess)
                {
                    // 明度を下げ　クリア画像を表示
                    targetFrame.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
                    targetFrame.transform.Find("FaildImage").GetComponent<Image>().enabled = false;
                    targetFrame.transform.Find("ClearImage").GetComponent<Image>().enabled = true;
                }
                else if (data.EventStatus == TargetEventStatus.NotStart)
                {
                    targetFrame.transform.Find("FaildImage").GetComponent<Image>().enabled = false;
                    targetFrame.transform.Find("ClearImage").GetComponent<Image>().enabled = false;
                }
                Debug.Log(data.EventStatus);
            }
        }

        // 達成状況に応じてデザインを変更する
        public void TargetFrameEffect(UserData user)
        {

        }
    }
}

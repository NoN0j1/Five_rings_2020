using ResultScene.Ranking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ResultScene.View
{
    public class RankingScreenView : MonoBehaviour
    {
        private List<RankDataView> _rankViews = new List<RankDataView>();

        // Start is called before the first frame update
        private void Start()
        {
            Init();
        }

        private void Init()
        {
            _rankViews.AddRange(GetComponentsInChildren<RankDataView>());
        }

        // ランキングのユーザー名をUIに反映
        public void ReflectRankName(RankData data)
        {
            // 応急処置
            if(_rankViews.Count == 0)
            {
                Init();
            }

            //小さい方の数だけループを回す
            //int count = Mathf.Min(data.UserName.Length, _rankViews.Count);
            int count = data.UserName.Length;

            for (int i = 0; i < count; ++i)
            {
                Debug.Log("ユーザー名:" + data.UserName[i]);
                _rankViews[i].UpdateRankData(data.UserName[i], data.AllStatusValue[i]);
            }
        }
    }
}

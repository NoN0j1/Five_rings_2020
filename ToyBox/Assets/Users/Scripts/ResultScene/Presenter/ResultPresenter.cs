using ResultScene.Model;
using ResultScene.ViewModel;
using UnityEngine;

// 表示スクリーン用
public enum ResultState
{
    ClearCount,
    Ranking,
    Restart,
    MAX
}


namespace ResultScene.Presenter
{
    public sealed class ResultPresenter : MonoBehaviour
    {
        [SerializeField]
        private ResultModel _model;
        [SerializeField]
        private ResultScreenController _view;

        private void Awake()
        {
            #region Model→View
            _model.OnScreenNumberChanged.AddListener(scrNum => _view.SetScreenNumber(scrNum));
            _model.OnRankDataChanged.AddListener(data => _view.SetRankData(data));
            #endregion

            #region View→Model
            _view.OnClickNext.AddListener(() => _model.NextScreen());
            #endregion
        }
    }
}


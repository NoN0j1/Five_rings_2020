using Common;
using ResultScene.Ranking;
using TitleScene.Saving;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace ResultScene.Model
{
    public sealed class ResultModel : MonoBehaviour
    {
        public ScreenEvent OnScreenNumberChanged = new ScreenEvent();
        public RankDataEvent OnRankDataChanged = new RankDataEvent();
        private CommonSceneCanvas _efectCanvas = null;
        private ResultState _scrNum = ResultState.ClearCount;
        private RankData _rankData = null;

        // Start is called before the first frame update
        private void Start()
        {
            _efectCanvas = GameObject.Find("Fade").GetComponent<CommonSceneCanvas>();
            RankSystem.PubInstance.LoadHighScore();
            RankSystem.PubInstance.CheckHighScore();
            GetRankData();
            RankSystem.PubInstance.SaveHighScore();
            _efectCanvas.WhiteFadeIn(1);
        }

        // 総合ステータスを評価
        public int EvaluateStatus(SaveSystem init)
        {
            //if(init.PubUserData.AllStatusValue > 100)
            //{
            //    return 1;
            //}
            return 0;
        }

        // 目標のクリア数を取得
        public int GetClearTgt(SaveSystem init)
        {
            return 0;// init.PubUserData.ClearTgtCnt;
        }

        // 次へボタンを押したときの処理
        public void NextScreen()
        {
            switch (_scrNum)
            {
                // 達成状況画面
                case ResultState.ClearCount:
                    _scrNum = ResultState.Ranking;
                    break;

                // ランキング画面
                case ResultState.Ranking:
                    SaveSystem.PubInstance.DeleteData();
                    SceneManager.LoadScene("TitleScene");
                    break;

                default:
                    break;
            }
            OnScreenNumberChanged.Invoke(_scrNum);
        }

        // ランクデータを抽出
        public RankData GetRankData()
        {
            _rankData = RankSystem.PubInstance.LoadHighScore();
            OnRankDataChanged.Invoke(_rankData);
            return _rankData;
        }
    }
}
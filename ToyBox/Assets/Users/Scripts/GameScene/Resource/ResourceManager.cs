using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine;
using UniRx;

namespace GameScene.Resource
{
    /// <summary>
    /// リソース読み込みを行なう静的クラス<br/>
    /// 作成者：川野
    /// </summary>
    public static class ResourceManager
    {
        private static List<UniTask> _tasks = new List<UniTask>();
        private static Dictionary<string, string> _resource = new Dictionary<string, string>();

        /// <summary>
        /// リソースの事前読み込みを行なう
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="assetName"></param>
        public static async UniTask LoadResourceBeforehand<T>(string assetName)
        {
            string typeName = nameof(T);
            if (_resource.ContainsKey(typeName))
            {
                if (_resource.ContainsValue(assetName))
                {
                    return;
                }
            }
            _resource.Add(typeName, assetName);
            await LoadAsset<T>(assetName);
        }

        /// <summary>
        /// リソースを読み込む
        /// </summary>
        /// <typeparam name="T">読み込み予定データ型</typeparam>
        /// <param name="assetName">AddressableName</param>
        public static async UniTask<T> LoadAsset<T>(string assetName)
        {
            Debug.Log("読み込み:"+ assetName);
            var handle = Addressables.LoadAssetAsync<T>(assetName);
            await handle;
            if(handle.Status == AsyncOperationStatus.Succeeded && handle.Result != null)
            {
                return handle.Result;
            }
            Debug.Log("成功");
            return default(T);
        }
    }
}
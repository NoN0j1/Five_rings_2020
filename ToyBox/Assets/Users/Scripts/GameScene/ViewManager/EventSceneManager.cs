using System.Collections.Generic;
using UnityEngine;
using GameScene.View;
using GameScene.Parameter;

namespace GameScene.View
{
    /// <summary>
    /// イベントのシーンを保持している対象に持たせる<br/>
    /// 作成者：川野
    /// </summary>
    public interface IEventSceneView
    {
        /// <summary>
        /// リソースをセットor非表示にする
        /// </summary>
        /// <param name="image"></param>
        void SetTexture(in Sprite image);
    }
}

namespace GameScene.ViewManager
{
    public sealed class EventSceneManager : MonoBehaviour
    {
        private enum ViewType
        {
            Background,
            Scroll,
            Human,
            Accessory
        }
        private List<IEventSceneView> _sceneViews = new List<IEventSceneView>();
        void Start()
        {
            transform.GetComponentsInChildren<IEventSceneView>(true, _sceneViews);
        }

        /// <summary>
        /// シーン情報をそれぞれのViewerに渡す
        /// </summary>
        /// <param name="sceneList">設定シーンデータ</param>
        public void SetScene(EventSceneSprite sceneList)
        {
            _sceneViews[(int)ViewType.Background].SetTexture(sceneList.Background);
            _sceneViews[(int)ViewType.Human].SetTexture(sceneList.Foreground);
            _sceneViews[(int)ViewType.Accessory].SetTexture(sceneList.Accessory);
            _sceneViews[(int)ViewType.Scroll].SetTexture(null);
        }

        /// <summary>
        /// シーン情報をそれぞれのViewerに渡す
        /// </summary>
        /// <param name="sceneList">設定シーンデータ</param>
        public void SetScrollScene(Sprite scene)
        {
            _sceneViews[(int)ViewType.Scroll].SetTexture(scene);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.View;
using Parameter;
using UnityEngine;

namespace GameScene.ViewManager
{
    public sealed class StatusChangeEffectManager : MonoBehaviour
    {
        // ステータスの変動をキューに登録するためのデータ
        private sealed class StatusChangeEffectData
        {
            // 変動するパラメータのタイプ
            private ParameterType _type;

            // 変動量
            private int _value;

            public ParameterType ParameterType => _type;

            public int Value => _value;

            public StatusChangeEffectData(in ParameterType type,in int value)
            {
                _type = type;
                _value = value;
            }
        }

        [SerializeField, Header("表示間隔")]
        private float _waitTime = 0.1f;

        [SerializeField]
        private StatusChangeEffectViewDictionary _statusChangeEffectList = null;

        [SerializeField, Header("ステータス変動演出の出現場所のリスト")]
        private List<RectTransform> _emissionPosList = new List<RectTransform>();

        // ステータス変動データのキュー
        private Queue<StatusChangeEffectData> _dataQueue = new Queue<StatusChangeEffectData>();

        /// <summary>
        /// ステータス変動演出を開始する
        /// </summary>
        /// <returns> ステータス変動演出終了までのUniTask</returns>
        public UniTask EmissionStart()
        {
            return EffectEmission(this.GetCancellationTokenOnDestroy());
        }

        /// <summary>
        /// ステータス変動を登録する
        /// </summary>
        /// <param name="type"> 登録したいステータスのタイプ</param>
        /// <param name="value"> 登録したい変動値</param>
        public void Register(in IEnumerable<(ParameterType, int)> changeData)
        {
            foreach((ParameterType, int) data in changeData)
            {
                if (!_statusChangeEffectList.ContainsKey(data.Item1))
                {
                    // 表示しないステータスだったら登録しない
                    return;
                }
                _dataQueue.Enqueue(new StatusChangeEffectData(data.Item1, data.Item2));
            }
        }

        /// <summary>
        /// ステータス変動演出を順番に出す
        /// </summary>
        private async UniTask EffectEmission(CancellationToken cancellationToken)
        {
            List<UniTask> taskList = new List<UniTask>(_dataQueue.Count);

            // 変動演出を順次出現させる
            const int oneSecond = 1000;
            int waitTimeMs = (int)(_waitTime * oneSecond);
            int count = 0;
            StatusChangeEffectData data;
            while (_dataQueue.Count > 0)
            {
                data = _dataQueue.Dequeue();

                taskList.Add(_statusChangeEffectList[data.ParameterType].Emission(_emissionPosList[count].position, data.Value));

                await UniTask.Delay(waitTimeMs, false, PlayerLoopTiming.Update, cancellationToken);

                count++;
            }

            // 演出終了待ち
            await UniTask.WhenAll(taskList);
        }
    }

    [Serializable]
    public sealed class StatusChangeEffectViewDictionary : SerializableDictionary<ParameterType, StatusChangeEffectView>
    {
    }
}

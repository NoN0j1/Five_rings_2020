using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

namespace GameScene.View
{
    /// <summary>
    /// 成功率の分布を表現するバーのスクリプト
    /// </summary>
    public sealed class SuccessDistributionBar : MonoBehaviour
    {
        private const float _widthOffset = 2.0f;
        private const float _heightOffset = 7.0f;
        private const float _moveTime = 1.0f;
        private float _barWidth = 0.0f;
        private List<RectTransform> _barRects = new List<RectTransform>();
        private float _timer = 0.0f;

        void Start()
        {
            _barWidth = GetComponent<RectTransform>().rect.width;
            Transform manageCanvas = transform.Find("SuccessRateCanvas");
            var child = manageCanvas.GetComponentsInChildren<RectTransform>();
            _barRects.AddRange(child);
            _barRects.RemoveAt(0); //０番は自分なので。
            List<float> dummyDist = new List<float>() { 25.0f, 25.0f, 25.0f, 25.0f };
            float left = _widthOffset;
            float right;
            float parcent = 0.0f;
            for (int i = 0; i < _barRects.Count; ++i)
            {
                parcent += dummyDist[i] * 0.01f;
                right = _barWidth - (_barWidth * Mathf.Min(parcent, 1.0f)) + _widthOffset;
                _barRects[i].offsetMax = new Vector2(-right, -_heightOffset);
                _barRects[i].offsetMin = new Vector2(left, _heightOffset);
                left = _barWidth - right - _widthOffset;
            }
        }

        public async UniTask UpdateDistributionBar(List<float> distData)
        {
            _timer = 0.0f;
            //distData.Reverse();
            Debug.Log("失敗率:" + distData[3]);
            Debug.Log("達成率:" + distData[2]);
            Debug.Log("成功率:" + distData[1]);
            Debug.Log("大成功率:" + distData[0]);

            float left = _widthOffset;
            float right;
            float parcent = 0.0f;
            List<Vector2> startX = new List<Vector2>();
            List<Vector2> GoalX = new List<Vector2>();
            for (int i = 0; i < _barRects.Count; ++i)
            {
                parcent += distData[i] * 0.01f;
                right = _barWidth - (_barWidth * Mathf.Min(parcent, 1.0f)) + _widthOffset;
                startX.Add(new Vector2(_barRects[i].offsetMax.x, _barRects[i].offsetMin.x));
                GoalX.Add(new Vector2(-right, left));
                left = _barWidth - right - _widthOffset;
            }

            while (_timer <= _moveTime)
            {
                for (int r = 0; r < _barRects.Count; ++r)
                {
                    UpdateRectOffset(_barRects[r], startX[r], GoalX[r]);
                }
                _timer += Time.deltaTime;
                await UniTask.NextFrame();
            }
        }

        private void UpdateRectOffset(RectTransform rt, in Vector2 start, in Vector2 goal)
        {
            float t = _timer / _moveTime;
            rt.offsetMax = new Vector2(EaseOut(start.x, goal.x, t), -_heightOffset);
            rt.offsetMin = new Vector2(EaseOut(start.y, goal.y, t), _heightOffset);
        }

        private float EaseOut(float a, float b, float t)
        {
            return a * (1 - t*(2 - t)) + b * t * (2 - t);
        }
    }
}
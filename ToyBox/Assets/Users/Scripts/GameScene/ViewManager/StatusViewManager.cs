using System.Collections.Generic;
using UnityEngine;
using GameScene.Parameter;
using GameScene.View;
using Parameter;

namespace GameScene.ViewManager
{
    /// <summary>
    /// ステータス変動時の演出の管理を行うクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-08-03
    /// </summary>
    public class StatusViewManager : MonoBehaviour
    {
        private Dictionary<ParameterType, StatusView> _views = new Dictionary<ParameterType, StatusView>();
        private Canvas _canvas = null;

        void Awake()
        {
            _canvas = GetComponent<Canvas>();

            StatusView[] temp;
            temp = transform.GetComponentsInChildren<StatusView>();
            foreach(StatusView v in temp)
            {
                _views.Add(v.Type, v);
            }
        }

        /// <summary>
        /// ステータス表示を更新する
        /// </summary>
        /// <param name="data">更新データ</param>
        public void UpdateStatus(in List<Status> data)
        {
            foreach(Status p in data)
            {
                if(_views.ContainsKey(p.ParameterType) == false)
                {
                    continue;
                }
                _views[p.ParameterType].UpdateData(p.Value);
            }
        }

        /// <summary>
        /// 目標数値を更新する
        /// </summary>
        /// <param name="data">更新データ</param>
        public void SetStatusFillValue(in List<Status> data)
        {
            foreach (var p in data)
            {
                if (_views.ContainsKey(p.ParameterType) == false)
                {
                    continue;
                }
                Debug.LogWarning("データ更新:" + p.ParameterType.ToString());
                _views[p.ParameterType].SetParameterMax(p.Value);
            }
        }

        public void SetCanvasEnable(bool set)
        {
            _canvas.enabled = set;
        }
    }
}
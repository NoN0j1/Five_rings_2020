using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using GameScene.Parameter;
using GameScene.View;

namespace GameScene.ViewManager
{
    public class SelectButtonManager : MonoBehaviour
    {
        private Image _maskImage = null;
        private List<Canvas> _canvas = new List<Canvas>();
        private List<SelectionButtonView> _buttonViews = new List<SelectionButtonView>();
        private Subject<int> _subject = new Subject<int>();
        private CriAtomSource _atomSrc = null;

        public IObservable<int> Observable => _subject.TakeUntilDestroy(this);

        private void Start()
        {
            Transform findTempTransform = null;

            findTempTransform = transform.Find("MaskImage");
            if(findTempTransform != null)
            {
                _maskImage = findTempTransform.GetComponent<Image>();
            }

            Canvas[] tempCanvas = transform.GetComponentsInChildren<Canvas>();
            foreach (Canvas c in tempCanvas)
            {
                findTempTransform = c.transform.Find("Button");
                if (findTempTransform == null)
                {
                    continue;
                }
                _canvas.Add(c);
                _buttonViews.Add(findTempTransform.GetComponent<SelectionButtonView>());
            }

            foreach(Canvas c in _canvas)
            {
                c.enabled = false;
            }

            for(var i = 0; i < _buttonViews.Count; ++i)
            {
                var count = i;
                _buttonViews[i].Button.OnClickAsObservable().Subscribe(_ =>
                {
                    _maskImage.enabled = false;
                    _subject.OnNext(count);
                }).AddTo(this);
            }

            TryGetComponent(out _atomSrc);
        }

        //private void Start()
        //{
            
        //}

        /// <summary>
        /// ボタンにパラメータを登録すると同時に表示する。
        /// </summary>
        /// <param name="parameters">ボタンを押すことで変動するステータスのList</param>
        public void ShowSelectButton(in List<SelectionStatus> parameters)
        {
            if(parameters.Count <= 0)
            {
                //選択肢が０の場合はエラーなので、とりあえず進ませる
                Debug.Log("イベントの選択肢データがありません。");
                _subject.OnNext(0);
                return;
            }

            _maskImage.enabled = true;
            for(var i = 0; i < parameters.Count; ++i)
            {
                _canvas[i].enabled = true;
                _buttonViews[i].SetAnchorPosition(i, parameters.Count);
                _buttonViews[i].UpdateButtonData(parameters[i]);
            }

            PlaySound();
        }

        /// <summary>
        /// 選択肢のボタンを隠す
        /// </summary>
        public void HideSelectButton()
        {
            foreach (Canvas c in _canvas)
            {
                c.enabled = false;
            }
        }

        /// <summary>
        /// ボタン選択時に音を鳴らす
        /// </summary>
        private void PlaySound()
        {
            if(_atomSrc == null)
            {
                return;
            }
            _atomSrc.Play();
        }
    }
}

using System;
using System.Collections.Generic;
using GameScene.Parameter;
using Parameter;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// 選択肢を担うButtonを保持するViewクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class SelectionButtonView : MonoBehaviour
    {
        private const float offsetSpace = 40.0f;

        [NonSerialized]
        public Button Button;
        private TMP_Text _text = null;
        private RectTransform _rectTransform = null;
        private CriAtomSource _atomSrc;
        private List<Status> _parameter = new List<Status>();
        public List<Status> Parameters => _parameter;

        private void Awake()
        {
            Button = GetComponent<Button>();
        }

        private void Start()
        {
            _text = transform.Find("Text").GetComponent<TMP_Text>();
            _rectTransform = GetComponent<RectTransform>();
            TryGetComponent(out _atomSrc);
            Button.OnClickAsObservable().Subscribe(_ => PlaySound());
        }

        /// <summary>
        /// ボタンの情報が変更されたときに呼ばれる
        /// </summary>
        /// <param name="s">選択肢データ</param>
        public void UpdateButtonData(SelectionStatus status)
        {
            _text.text = status.Text;
            List<Status> view = new List<Status>();
            foreach(var v in status.CalculationValue)
            {
                view.Add(new Status((ParameterType)v.Key, v.Value.Value));
            }
            _parameter = view;
        }

        /// <summary>
        /// ボタンの表示位置をボタンの表示数によって調整する
        /// </summary>
        /// <param name="my">自分が何番目のボタンか</param>
        /// <param name="selectMax">全体でボタンがいくつあるか</param>
        public void SetAnchorPosition(int my, int selectMax)
        {
            float height = _rectTransform.rect.height + offsetSpace/2;
            float anchoredPositionY = 0;
            int roundDownHalf = selectMax / 2;

            if (selectMax % 2 == 0)
            {
                anchoredPositionY = (0.5f * roundDownHalf) * height - my * height;
                
            }
            else
            {
                anchoredPositionY = height * ((selectMax - 1) - my - (roundDownHalf));
            }
            _rectTransform.anchoredPosition = new Vector2(0, anchoredPositionY);
        }

        /// <summary>
        /// ボタンが押された時に音を鳴らす
        /// </summary>
        private void PlaySound()
        {
            if (_atomSrc == null)
            {
                return;
            }
            _atomSrc.Play();
        }
    }
}

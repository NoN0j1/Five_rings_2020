using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// 一番手前に表示される演出用画像を表示・管理するクラス<br/>
    /// 作成者：川野
    /// </summary>
    [RequireComponent(typeof(Image))]
    public sealed class SideScrollSceneView : MonoBehaviour, IEventSceneView
    {
        private Image _image = null;
        private RectTransform _rectTransform = null;

        void Awake()
        {
            _image = GetComponent<Image>();
            _rectTransform = GetComponent<RectTransform>();
            _image.enabled = false;
        }

        private void FixedUpdate()
        {
            if(_image == null)
            {
                return;
            }
            if(_rectTransform.transform.localPosition.x > -2000)
            {
                _rectTransform.transform.localPosition -= new Vector3(5.0f, 0, 0);
            }
        }

        //Interface実装--------------------------
        public void SetTexture(in Sprite image)
        {
            if (image == null)
            {
                _image.enabled = false;
                return;
            }
            _image.enabled = true;
            _image.sprite = image;
            _rectTransform.transform.localPosition = new Vector3(1000, 0, 0);
        }
        //---------------------------------------
    }
}
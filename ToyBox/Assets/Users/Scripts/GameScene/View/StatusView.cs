using System;
using GameScene.UIAlways;
using Parameter;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace GameScene.View
{
    /// <summary>
    /// 画面上に現在ステータスをアイコンで表示するクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-08-02
    /// </summary>
    public sealed class StatusView : MonoBehaviour
    {
        /// <summary>
        /// 担当するアイコン種別(Inspectorで指定)
        /// </summary>
        public ParameterType Type;

        private RotateUI _rotate = null;
        private Image _icon = null;
        private TextMeshProUGUI _text;
        private int _parameterMax = 1;

        private void Awake()
        {
            TryGetComponent(out _text);
            if(_text != null)
            {
                return;
            }
            Transform ring = transform.Find("Ring");
            ring.TryGetComponent(out _rotate);

            Transform iconImage = transform.Find("Icon");
            iconImage.TryGetComponent(out _icon);
            if (Type == ParameterType.Money)
            {
                if (transform.childCount < 0)
                {
                    GameObject obj = new GameObject();
                    obj.transform.SetParent(this.transform);
                }
                var child = transform.GetChild(0);
            }
        }

        /// <summary>
        /// ステータス情報が変更されたときに呼ばれる
        /// </summary>
        /// <param name="s">更新された値</param>
        public void UpdateData(in int value)
        {
            if(_icon == null)
            {
                _text.text = value.ToString();
            }
            else
            {
                _icon.fillAmount = Math.Min((float)value / (float)_parameterMax, 1.0f);
                _rotate.enabled = _icon.fillAmount >= 1;
            }
        }

        /// <summary>
        /// 現在の目標イベントでの必要値をセットする
        /// </summary>
        /// <param name="maxData">目標ステータス値(Fillの上限値)</param>
        public void SetParameterMax(in int maxData)
        {
            if(maxData < 0)
            {
                //0割り回避の為
                _parameterMax = 1;
            }
            else
            {
                _parameterMax = maxData;
            }
        }
    }
}
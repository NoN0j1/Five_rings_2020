using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// 背景の画像を表示・管理するクラス<br/>
    /// 作成者：川野
    /// </summary>
    [RequireComponent(typeof(Image))]
    public sealed class SceneBackgroundView : MonoBehaviour, IEventSceneView
    {
        private Image _image = null;

        void Awake()
        {
            _image = GetComponent<Image>();
        }

        //Interface実装--------------------------
        public void SetTexture(in Sprite image)
        {
            if (image == null)
            {
                _image.sprite = null;
                return;
            }
            _image.sprite = image;
        }
        //---------------------------------------
    }
}
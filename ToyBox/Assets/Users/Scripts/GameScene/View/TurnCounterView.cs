using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace GameScene.View
{
    /// <summary>
    /// ターン表示を行なうViewクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class TurnCounterView : MonoBehaviour
    {
        public float EffectSpeed = 1.0f;
        public float FallingY = -100;
        public float FallingX = -35;
        public Vector3 FallingRotate = Vector3.zero;
        private const string _theDay = "当日";
        private Color _targetColor = new Color(1, 1, 1, 0);
        private Text _text = null;
        private Transform _cuttingPaper = null;
        private Image _cutImage = null;
        private Text _cutText = null;
        private RectTransform _paperRectTransform = null;
        private Vector2 _defaultPos = Vector2.zero;
        private Sequence _sequence = null;

        private void Awake()
        {
            Transform calendar = transform.Find("TurnCalendar");
            _cuttingPaper = transform.Find("Cut");
            calendar.Find("Text").TryGetComponent(out _text);
            _cuttingPaper.Find("Text").TryGetComponent(out _cutText);
            _cuttingPaper.TryGetComponent(out _cutImage);
            _cuttingPaper.TryGetComponent(out _paperRectTransform);
            _text.text = string.Empty;
            _cutText.text = _text.text;
            _defaultPos = _paperRectTransform.anchoredPosition;
            _cuttingPaper.gameObject.SetActive(false);
        }

        /// <summary>
        /// 表示を更新する
        /// </summary>
        /// <param name="turnViewInfo">更新内容 item1:残りターン数, item2:アニメーションするかどうか</param>
        public void SetTurn(in (uint, bool) turnViewInfo)
        {
            _cutImage.color = Color.white;
            _cutText.color = Color.black;
            if(turnViewInfo.Item1 == 0)
            {
                _text.text = _theDay;
            }
            else
            {
                _text.text = turnViewInfo.Item1.ToString();
            }

            if(turnViewInfo.Item2)
            {
                _cuttingPaper.gameObject.SetActive(true);
                FallPaper().Forget();
            }
            else
            {
                _cutText.text = _text.text;
            }
        }


        private async UniTask FallPaper()
        {
            _sequence = DOTween.Sequence();
            
            var moveTask = _sequence
                .Append(_paperRectTransform.DOMoveY(FallingY, EffectSpeed, true).SetEase(Ease.OutCubic).SetRelative())
                .Join(_paperRectTransform.DOMoveX(FallingX, EffectSpeed, true).SetEase(Ease.OutQuad).SetRelative())
                .Join(_paperRectTransform.DORotate(FallingRotate, EffectSpeed / 2).SetDelay(EffectSpeed / 10))
                .Join(_cutImage.DOColor(_targetColor, EffectSpeed))
                .Join(_cutText.DOColor(_targetColor, EffectSpeed));
            
            await moveTask.Play().ToUniTask();
            _cuttingPaper.gameObject.SetActive(false);
            _paperRectTransform.anchoredPosition = _defaultPos;
            _paperRectTransform.rotation = Quaternion.identity;
            _cutText.text = _text.text;
        }
    }
}
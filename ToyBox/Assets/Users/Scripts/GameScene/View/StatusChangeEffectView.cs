using System.Diagnostics;
using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// ステータス変動時の演出を行うViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(CriAtomSource))]
    public sealed class StatusChangeEffectView : MonoBehaviour
    {
        // アイコン画像
        [SerializeField,Header("アイコン画像")]
        private Image _statusIcon = null;

        // 変動値を表示する用のテキスト
        [SerializeField, Header("変動値を表示する用のテキスト")]
        private Text _text = null;

        // 矢印画像
        [SerializeField,Header("矢印画像")]
        private Image _arrowImage = null;

        // 上昇矢印の色
        [SerializeField, Header("上昇矢印の色")]
        private Color _upArrowColor = Color.red;

        // 下降矢印の色
        [SerializeField, Header("下降矢印の色")]
        private Color _downArrowColor = Color.blue;

        // 上昇時の音
        [SerializeField, Header("上昇時の音")]
        private string _upSoundName = string.Empty;

        // 上昇時の音
        [SerializeField, Header("下降時の音")]
        private string _downSoundName = string.Empty;

        private RectTransform _rectTransform = null;

        // 音再生用
        private CriAtomSource _criAtomSource = null;

        // 表示時間
        private float _displayTime = 1.0f;

        // 移動方向
        private Vector3 _moveDirection = Vector3.zero;

        // 移動量
        private readonly float _moveValue = 50.0f;

        // Start is called before the first frame update
        void Start()
        {
            NullCheck();

            TryGetComponent(out _rectTransform);

            TryGetComponent(out _criAtomSource);

            gameObject.SetActive(false);
        }

        /// <summary>
        /// ステータス変動の演出
        /// </summary>
        /// <param name="pos"> 出現場所</param>
        /// <param name="value"> 変動値</param>
        /// <returns> 演出終了までのUniTask</returns>
        public UniTask Emission(in Vector3 pos, in int value)
        {
            gameObject.SetActive(true);

            string soundName = string.Empty;

            string sign;
            if(value >= 0)
            {
                sign = "+";
                _text.color = Color.red;
                _moveDirection = Vector3.up;
                _arrowImage.rectTransform.rotation = Quaternion.identity;
                _arrowImage.color = _upArrowColor;
                soundName = _upSoundName;
            }
            else
            {
                sign = "-";
                _text.color = Color.blue;
                _moveDirection = Vector3.down;
                _arrowImage.rectTransform.rotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                _arrowImage.color = _downArrowColor;
                soundName = _downSoundName;
            }

            _text.text = $"{sign}{Mathf.Abs(value)}";
            _statusIcon.color = Color.white;

            // 出現位置をずらす
            _rectTransform.position = pos - (_moveDirection * _moveValue);

            // 音を再生
            _criAtomSource.Play(soundName);

            return DisplayWait(this.GetCancellationTokenOnDestroy());
        }

        /// <summary>
        /// Nullチェック
        /// </summary>
        [Conditional("UNITY_EDITOR")]
        private void NullCheck()
        {
            if(_statusIcon == null)
            {
                UnityEngine.Debug.LogError("statusIconがNullです");
            }
            if (_text == null)
            {
                UnityEngine.Debug.LogError("textがNullです");
            }
            if (_arrowImage == null)
            {
                UnityEngine.Debug.LogError("arrowImageがNullです");
            }
        }

        /// <summary>
        /// ステータス変動の演出中
        /// </summary>
        private async UniTask DisplayWait(CancellationToken cancellationToken)
        {
            // 透明度の変化
            _text.DOFade(0.0f, _displayTime)
                .SetEase(Ease.InQuart)
                .Play();
            _statusIcon.DOFade(0.0f, _displayTime)
                .SetEase(Ease.InQuart)
                .Play();
            _arrowImage.DOFade(0.0f, _displayTime)
                .SetEase(Ease.InQuart)
                .Play();

            // 移動
            _rectTransform.DOMove(_moveDirection * _moveValue, _displayTime)
                .SetRelative()
                .SetEase(Ease.OutExpo)
                .Play();

            const int oneSecond = 1000;
            int displayTimeMs = (int)(_displayTime * oneSecond);
            await UniTask.Delay(displayTimeMs, false, PlayerLoopTiming.Update, cancellationToken);

            gameObject.SetActive(false);
        }
    }
}

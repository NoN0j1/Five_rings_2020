using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// イベント文章を表示するViewクラス<br/>
    /// 作成者：川野
    /// </summary>
    
    [RequireComponent(typeof(ObservableEventTrigger))]
    public sealed class EventTextView : MonoBehaviour
    {
        [SerializeField]
        private double _viewIntervalTime = 1;
        private TMP_Text _uiText = null;
        private ObservableEventTrigger _eventTrigger = null;
        private int _currentTextViewCount = 1;
        private string _displayText = null;
        private bool _nextText = false;
        private Canvas _canvas = null;

        /// <summary>
        /// 文字の表示が完了したかどうか
        /// </summary>
        private bool IsFinishDisplay
        {
            get
            {
                return _displayText.Length <= _currentTextViewCount;
            }
        }

        void Start()
        {
            _canvas = GetComponent<Canvas>();
            _canvas.enabled = false;
            _eventTrigger = GetComponent<ObservableEventTrigger>();
            transform.Find("Frame").Find("Text").TryGetComponent(out _uiText);
            _eventTrigger
                .OnPointerClickAsObservable()
                .ThrottleFirst(TimeSpan.FromSeconds(0.4))
                .Subscribe(_ => DisplayInstantly())
                .AddTo(this);
        }

        /// <summary>
        /// 表示するテキストデータを登録する
        /// </summary>
        /// <param name="s">テキストデータ</param>
        public void SetText(in string text)
        {
            _canvas.enabled = true;
            _displayText = text;
            _currentTextViewCount = 0;
            _nextText = false;
        }

        /// <summary>
        /// テキストを少しずつ表示する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        public async UniTask DelayedDisplayText(CancellationTokenSource ct)
        {
            await DisplayStreamText(ct);
            while(_nextText == false)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                await UniTask.NextFrame(ct.Token).Preserve();
            }
        }

        /// <summary>
        /// 文字列を指定文字数だけ表示する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask DisplayStreamText(CancellationTokenSource ct)
        {
            while (IsFinishDisplay == false)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }
                await DisplayText(ct).Preserve();
                await UniTask.NextFrame(ct.Token).Preserve();
            }
        }

        /// <summary>
        /// 表示する指定文字数だけの文字列を生成する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask DisplayText(CancellationTokenSource ct)
        {
            await UniTask.Delay(TimeSpan.FromMilliseconds(_viewIntervalTime),false, PlayerLoopTiming.FixedUpdate, ct.Token).Preserve();
            if(ct.IsCancellationRequested || _uiText.IsDestroyed())
            {
                return;
            }
            _currentTextViewCount++;
            _currentTextViewCount = Math.Min(_currentTextViewCount, _displayText.Length);
            _uiText.text = _displayText.Substring(0, _currentTextViewCount);
        }

        /// <summary>
        /// テキストを瞬時に全て表示する。全て表示していた場合はテキストを進める。
        /// </summary>
        private void DisplayInstantly()
        {
            if(_displayText == null)
            {
                return;
            }
            if(_currentTextViewCount < _displayText.Length)
            {
                _currentTextViewCount = _displayText.Length;
                _uiText.text = _displayText.Substring(0, Math.Min(_currentTextViewCount, _displayText.Length));
            }
            else
            {
                _nextText = true;
            }
        }
    }
}

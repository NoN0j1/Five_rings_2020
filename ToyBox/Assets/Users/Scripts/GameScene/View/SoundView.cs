using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameScene.View
{
    /// <summary>
    /// 効果音やBGMを管理するView
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-08-18
    /// </summary>
    public sealed class SoundView : MonoBehaviour
    {
        private enum SoundType
        {
            BGM,
            SE
        }
        private List<CriAtomSource> _criAtomSource = new List<CriAtomSource>();

        void Start()
        {
            var s = GetComponentsInChildren<CriAtomSource>();

            _criAtomSource.AddRange(GetComponentsInChildren<CriAtomSource>());
        }

        public void StartDefaultBgm()
        {
            if(_criAtomSource[(int)SoundType.BGM].cueName == string.Empty)
            {
                _criAtomSource[(int)SoundType.BGM].cueName = "Endurance_Game";
                _criAtomSource[(int)SoundType.BGM].Play();
            }
        }

        public void ChangeBgm(string bgmName)
        {
            _criAtomSource[(int)SoundType.BGM].Stop();
            _criAtomSource[(int)SoundType.BGM].cueName = bgmName;
            _criAtomSource[(int)SoundType.BGM].Play();
        }

        public void PlaySE(string seName)
        {
            _criAtomSource[(int)SoundType.SE].cueName = seName;
            _criAtomSource[(int)SoundType.SE].Play();
        }
    }
}
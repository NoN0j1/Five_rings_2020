using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Parameter;

namespace GameScene.View
{
    /// <summary>
    /// 目標イベント判定を受けた時に結果を表示するViewクラス<br/>
    /// 作成者：川野
    /// </summary>
    [RequireComponent(typeof(Image))]
    public sealed class ClearEffectView : MonoBehaviour
    {
        private Image _image = null;
        RectTransform _rect => _image.rectTransform;
        private Sequence _sequence = null;
        private Func<UniTask> _func;
        private Dictionary<TargetEventStatus, Sprite> _imageResource = new Dictionary<TargetEventStatus, Sprite>();
        private readonly List<string> _spriteAddress = new List<string>() { "Fail", "Complete", "Success", "GreatSuccess" };
        void Awake()
        {
            _image = GetComponent<Image>();
            _image.enabled = false;
        }

        private async void Start()
        {
            List<UniTask<Sprite>> task = new List<UniTask<Sprite>>();
            List<Sprite> loadResource = new List<Sprite>();
            for (int i = (int)TargetEventStatus.Failed; i < Enum.GetValues(typeof(TargetEventStatus)).Length; ++i)
            {
                task.Add(Resource.ResourceManager.LoadAsset<Sprite>(_spriteAddress[i - (int)TargetEventStatus.Failed]));
            }
            var load = await UniTask.WhenAll(task);
            loadResource.AddRange(load);

            TargetEventStatus status = TargetEventStatus.Failed;
            foreach (Sprite s in loadResource)
            {
                _imageResource.Add(status, s);
                status++;
            }
        }

        public async UniTask DisplayEffect(TargetEventStatus eventResult)
        {
            switch(eventResult)
            {
                case TargetEventStatus.GreatSuccess:
                    _func = GreatSuccessEffect;
                    break;
                case TargetEventStatus.Success:
                    _func = SuccessEffect;
                    break;
                case TargetEventStatus.Completed:
                    _func = CompleteEffect;
                    break;
                case TargetEventStatus.Failed:
                    _func = FailEffect;
                    break;
                default:
                    return;
            }
            _image.enabled = true;
            _image.overrideSprite = _imageResource[eventResult];
            _sequence = DOTween.Sequence();
            _func = GreatSuccessEffect;
            await _func();
            
            _image.enabled = false;
        }

        private async UniTask GreatSuccessEffect()
        {
            _rect.localScale = Vector3.zero;
            //_rect.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            _sequence.Append(_rect.DOLocalRotate(new Vector3(0, 0, -360 * 5.0f), 1.0f));
            _sequence.Join(_rect.DOScale(new Vector3(1, 1, 1), 1.0f));
            await _sequence.Play();
            await UniTask.Delay(2000, false, PlayerLoopTiming.Update);
        }
        private async UniTask SuccessEffect()
        {

        }
        private async UniTask CompleteEffect()
        {

        }
        private async UniTask FailEffect()
        {

        }
    }
}
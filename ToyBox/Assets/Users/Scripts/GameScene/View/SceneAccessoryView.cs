using UnityEngine;
using UnityEngine.UI;

namespace GameScene.View
{
    /// <summary>
    /// 小物類の画像を表示・管理するクラス<br/>
    /// 作成者：川野
    /// </summary>
    [RequireComponent(typeof(Image))]
    public sealed class SceneAccessoryView : MonoBehaviour, IEventSceneView
    {
        private Image _image = null;

        void Awake()
        {
            _image = GetComponent<Image>();
        }

        //Interface実装--------------------------
        public void SetTexture(in Sprite image)
        {
            if (image == null)
            {
                _image.enabled = false;
                return;
            }
            _image.enabled = true;
            _image.sprite = image;
        }
        //---------------------------------------
    }
}
﻿using System;
using GameScene.Usecase;
using GameScene.View;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// イベント内テキストをテキスト表示Viewに送り、<br/>
    /// 全て表示が終わるまで待つようにGameManagerのAsyncTaskに追加を行なうPresenter<br/>
    /// 作成者：川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class EventTextPresenter: IInitializable, IDisposable
    {
        private readonly IGameManager _managerInterface;
        private readonly EventTextView _eventTextView;
        private CompositeDisposable _disposables = new CompositeDisposable();

        [Inject]
        public EventTextPresenter(IGameManager manager, EventTextView textView)
        {
            _managerInterface = manager;
            _eventTextView = textView;
        }

        public void Initialize()
        {
            _managerInterface.CommonEventInterface.TextObservable
                .Subscribe(text =>
                {
                    _eventTextView.SetText(text);
                    _managerInterface.CommonEventInterface.AddAsyncTask(_eventTextView.DelayedDisplayText(_managerInterface.CommonEventInterface.CancellationToken));
                })
                .AddTo(_disposables);
        }
        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
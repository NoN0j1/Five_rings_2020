﻿using System;
using GameScene.Usecase;
using GameScene.ViewManager;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// ステータス表示を行なっているViewに最新のステータス情報を送るPresenter<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class StatusPresenter : IPostInitializable, IDisposable
    {
        private readonly IGameManager _gameManagerInterface = null;
        private readonly StatusViewManager _viewManager = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public StatusPresenter(StatusViewManager buttonManager, IGameManager manager)
        {
            _gameManagerInterface = manager;
            _viewManager = buttonManager;
        }

        void IPostInitializable.PostInitialize()
        {
            _gameManagerInterface.NormalEventInterface.StatusObservable
                .Subscribe(data =>
                {
                    _viewManager.UpdateStatus(data);
                })
                .AddTo(_disposable);

            _gameManagerInterface.IntroduceNextTargetInterface.NextTargetStatusObservable
                .Subscribe(targetParameter =>
                {
                    _viewManager.SetStatusFillValue(targetParameter);
                })
                .AddTo(_disposable);

            _gameManagerInterface.StatusEnable
                .Subscribe(enable => _viewManager.SetCanvasEnable(enable))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}
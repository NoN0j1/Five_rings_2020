﻿using System;
using System.Collections.Generic;
using VContainer;
using VContainer.Unity;
using GameScene.Usecase;
using GameScene.View;
using UniRx;

namespace GameScene.Presenter
{
    public sealed class DistributionBarPresenter : IPostInitializable, IDisposable
    {
        private readonly IGameManager _gameManager = null;
        private readonly SuccessDistributionBar _bar = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public DistributionBarPresenter(IGameManager manager, SuccessDistributionBar view)
        {
            _gameManager = manager;
            _bar = view;
        }

        public void PostInitialize()
        {
            _gameManager.NormalEventInterface.SuccsessRateObservable
                .Subscribe(data => _gameManager.CommonEventInterface.AddAsyncTask(_bar.UpdateDistributionBar(data)))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}

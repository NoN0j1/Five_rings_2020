using System;
using GameScene.Usecase;
using GameScene.ViewManager;
using UniRx;
using VContainer;
using VContainer.Unity;

public sealed class StatusChangeEffectPresenter : IInitializable, IDisposable
{
    private readonly IGameManager _managerInterface;

    private readonly StatusChangeEffectManager _statusChangeEffectManager;

    private CompositeDisposable _disposables = new CompositeDisposable();

    [Inject]
    public StatusChangeEffectPresenter(IGameManager gameManager, StatusChangeEffectManager statusChangeEffectManager)
    {
        _managerInterface = gameManager;
        _statusChangeEffectManager = statusChangeEffectManager;
    }

    public void Initialize()
    {
        //変動内容を登録
        _managerInterface.NormalEventInterface.StatusChangeRegister
            .Subscribe(data =>
            {
                _statusChangeEffectManager.Register(data);
            })
            .AddTo(_disposables);

        //ステータス変動開始処理を登録
        _managerInterface.NormalEventInterface.StatusChangeStart
            .Subscribe(unit =>
            {
                _managerInterface.CommonEventInterface.AddAsyncTask(_statusChangeEffectManager.EmissionStart());
            })
            .AddTo(_disposables);

        //_managerInterface.StatusChangeRegister.Subscribe(data => _statusChangeEffectManager.Register(data.Item1, data.Item2))
        //    .AddTo(_disposables);

        //_managerInterface.StatusChangeStart.Subscribe(unit => _managerInterface.AddAsyncTask(_statusChangeEffectManager.EmissionStart()))
        //    .AddTo(_disposables);
    }

    public void Dispose()
    {
        _disposables.Dispose();
    }
}

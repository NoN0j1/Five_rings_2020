﻿using System;
using GameScene.Usecase;
using GameScene.View;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// 効果音やBGM切替の通知を確認して管理Viewに送るクラス<br/>
    /// 作成者：川野<br/>
    /// 最終更新 : 2021-08-18
    /// </summary>
    public sealed class SoundPresenter : IPostInitializable, IDisposable
    {
        private readonly IGameManager _managerInterface = null;
        private readonly SoundView _soundView = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public SoundPresenter(SoundView sound, IGameManager manager)
        {
            _managerInterface = manager;
            _soundView = sound;
        }

        void IPostInitializable.PostInitialize()
        {
            _managerInterface.CommonEventInterface.StartEventObservable
                .Subscribe(_ => _soundView.StartDefaultBgm())
                .AddTo(_disposable);
            _managerInterface.CommonEventInterface.BgmObservable
                .Subscribe(data => _soundView.ChangeBgm(data))
                .AddTo(_disposable);

            _managerInterface.CommonEventInterface.SeObservable
                .Subscribe(data => _soundView.PlaySE(data))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}

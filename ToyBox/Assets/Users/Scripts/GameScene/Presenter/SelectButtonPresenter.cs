using System;
using Cysharp.Threading.Tasks;
using GameScene.Usecase;
using GameScene.ViewManager;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// 選択肢を担うButtonに選択肢表示の通知を送り、<br/>
    /// Buttonが押された通知をGameManagerに送るPresenterクラス<br/>
    /// 作成者：川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class SelectButtonPresenter : IPostInitializable, IDisposable
    {
        private readonly IGameManager _managerInterface = null;
        private readonly SelectButtonManager _buttonManager = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public SelectButtonPresenter(SelectButtonManager buttonManager, IGameManager manager)
        {
            _managerInterface = manager;
            _buttonManager = buttonManager;
        }

        void IPostInitializable.PostInitialize()
        {
            //何番目のボタンが押されたかを選択肢ボタンManagerから受け取りイベントに流す
            _buttonManager.Observable
                .Subscribe(selectNum =>
                {
                    _buttonManager.HideSelectButton();
                    _managerInterface.NormalEventInterface.SelectionNumObserver.OnNext(selectNum);
                })
                .AddTo(_disposable);

            //選択肢の情報を受け取り、選択肢ボタンに送信する
            _managerInterface.NormalEventInterface.SelectionDataObservable
                .Subscribe(data =>_buttonManager.ShowSelectButton(data))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}

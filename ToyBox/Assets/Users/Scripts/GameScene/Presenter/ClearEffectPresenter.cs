using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using GameScene.Usecase;
using GameScene.View;
using GameScene.Parameter;
using UniRx;

public class ClearEffectPresenter : IPostInitializable, IDisposable
{
    private readonly ClearEffectView _view = null;
    private readonly IGameManager _managerInterface = null;
    private CompositeDisposable _disposable = new CompositeDisposable();

    [Inject]
    public ClearEffectPresenter(IGameManager gameManager, ClearEffectView view)
    {
        _managerInterface = gameManager;
        _view = view;
    }
    void IPostInitializable.PostInitialize()
    {
        _managerInterface.TargetEventInterface.ClearStatusObservable
              .Subscribe(result => 
              _managerInterface.CommonEventInterface.AddAsyncTask(_view.DisplayEffect(result)))
              .AddTo(_disposable);
        //_managerInterface.TargetObservable
        //    .OfType<object, TargetEventResult>()
        //    .Subscribe(result => _managerInterface.AddAsyncTask(_view.DisplayEffect(result)))
        //    .AddTo(_disposable);
    }

    void IDisposable.Dispose()
    {
        _disposable.Dispose();
    }
}

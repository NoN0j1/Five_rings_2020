﻿using System;
using GameScene.Parameter;
using GameScene.Usecase;
using GameScene.ViewManager;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// イベントのシーン情報をSceneViewManagerに送るPresenter<rb/>
    /// 作成者：川野
    /// </summary>
    public sealed class EventScenePresenter:IPostInitializable, IDisposable
    {
        private readonly IGameManager _managerInterface = null;
        private readonly EventSceneManager _sceneManager = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public EventScenePresenter(EventSceneManager sceneManager, IGameManager gameManager)
        {
            _sceneManager = sceneManager;
            _managerInterface = gameManager;
        }

        void IPostInitializable.PostInitialize()
        {
            _managerInterface.CommonEventInterface.EventSceneObservable
                .Subscribe(scene => _sceneManager.SetScene(scene))
                .AddTo(_disposable);
            _managerInterface.CommonEventInterface.ScrollSceneObservable
                .Subscribe(scene => _sceneManager.SetScrollScene(scene))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}

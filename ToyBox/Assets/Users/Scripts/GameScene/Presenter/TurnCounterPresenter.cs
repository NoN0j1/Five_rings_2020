using System;
using GameScene.Usecase;
using GameScene.View;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace GameScene.Presenter
{
    /// <summary>
    /// ターン数表示箇所のViewに情報を送信するPresenter<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class TurnCounterPresenter : IPostInitializable, IDisposable
    {
        private readonly TurnCounterView _turnView = null;
        private readonly IGameManager _managerInterface = null;
        private CompositeDisposable _disposable = new CompositeDisposable();

        [Inject]
        public TurnCounterPresenter(IGameManager manager, TurnCounterView gameTurnView)
        {
            _managerInterface = manager;
            _turnView = gameTurnView;
        }

        void IPostInitializable.PostInitialize()
        {
            //値が変わった時にViewのSetTextが呼ばれるように登録
            _managerInterface.TurnDisplay
                .Subscribe(displayNum => _turnView.SetTurn(displayNum))
                .AddTo(_disposable);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
    }
}
using Common;
using GameScene.Presenter;
using GameScene.Repository;
using GameScene.Repository.Interface;
using GameScene.Usecase;
using GameScene.ViewManager;
using GameScene.View;
using VContainer;
using VContainer.Unity;

namespace GameScene.DI
{
    /// <summary>
    /// メインゲームシーンのDIを行なうクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class GameLoopVContainer : LifetimeScope
    {
        /// <summary>
        /// VContainerが管理するクラスのインスタンスを登録
        /// </summary>
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<GameManager>(Lifetime.Scoped);
            builder.Register<IGameRepository, GameRepository>(Lifetime.Scoped);
            builder.RegisterComponentInHierarchy<SelectButtonManager>();
            builder.RegisterComponentInHierarchy<EventSceneManager>();
            builder.RegisterComponentInHierarchy<StatusViewManager>();
            builder.RegisterComponentInHierarchy<EventTextView>();
            builder.RegisterComponentInHierarchy<TurnCounterView>();
            builder.RegisterComponentInHierarchy<ClearEffectView>();
            builder.RegisterComponentInHierarchy<SoundView>();
            builder.RegisterComponentInHierarchy<SuccessDistributionBar>();
            builder.RegisterComponentInHierarchy<StatusChangeEffectManager>();
            builder.RegisterComponentInHierarchy<CommonSceneCanvas>();
            builder.RegisterEntryPoint<GameManager>(Lifetime.Scoped);
            builder.RegisterEntryPoint<GameRepository>(Lifetime.Scoped);
            builder.RegisterEntryPoint<TurnCounterPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<SelectButtonPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<StatusPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<EventTextPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<EventScenePresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<ClearEffectPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<DistributionBarPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<StatusChangeEffectPresenter>(Lifetime.Scoped);
            builder.RegisterEntryPoint<SoundPresenter>(Lifetime.Scoped);
        }
    }
}
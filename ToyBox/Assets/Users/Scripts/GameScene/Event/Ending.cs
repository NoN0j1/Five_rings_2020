﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Repository.Interface;
using Parameter;
using TitleScene.Saving;

namespace GameScene.Event
{
    /// <summary>
    /// エンディングイベントクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class Ending : IEvent
    {
        /// <summary>
        /// イベント内で呼び出される関数キューの実装を行なうクラス<br/>
        /// 作成者 : 川野<br/>
        /// 最終更新 : 2021-07-28
        /// </summary>
        private sealed class Function
        {
            /// <summary>
            /// 実行親
            /// </summary>
            private readonly Ending _ending = null;

            public Function(Ending ending)
            {
                _ending = ending;
            }

            /// <summary>
            /// イベントテキストを送信する
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask BeginEnding(CancellationTokenSource ct)
            {
                _ending.EventCommons.SendEventName(_ending.EventName);
                await _ending.EventCommons.SendEventText(_ending.EventText);
            }
        }

        /// <summary>
        /// イベント名
        /// </summary>
        private string[] _eventNames = { "トゥルーエンド", "エンディング2", "エンディング3" };

        /// <summary>
        /// イベントテキスト
        /// </summary>
        private List<string> _eventText = new List<string>();

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        private EventCommonsProcess _commonsProcess = null;

        /// <summary>
        /// エンディング番号
        /// </summary>
        private uint _resultNum = 0;

        /// <summary>
        /// 関数名リスト
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// セーブ内容
        /// </summary>
        public SaveEventInfo _saveData = null;

        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// 関数実行クラス
        /// </summary>
        private Function _function = null;

        /// <summary>
        /// イベント名を取得する
        /// </summary>
        public string EventName => _eventNames[_resultNum];

        /// <summary>
        /// イベント内テキストリストを取得する
        /// </summary>
        public List<string> EventText => _eventText;

        /// <summary>
        /// イベント共通処理を行なうクラスを取得する
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcess;

        //interface実装--------------------------------------------------
        async UniTask IEvent.Run()
        {
            await EventLoop(_commonsProcess.CancellationToken);
        }
        //===============================================================


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repository">リポジトリインターフェース</param>
        public Ending(IGameRepository repository, EventCommonsProcess commons)
        {
            _repositoryInterface = repository;
            _commonsProcess = commons;
            _function = new Function(this);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize(uint resultType)
        {
            _resultNum = resultType;
            _saveData = new SaveEventInfo(_eventNames[_resultNum], GameEventType.Ending);
            EventTextSetting();
            EnqueueFunctionName();
        }

        /// <summary>
        /// イベント文章を登録する
        /// </summary>
        private void EventTextSetting()
        {
            if(_resultNum == 0)
            {
                _eventText.Add("{シーン}Prologue2");
                _eventText.Add("{SendScene}");
                _eventText.Add("運営委員会は世間から全く期待されなくなってしまった。");
                _eventText.Add("組織自体は別に引き継がれる。");
                _eventText.Add("～ ＦＩＮ ～");
                _eventText.Add("ＢＡＤ ＥＮＤ");
            }
            else if(_resultNum == 1)
            {
                _eventText.Add("{シーン}Prologue0");
                _eventText.Add("{SendScene}");
                _eventText.Add("無事に大会は開催された。");
                _eventText.Add("開催してみれば概ね好評であった。");
                _eventText.Add("様々な苦労はあったが、やって良かったと思う。");
                _eventText.Add("（報酬も得られたしね☆）");
                _eventText.Add("～ ＦＩＮ ～");
            }
        }

        /// <summary>
        /// 基本の処理順で関数を登録
        /// </summary>
        private void EnqueueFunctionName()
        {
            _functionQueue.Enqueue(nameof(_function.BeginEnding));
        }

        /// <summary>
        /// 順番にターン内の関数を実行する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask EventLoop(CancellationTokenSource ct)
        {
            //開始前にセーブ
            _saveData.UpdateValue(_functionQueue, (int)_resultNum);
            _repositoryInterface.Save(_saveData);

            string func;
            System.Reflection.MethodInfo method;
            UniTask task;
            while (_functionQueue.Count != 0)
            {
                func = _functionQueue.Peek();
                method = _function.GetType().GetMethod(func);
                task = (UniTask)method.Invoke(_function, new object[] { ct });
                await task;
                await UniTask.NextFrame(ct.Token).Preserve();
                _functionQueue.Dequeue();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using GameScene.Repository.Interface;
using Parameter;
using ScriptableObjects;
using ScriptableObjects.EventData;
using TitleScene.Saving;
using UniRx;

namespace GameScene.Event
{
    /// <summary>
    /// 通常イベントクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class TurnEvent : IEvent
    {
        /// <summary>
        /// イベント内で呼び出される関数キューの実装を行なうクラス<br/>
        /// 作成者 : 川野<br/>
        /// 最終更新 : 2021-07-28
        /// </summary>
        private sealed class Function
        {
            /// <summary>
            /// 実行親
            /// </summary>
            private readonly TurnEvent _turnEvent = null;

            public Function(TurnEvent turn)
            {
                _turnEvent = turn;
            }

            /// <summary>
            /// ターンの最初に更新する内容を送る
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask TurnBeginUpdate(CancellationTokenSource ct)
            {
                _turnEvent.SaveInfo.UpdateValue(_turnEvent.FunctionQueue, _turnEvent.Select);
                await _turnEvent.EventCommons.SendEventScene(_turnEvent.EventScene);
                //成功率の更新
                _turnEvent.EventCommons.Calculator.SuccessRateCalculation();
                _turnEvent.TurnProcessor.TurnSubject.OnNext(_turnEvent.EventCommons.Calculator.SuccsessRates);
                //-------------
                //現在能力値表示の更新
                _turnEvent.TurnProcessor.TurnSubject.OnNext(_turnEvent.AbilityValues);

                await _turnEvent.EventCommons.WaitAsyncTask();
            }

            /// <summary>
            /// イベント内容を送る
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask SendEventData(CancellationTokenSource ct)
            {
                _turnEvent.EventCommons.SendEventName(_turnEvent.EventName);
                await _turnEvent.EventCommons.SendEventText(_turnEvent.EventText);

                //選択肢を送る
                _turnEvent.TurnProcessor.TurnSubject.OnNext(_turnEvent.Selections);

                //選択肢が押されるとSubscribeに書いた処理によって_selectNumが変わるのでループを抜ける
                while (_turnEvent.Select == -1)
                {
                    if (ct.IsCancellationRequested)
                    {
                        return;
                    }
                    await UniTask.NextFrame(ct.Token).Preserve();
                }
            }

            /// <summary>
            /// 選択肢を選んだあとの処理
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask ChoseSelectionData(CancellationTokenSource ct)
            {
                _turnEvent.SaveInfo.UpdateValue(_turnEvent.FunctionQueue, _turnEvent.Select);
                var changeValue = _turnEvent.EventCommons.Calculator.CalculateParameterValue(_turnEvent.Selections[_turnEvent.Select]);
                _turnEvent.TurnProcessor.TurnSubject.OnNext(changeValue);
            }

            /// <summary>
            /// 選択肢を選んだあとの処理
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask ChangeValueAnimation(CancellationTokenSource ct)
            {
                _turnEvent.TurnProcessor.TurnSubject.OnNext(Unit.Default);
                await _turnEvent.EventCommons.WaitAsyncTask();
            }
        }
        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// セーブ内容
        /// </summary>
        private SaveEventInfo _saveData = null;

        /// <summary>
        /// 選ばれた選択肢
        /// </summary>
        private int _selectNum = -1;

        /// <summary>
        /// 選択肢内容
        /// </summary>
        private List<SelectionStatus> _selectionStatus = null;

        /// <summary>
        /// 読み込み元データ
        /// </summary>
        private EventData _eventData = null;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        private EventCommonsProcess _commonsProcess = null;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        private TurnEventProcess _turnProcess = null;

        /// <summary>
        /// 関数名リスト
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// 関数実行クラス
        /// </summary>
        private Function _function = null;

        /// <summary>
        /// イベント名を取得する
        /// </summary>
        public string EventName => _eventData.EventName;

        /// <summary>
        /// イベント内テキストリストを取得する
        /// </summary>
        public List<string> EventText => _eventData.EventTextList;

        /// <summary>
        /// 選んだ選択肢を取得する
        /// </summary>
        public int Select => _selectNum;

        /// <summary>
        /// 選択肢リストを取得する
        /// </summary>
        public List<SelectionStatus> Selections => _selectionStatus;

        /// <summary>
        /// イベントシーン情報を取得する
        /// </summary>
        public EventDirectionScene EventScene => _eventData.EventDirectionSceneName;

        /// <summary>
        /// セーブ情報を取得する
        /// </summary>
        public SaveEventInfo SaveInfo => _saveData;

        /// <summary>
        /// 現在の能力値を取得する
        /// </summary>
        public List<Status> AbilityValues => _repositoryInterface.AbilityValues;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcess;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        public TurnEventProcess TurnProcessor => _turnProcess;

        /// <summary>
        /// 関数キューを取得する
        /// </summary>
        public Queue<string> FunctionQueue => _functionQueue;

        //interface実装--------------------------------------------------
        async UniTask IEvent.Run()
        {
            _commonsProcess.EventStart();
            await EventLoop(_commonsProcess.CancellationToken);
        }
        //===============================================================


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repository">リポジトリインターフェース</param>
        /// <param name="common">共通処理クラス</param>
        /// <param name="turnProcess">ターンイベント処理用クラス</param>
        public TurnEvent(IGameRepository repository, EventCommonsProcess common, TurnEventProcess turnProcess)
        {
            _repositoryInterface = repository;
            _commonsProcess = common;
            _turnProcess = turnProcess;
            _function = new Function(this);
            _turnProcess.SelectNumObservable
                .Subscribe(num => _selectNum = num)
                .AddTo(_commonsProcess.Disposables);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="data">ScriptableObjectから読み込んだイベントデータ</param>
        public void Initialize(EventData data)
        {
            _eventData = data;
            _saveData = new SaveEventInfo(_eventData.DataName, GameEventType.Turn);
            _selectNum = _saveData.Result;
            ConvertSelectionData();
            EnqueueFunctionName();
        }

        /// <summary>
        /// セーブデータから初期化
        /// </summary>
        /// <param name="data">ScriptableObjectから読み込んだイベントデータ</param>
        /// <param name="save">セーブデータ</param>
        public void Initialize(EventData data, UserData save)
        {
            _eventData = data;
            _saveData = save.PickedEvent;
            _selectNum = _saveData.Result;
            _functionQueue = _saveData.FunctionQueue;
            ConvertSelectionData();
        }

        /// <summary>
        /// 基本の処理順で関数を登録
        /// </summary>
        private void EnqueueFunctionName()
        {
            _functionQueue.Enqueue(nameof(_function.TurnBeginUpdate));
            _functionQueue.Enqueue(nameof(_function.SendEventData));
            _functionQueue.Enqueue(nameof(_function.ChoseSelectionData));
            _functionQueue.Enqueue(nameof(_function.ChangeValueAnimation));
        }

        /// <summary>
        /// ScriptalbleObjectのデータをゲームで使う形に変換する
        /// </summary>
        private void ConvertSelectionData()
        {
            _selectionStatus = new List<SelectionStatus>();
            foreach (var data in _eventData.SelectionDataList)
            {
                if(data == null)
                {
                    UnityEngine.Debug.Log("データなし");
                }
                _selectionStatus.Add(new SelectionStatus(data.ParameterIncreaseValue, data.SelectionText));
            }
        }

        /// <summary>
        /// 順番にターン内の関数を実行する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask EventLoop(CancellationTokenSource ct)
        {
            //開始前にセーブ
            _saveData.UpdateValue(_functionQueue, _selectNum);
            _repositoryInterface.Save(_saveData);

            string func;
            System.Reflection.MethodInfo method;
            UniTask task;
            while (_functionQueue.Count != 0)
            {
                _repositoryInterface.Save(_saveData);
                func = _functionQueue.Peek();
                method = _function.GetType().GetMethod(func);
                task = (UniTask)method.Invoke(_function, new object[] { ct });
                await task;
                await UniTask.NextFrame(ct.Token).Preserve();
                _functionQueue.Dequeue();
            }
        }
    }
}

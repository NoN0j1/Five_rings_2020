﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Repository.Interface;
using TitleScene.Saving;
using Parameter;
using ScriptableObjects;

namespace GameScene.Event
{
    /// <summary>
    /// オープニングイベントクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class Prologue : IEvent
    {
        /// <summary>
        /// イベント内で呼び出される関数キューの実装を行なうクラス<br/>
        /// 作成者 : 川野<br/>
        /// 最終更新 : 2021-07-28
        /// </summary>
        private sealed class Function
        {
            /// <summary>
            /// 実行親
            /// </summary>
            private readonly Prologue _prologue = null;

            public Function(Prologue prologue)
            {
                _prologue = prologue;
            }
            public async UniTask BeginPrologue(CancellationTokenSource ct)
            {
                _prologue.EventCommons.SendEventName(_prologue.EventName);
                await _prologue.EventCommons.SendEventScene(_prologue.EventScenes);
                await _prologue.EventCommons.SendEventText(_prologue.EventText);
            }
        }
        /// <summary>
        /// イベント名
        /// </summary>
        private string _eventName = "プロローグ";

        /// <summary>
        /// イベントテキスト
        /// </summary>
        private List<string> _eventTextData = new List<string>();

        /// <summary>
        /// シーン情報
        /// </summary>
        private EventDirectionScene _sceneNames = new EventDirectionScene();

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        private EventCommonsProcess _commonsProcess = null;

        /// <summary>
        /// 関数名リスト
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// セーブ内容
        /// </summary>
        private SaveEventInfo _saveData = null;

        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// 関数実行クラス
        /// </summary>
        private Function _function = null;

        /// <summary>
        /// 現在のイベント名を取得する
        /// </summary>
        public string EventName => _eventName;

        /// <summary>
        /// 現在のテキストデータを取得する
        /// </summary>
        public List<string> EventText => _eventTextData;

        /// <summary>
        /// イベント共通処理クラスを取得する
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcess;

        /// <summary>
        /// シーン情報を取得する
        /// </summary>
        public EventDirectionScene EventScenes => _sceneNames;

        //interface実装--------------------------------------------------
        async UniTask IEvent.Run()
        {
            await EventLoop(_commonsProcess.CancellationToken);
        }
        //===============================================================


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repository"></param>
        public Prologue(IGameRepository repository, EventCommonsProcess commons)
        {
            _repositoryInterface = repository;
            _commonsProcess = commons;
            _function = new Function(this);
            EventTextSetting();
            EventSceneSetting();
            EnqueueFunctionName();
            _saveData = new SaveEventInfo(_eventName, GameEventType.Prologue);
        }

        /// <summary>
        /// イベント文章を登録する
        /// </summary>
        private void EventTextSetting()
        {
            _eventTextData.Add("{BGM}スタジアムのざわめき");
            _eventTextData.Add("謎の外人「それでは協議した結果を発表いたします」");
            _eventTextData.Add("謎の外人「次回の開催場所は・・・」");
            _eventTextData.Add("ざわ・・・ざわ・・・");
            _eventTextData.Add("{シーン}Prologue0");
            _eventTextData.Add("{人物}BusinessMan2");
            _eventTextData.Add("{SendScene}");
            _eventTextData.Add("謎の外人「トキョ」");
            _eventTextData.Add("{SE}スタジアムの歓声1");
            _eventTextData.Add("謎の外人「2020年の開催地はTOKYOに決定しました」");
            _eventTextData.Add("{SE}スタジアムの歓声1");
            _eventTextData.Add("{人物}");
            _eventTextData.Add("{SendScene}");
            _eventTextData.Add("{横シーン}Prologue1");
            _eventTextData.Add("ウワアアアアア");
            _eventTextData.Add("{SE}スタジアムの歓声1");
            _eventTextData.Add("よっしゃああああああ！");
            _eventTextData.Add("わっしょい！！わっしょい！！");
            _eventTextData.Add("{SE}スタジアムの歓声1");
            _eventTextData.Add("やりましたね！{名前}さん！！！");
            _eventTextData.Add("念願叶ってついに我が国での開催ですよ！");
            _eventTextData.Add("{シーン}Prologue2");
            _eventTextData.Add("{SendScene}");
            _eventTextData.Add("{BGM}Endurance_Game");
            _eventTextData.Add("こうして国際大会を開催することになった");
            _eventTextData.Add("まるで夢のようだ。");
            _eventTextData.Add("若干の××はしていたが上手くいくとは。");
            _eventTextData.Add("絶対に成功させるぞ！！！");
            _eventTextData.Add("後に大きな問題が降りかかることは");
            _eventTextData.Add("この時はまだ知る由も無かった。");
        }

        /// <summary>
        /// シーン情報を登録する
        /// </summary>
        private void EventSceneSetting()
        {
            _sceneNames.Background = "Prologue0";
            _sceneNames.Foreground = "BusinessMan";
            _sceneNames.EffectDirection = "PrologueDesk";
        }

        /// <summary>
        /// 基本の処理順で関数を登録
        /// </summary>
        private void EnqueueFunctionName()
        {
            _functionQueue.Enqueue(nameof(_function.BeginPrologue));
        }

        /// <summary>
        /// 順番にターン内の関数を実行する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask EventLoop(CancellationTokenSource ct)
        {
            //開始前にセーブ
            _saveData.UpdateValue(_functionQueue, -1);
            _repositoryInterface.Save(_saveData);

            string func;
            System.Reflection.MethodInfo method;
            UniTask task;
            while (_functionQueue.Count != 0)
            {
                func = _functionQueue.Peek();
                method = _function.GetType().GetMethod(func);
                task = (UniTask)method.Invoke(_function, new object[] { ct });
                await task;
                await UniTask.NextFrame().Preserve();
                _functionQueue.Dequeue();
            }
        }
    }
}

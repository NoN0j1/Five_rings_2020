﻿using System;
using System.Collections.Generic;
using GameScene.Interface;
using GameScene.Parameter;
using Parameter;
using UniRx;

namespace GameScene.Event
{
    /// <summary>
    /// ターンイベントで行なう処理<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class TurnEventProcess : IDisposable, IEventNormalTurn
    {
        private Subject<object> _multiSubject = new Subject<object>();
        private Subject<int> _selectionSubject = new Subject<int>();
        private CompositeDisposable _disposables = new CompositeDisposable();

        public IObserver<object> TurnSubject => _multiSubject;

        /// <summary>
        /// 選ばれた選択肢を受け取った処理
        /// </summary>
        public IObservable<int> SelectNumObservable => _selectionSubject;

        //Interface実装 --------------------------------------------------------------------------
        public IObservable<IEnumerable<(ParameterType, int)>> StatusChangeRegister => _multiSubject.OfType<object, IEnumerable<(ParameterType, int)>>();

        public IObservable<Unit> StatusChangeStart => _multiSubject.OfType<object, Unit>();

        public IObservable<List<SelectionStatus>> SelectionDataObservable => _multiSubject.OfType<object, List<SelectionStatus>>();

        public IObservable<List<float>> SuccsessRateObservable => _multiSubject.OfType<object, List<float>>();

        public IObservable<List<Status>> StatusObservable => _multiSubject.OfType<object, List<Status>>();

        public IObserver<int> SelectionNumObserver => _selectionSubject;

        public void Dispose()
        {
            _disposables.Dispose();
        }
        //=========================================================================================
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using GameScene.Repository.Interface;
using Parameter;
using ScriptableObjects;
using ScriptableObjects.TargetData;
using TitleScene.Saving;

namespace GameScene.Event
{
    /// <summary>
    /// 目標イベントクラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class TargetEvent : IEvent
    {
        private class Function
        {
            /// <summary>
            /// 実行親
            /// </summary>
            private readonly TargetEvent _targetEvent = null;

            public Function(TargetEvent target)
            {
                _targetEvent = target;
            }

            /// <summary>
            /// イベント内容を送る
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask SendEventData(CancellationTokenSource ct)
            {
                _targetEvent.EventCommons.SendEventName(_targetEvent.TargetEventData.DataName);
                await _targetEvent.EventCommons.SendEventScene(_targetEvent.EventScene);
                await _targetEvent.EventCommons.SendEventText(_targetEvent.TargetEventData.TextList);
                _targetEvent.SaveData.UpdateValue(_targetEvent.FunctionQueue, (int)_targetEvent.ClearStatus);
            }

            /// <summary>
            /// 目標イベントの結果を送信する
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask SendResult(CancellationTokenSource ct)
            {
                _targetEvent.TargetProcessor.ResultObserver.OnNext(_targetEvent.ClearStatus);
                await _targetEvent.EventCommons.WaitAsyncTask();
            }

            /// <summary>
            /// 結果後のテキストを送信する
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask SendFinishedText(CancellationTokenSource ct)
            {
                if (_targetEvent.ClearStatus == TargetEventStatus.NotStart)
                {
                    return;
                }

                //結果後テキストを表示
                if (_targetEvent.ClearStatus == TargetEventStatus.Failed)
                {
                    await _targetEvent.EventCommons.SendEventText(_targetEvent.TargetEventData.FailureTextList);
                }
                else
                {
                    await _targetEvent.EventCommons.SendEventText(_targetEvent.TargetEventData.SuccessTextList);
                }
            }
        }
        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// セーブ内容
        /// </summary>
        private SaveEventInfo _saveData = null;

        /// <summary>
        /// 達成状況
        /// </summary>
        private TargetEventStatus _clearStatus = TargetEventStatus.NotStart;

        /// <summary>
        /// 目標ステータス
        /// </summary>
        private List<Status> _targetAbilityValue = new List<Status>();

        /// <summary>
        /// 読み込み元データ
        /// </summary>
        private TargetEventData _eventData = null;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        private EventCommonsProcess _commonsProcess = null;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        private TargetEventProcess _targetProcess = null;

        /// <summary>
        /// 関数名リスト
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// 関数実行クラス
        /// </summary>
        private Function _function = null;

        /// <summary>
        /// セーブ内容を取得する
        /// </summary>
        public SaveEventInfo SaveData => _saveData;

        /// <summary>
        /// イベントデータを取得する
        /// </summary>
        public TargetEventData TargetEventData => _eventData;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcess;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        public TargetEventProcess TargetProcessor => _targetProcess;

        /// <summary>
        /// クリア状況を取得する
        /// </summary>
        public TargetEventStatus ClearStatus => _clearStatus;

        /// <summary>
        /// 関数キューを取得する
        /// </summary>
        public Queue<string> FunctionQueue => _functionQueue;

        /// <summary>
        /// 目標能力値
        /// </summary>
        public List<Status> TargetValue => _targetAbilityValue;

        /// <summary>
        /// イベントシーン情報を取得する
        /// </summary>
        public EventDirectionScene EventScene => _eventData.EventDirectionScene;


        //interface実装--------------------------------------------------
        async UniTask IEvent.Run()
        {
            ResultDrawing();
            _commonsProcess.EventStart();
            await TargetEventLoop(_commonsProcess.CancellationToken);
        }
        //===============================================================



        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repository">リポジトリインターフェース</param>
        public TargetEvent(IGameRepository repository, EventCommonsProcess common, TargetEventProcess targetProcess)
        {
            _repositoryInterface = repository;
            _commonsProcess = common;
            _targetProcess = targetProcess;
            _function = new Function(this);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="eventData">読み込んだ目標イベントデータ</param>
        public void Initialize(in TargetEventData eventData)
        {
            if(_functionQueue.Count != 0)
            {
                return;
            }
            _clearStatus = TargetEventStatus.NotStart;
            _eventData = eventData;
            _saveData = new SaveEventInfo(_eventData.DataName, GameEventType.Target);
            EnqueueFunctionName();
            InitializeTargetParameter(eventData.Parameter);
            _targetProcess.UpdateTargetParameter(_targetAbilityValue);
        }


        /// <summary>
        /// セーブデータから初期化する
        /// </summary>
        /// <param name="eventData">読み込んだ目標イベントデータ</param>
        /// <param name="saveData">セーブデータ</param>
        public void Initialize(in TargetEventData eventData, in UserData save)
        {
            _eventData = eventData;
            _saveData = save.PickedEvent;
            _clearStatus = (_saveData.Result == -1 ? TargetEventStatus.NotStart : (TargetEventStatus)_saveData.Result);
            _functionQueue = _saveData.FunctionQueue;
            InitializeTargetParameter(eventData.Parameter);
            _targetProcess.UpdateTargetParameter(_targetAbilityValue);
        }

        /// <summary>
        /// 基本の処理順で関数を登録
        /// </summary>
        private void EnqueueFunctionName()
        {
            _functionQueue.Enqueue(nameof(_function.SendEventData));
            _functionQueue.Enqueue(nameof(_function.SendResult));
            _functionQueue.Enqueue(nameof(_function.SendFinishedText));
        }

        /// <summary>
        /// 目標ステータス値DataをEditerDataからゲーム内で使用する形にする
        /// </summary>
        /// <param name="dataParameter">Editer上で扱うデータ</param>
        private void InitializeTargetParameter(in TargetEventData.TargetParameterDictionary dataParameter)
        {
            _targetAbilityValue.Clear();
            for (var t = 0; t < Enum.GetValues(typeof(ParameterType)).Length; ++t)
            {
                _targetAbilityValue.Add(new Status((ParameterType)t, 0));
            }

            foreach (var p in dataParameter)
            {
                _targetAbilityValue[(int)p.Key].Value = p.Value;
            }
        }


        /// <summary>
        /// 抽籤結果を取得する
        /// </summary>
        private void ResultDrawing()
        {
            if (_clearStatus == TargetEventStatus.NotStart)
            {
                _clearStatus = EventCommons.Calculator.GetLotteryResults;
            }
        }

        /// <summary>
        /// 順番にターン内の関数を実行する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask TargetEventLoop(CancellationTokenSource ct)
        {
            //開始前にセーブ
            _saveData.UpdateValue(_functionQueue, (int)_clearStatus);
            _repositoryInterface.Save(_saveData);

            string func;
            System.Reflection.MethodInfo method;
            UniTask task;
            while (_functionQueue.Count != 0)
            {
                func = _functionQueue.Peek();
                method = _function.GetType().GetMethod(func);
                task = (UniTask)method.Invoke(_function, new object[] { ct });
                await task;
                await UniTask.NextFrame().Preserve();
                _functionQueue.Dequeue();
            }
        }
    }
}

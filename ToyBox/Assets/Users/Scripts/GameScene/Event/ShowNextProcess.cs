﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using UniRx;

namespace GameScene.Event
{
    public sealed class ShowNextProcess : INextTarget
    {
        /// <summary>
        /// 目標ステータス一覧
        /// </summary>
        private Subject<List<Status>> _statusListSubject = new Subject<List<Status>>();

        public IObservable<List<Status>> NextTargetStatusObservable => _statusListSubject;

        public void SendTargetAbilityValue(List<Status> target)
        {
            _statusListSubject.OnNext(target);
        }
    }
}

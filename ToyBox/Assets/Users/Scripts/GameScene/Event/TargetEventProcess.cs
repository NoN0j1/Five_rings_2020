﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameScene.Parameter;
using GameScene.Interface;
using UniRx;
using Cysharp.Threading.Tasks;
using Parameter;

namespace GameScene.Event
{
    public sealed class TargetEventProcess : IDisposable, IEventTargetTurn
    {
        private Subject<TargetEventStatus> _subject = new Subject<TargetEventStatus>();
        private List<Status> _targetParameter = null;
        private CompositeDisposable _disposables = new CompositeDisposable();

        /// <summary>
        /// リザルト通知を行なえるObserverを取得する
        /// </summary>
        public IObserver<TargetEventStatus> ResultObserver => _subject;

        //Interface実装 --------------------------------------------------------------------------
        public List<Status> TargetParameter => _targetParameter;

        public IObservable<TargetEventStatus> ClearStatusObservable => _subject;

        public void Dispose()
        {
            _disposables.Dispose();
        }
        //=========================================================================================

        public void  UpdateTargetParameter(List<Status> updateValues)
        {
            _targetParameter = updateValues;
        }
    }
}

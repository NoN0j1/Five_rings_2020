using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using GameScene.Usecase;
using GameScene.Resource;
using ScriptableObjects;
using TitleScene.Saving;
using UniRx;
using UnityEngine;

namespace GameScene.Event
{
    /// <summary>
    /// イベントで共通で行なう処理<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class EventCommonsProcess : IDisposable, IEventCommon
    {
        private readonly CancellationTokenSource _cancellationTokenSource = null;
        private List<UniTask> _asyncTasks = new List<UniTask>();
        private Subject<string> _eventNameSubject = new Subject<string>();
        private Subject<string> _eventTextSubject = new Subject<string>();
        private Subject<object> _eventSceneSubject = new Subject<object>();
        private Subject<string> _bgmSubject = new Subject<string>();
        private Subject<string> _seSubject = new Subject<string>();
        private Subject<Unit> _StartEventSubject = new Subject<Unit>();
        private CompositeDisposable _disposables = null;
        private Calculator _calculator = null;
        private EventSceneSprite _eventScene = null;


        public Calculator Calculator => _calculator;

        public CompositeDisposable Disposables => _disposables;

        //Interface実装 --------------------------------------------------------------------------
        public CancellationTokenSource CancellationToken => _cancellationTokenSource;

        public IObservable<Unit> StartEventObservable => _StartEventSubject;

        public List<UniTask> AsyncTasks => _asyncTasks;

        public SaveEventInfo SaveData => throw new NotImplementedException();

        public IObservable<string> EventNameObservable => _eventNameSubject;

        public IObservable<string> TextObservable => _eventTextSubject;

        public IObservable<EventSceneSprite> EventSceneObservable => _eventSceneSubject.OfType<object, EventSceneSprite>();
        
        public IObservable<Sprite> ScrollSceneObservable => _eventSceneSubject.OfType<object, Sprite>();

        public IObservable<string> BgmObservable => _bgmSubject;

        public IObservable<string> SeObservable => _seSubject;

        public void AddAsyncTask(UniTask task)
        {
            _asyncTasks.Add(task);
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }
        //=========================================================================================

        public void EventStart()
        {
            _StartEventSubject.OnNext(Unit.Default);
        }

        public EventCommonsProcess(CompositeDisposable disposables, CancellationTokenSource ct, Calculator calculator)
        {
            _disposables = disposables;
            _cancellationTokenSource = ct;
            _calculator = calculator;
            _eventScene = new EventSceneSprite();
        }

        public async UniTask WaitAsyncTask()
        {
            await UniTask.WhenAll(_asyncTasks);
            _asyncTasks.Clear();
        }

        public void SendEventName(string name)
        {
            _eventNameSubject.OnNext(name);
        }

        public async UniTask SendEventText(List<string> textList)
        {
            string temp;
            EventDirectionScene scene = new EventDirectionScene();
            foreach (string s in textList)
            {
                temp = s.Replace("{名前}", SaveSystem.PubInstance.PubUserData.UserName);
                if(temp.Contains("{SendScene}"))
                {
                    await SendEventScene(scene);
                    continue;
                }
                if(temp.Contains("{シーン}"))
                {
                    temp = s.Replace("{シーン}", "");
                    scene.Background = temp;
                    continue;
                }
                if (temp.Contains("{人物}"))
                {
                    temp = s.Replace("{人物}", "");
                    scene.Foreground = temp;
                    continue;
                }
                if (temp.Contains("{小物}"))
                {
                    temp = s.Replace("{小物}", "");
                    scene.EffectDirection = temp;
                    continue;
                }
                if (temp.Contains("{横シーン}"))
                {
                    temp = s.Replace("{横シーン}", "");
                    await SendSideScrollScene(temp);
                    continue;
                }
                if(temp.Contains("{BGM}"))
                {
                    temp = s.Replace("{BGM}", "");
                    _bgmSubject.OnNext(temp);
                    continue;
                }
                if(temp.Contains("{SE}"))
                {
                    temp = s.Replace("{SE}", "");
                    _seSubject.OnNext(temp);
                    continue;
                }
                _eventTextSubject.OnNext(temp);
                await WaitAsyncTask();
                await UniTask.NextFrame(_cancellationTokenSource.Token).Preserve();
            }
        }

        public async UniTask SendEventScene(EventDirectionScene sceneNames)
        {
            await _eventScene.LoadResource(sceneNames);
            _eventSceneSubject.OnNext(_eventScene);
        }

        public async UniTask SendSideScrollScene(string sceneNames)
        {
            Sprite sp = await ResourceManager.LoadAsset<Sprite>(sceneNames);
            _eventSceneSubject.OnNext(sp);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using GameScene.Repository.Interface;
using Parameter;
using ScriptableObjects.TargetData;
using UniRx;

namespace GameScene.Event
{
    /// <summary>
    /// 次回イベントクラス<br/>
    /// 作成者:川野<br/>
    /// 最終更新:2021-07-29
    /// </summary>
    public sealed class ShowNextTarget : IEvent
    {
        /// <summary>
        /// イベント内で呼び出される関数キューの実装を行なうクラス<br/>
        /// 作成者 : 川野<br/>
        /// 最終更新 : 2021-07-29
        /// </summary>
        private sealed class Function
        {
            /// <summary>
            /// 実行親
            /// </summary>
            private readonly ShowNextTarget _showNextEvent = null;

            public Function(ShowNextTarget showNext)
            {
                _showNextEvent = showNext;
            }

            /// <summary>
            /// イベントテキストを送信する
            /// </summary>
            /// <param name="ct">キャンセルトークン</param>
            public async UniTask SendNextTarget(CancellationTokenSource ct)
            {
                UnityEngine.Debug.Log("目標ステータス送るよ！");
                _showNextEvent.ShowNextProcessor.SendTargetAbilityValue(_showNextEvent.TargetAbilityValue);
                _showNextEvent.EventCommons.Calculator.WeightingTargetValue(_showNextEvent.TargetAbilityValue);
                await _showNextEvent.EventCommons.WaitAsyncTask();
            }
        }

        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        private EventCommonsProcess _commonsProcess = null;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        private ShowNextProcess _showNext = null;

        /// <summary>
        /// 関数名リスト
        /// </summary>
        private Queue<string> _functionQueue = new Queue<string>();

        /// <summary>
        /// 目標ステータスリスト
        /// </summary>
        private List<Status> _targetAbilityValue = null;

        /// <summary>
        /// 関数実行クラス
        /// </summary>
        private Function _function = null;

        /// <summary>
        /// イベント共通処理クラス
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcess;

        /// <summary>
        /// ターンイベント専用処理クラス
        /// </summary>
        public ShowNextProcess ShowNextProcessor => _showNext;

        public List<Status> TargetAbilityValue => _targetAbilityValue;

        //interface実装--------------------------------------------------
        async UniTask IEvent.Run()
        {
            await EventLoop(_commonsProcess.CancellationToken);
        }
        //===============================================================

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="repository">リポジトリインターフェース</param>
        public ShowNextTarget(IGameRepository repository, EventCommonsProcess commons, ShowNextProcess showProcessor)
        {
            _repositoryInterface = repository;
            _commonsProcess = commons;
            _showNext = showProcessor;
            _function = new Function(this);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="data">ScriptableObjectから読み込んだイベントデータ</param>
        public void Initialize(TargetEventData data)
        {
            EnqueueFunctionName();
            InitializeTargetParameter(data.Parameter);
        }

        /// <summary>
        /// 目標ステータス値DataをEditerDataからゲーム内で使用する形にする
        /// </summary>
        /// <param name="dataParameter">Editer上で扱うデータ</param>
        private void InitializeTargetParameter(in TargetEventData.TargetParameterDictionary dataParameter)
        {
            int valueTypeNum = Enum.GetValues(typeof(ParameterType)).Length;
            _targetAbilityValue = new List<Status>(valueTypeNum);
            for (var t = 0; t < valueTypeNum; ++t)
            {
                _targetAbilityValue.Add(new Status((ParameterType)t, 0));
            }

            foreach (var p in dataParameter)
            {
                _targetAbilityValue[(int)p.Key].Value = p.Value;
            }
        }

        /// <summary>
        /// 基本の処理順で関数を登録
        /// </summary>
        private void EnqueueFunctionName()
        {
            _functionQueue.Enqueue(nameof(_function.SendNextTarget));
        }

        /// <summary>
        /// 順番にターン内の関数を実行する
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask EventLoop(CancellationTokenSource ct)
        {
            string func;
            System.Reflection.MethodInfo method;
            UniTask task;
            while (_functionQueue.Count != 0)
            {
                func = _functionQueue.Peek();
                method = _function.GetType().GetMethod(func);
                task = (UniTask)method.Invoke(_function, new object[] { ct });
                await task;
                await UniTask.NextFrame(ct.Token).Preserve();
                _functionQueue.Dequeue();
            }
        }
    }
}

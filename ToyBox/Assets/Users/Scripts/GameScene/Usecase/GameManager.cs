using Cysharp.Threading.Tasks;
using Common;
using GameScene.Event;
using GameScene.Interface;
using GameScene.Repository.Interface;
using UniRx;
using UnityEngine.SceneManagement;
using VContainer;
using VContainer.Unity;

namespace GameScene.Usecase
{
    /// <summary>
    /// ゲーム進行上でイベント通知を受け取る、またはawaitタスクを登録したい場合はこのinterfaceから。<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-29
    /// </summary>
    public interface IGameManager
    {
        IEventCommon CommonEventInterface { get; }

        IEventTargetTurn TargetEventInterface { get; }

        IEventNormalTurn NormalEventInterface { get; }

        INextTarget IntroduceNextTargetInterface { get; }

        IReactiveProperty<bool> StatusEnable { get; }

        /// <summary>
        /// ターン数表示
        /// </summary>
        ISubject<(uint, bool)> TurnDisplay { get; }
    }

    public class GameManager : IPostInitializable, IGameManager
    {
        private ReactiveProperty<bool> _statusWindowEnable = new ReactiveProperty<bool>(false);

        /// <summary>
        /// 発生イベント
        /// </summary>
        private IEvent _pickEvent = null;

        /// <summary>
        /// ターン数表示
        /// </summary>
        private Subject<(uint, bool)> _turnDisplay = new Subject<(uint, bool)>();

        /// <summary>
        /// ステータスウィンドウの旧表示状態
        /// </summary>
        private bool _oldEnable = false;

        /// <summary>
        /// リポジトリの情報にアクセスするインターフェース
        /// </summary>
        private readonly IGameRepository _repositoryInterface = null;

        /// <summary>
        /// フェード等を行なうキャンバス
        /// </summary>
        private CommonSceneCanvas _commonCanvas = null;

        //Interface実装 --------------------------------------------------------------------
        public IEventCommon CommonEventInterface => _repositoryInterface.CommonEventInterface;

        public IEventTargetTurn TargetEventInterface => _repositoryInterface.TargetEventInterface;

        public IEventNormalTurn NormalEventInterface => _repositoryInterface.NormalEventInterface;

        public IReactiveProperty<bool> StatusEnable => _statusWindowEnable;

        public ISubject<(uint, bool)> TurnDisplay => _turnDisplay;

        public INextTarget IntroduceNextTargetInterface => _repositoryInterface.NextTargetEventInterface;

        //==================================================================================

        /// <summary>
        /// コンストラクタ、VContainerからDIを行なう
        /// </summary>
        /// <param name="repositoryInterface">リポジトリInterface</param>
        [Inject]
        public GameManager(IGameRepository repositoryInterface, CommonSceneCanvas commonCanvas)
        {
            _repositoryInterface = repositoryInterface;
            _commonCanvas = commonCanvas;
        }

        /// <summary>
        /// VContainerのPostInitialize時に呼び出される初期化処理
        /// </summary>
        public void PostInitialize()
        {
            UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
            _repositoryInterface.InitializeEnd.Subscribe(_ => EntryGameLoop().Forget());
        }

        /// <summary>
        /// メインゲームループ
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        private async UniTask EntryGameLoop()
        {
            _commonCanvas.BlackFadeIn(1);
            while(_commonCanvas.IsFinishFadeIn == false)
            {
                if (_repositoryInterface.CancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }
                await UniTask.NextFrame(_repositoryInterface.CancellationTokenSource.Token);
            }

            while (true)
            {
                if (_repositoryInterface.CancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }
                _pickEvent = await _repositoryInterface.GetEvent();
                _oldEnable = _statusWindowEnable.Value;
                _statusWindowEnable.Value = (_pickEvent is TurnEvent || _pickEvent is TargetEvent);
                if(_statusWindowEnable.Value)
                {
                    _turnDisplay.OnNext((_repositoryInterface.RemainingTurn, _oldEnable & _statusWindowEnable.Value));
                }
                await _pickEvent.Run();
                await UniTask.NextFrame(_repositoryInterface.CancellationTokenSource.Token);
                _repositoryInterface.AddTurn();
                if (_pickEvent is Ending)
                {
                    break;
                }
            }

            _commonCanvas.WhiteFadeOut(1.5f);
            while (_commonCanvas.IsFinishFadeOut == false)
            {
                if (_repositoryInterface.CancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }
                await UniTask.NextFrame(_repositoryInterface.CancellationTokenSource.Token);
            }
            await SceneManager.LoadSceneAsync("ResultScene", LoadSceneMode.Single);
        }
    }
}
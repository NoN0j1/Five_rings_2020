﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameScene.Parameter;
using GameScene.Repository.Interface;
using Parameter;
using ScriptableObjects.SelectionData;
using UnityEngine;

namespace GameScene.Usecase
{
    /// <summary>
    /// ステータスや成功率などの計算を行なうクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class Calculator
    {
        /// <summary>
        /// 百分率
        /// </summary>
        private const int _percentage = 100;
        
        /// <summary>
        /// クリア評価の最大値
        /// </summary>
        private const float _evaluationMax = 1.5f;
        
        /// <summary>
        /// クリア評価の下限値
        /// </summary>
        private const float _evaluationMin = 0.5f;
        
        /// <summary>
        /// クリア抽籤範囲(1/nのn)
        /// </summary>
        private const int _lotteryRange = 10000;
        
        /// <summary>
        /// 目標ステータスの重み
        /// </summary>
        private List<(ParameterType, float)> _targetWeight = new List<(ParameterType, float)>();
        
        /// <summary>
        /// 目標イベントクリア率の分布(抽選用) 
        /// </summary>
        private List<int> _rateDistibution = new List<int>();

        /// <summary>
        /// リポジトリインターフェース
        /// </summary>
        private readonly IGameRepository _gameRepository = null;

        public Calculator(IGameRepository repository)
        {
            _gameRepository = repository;
        }

        /// <summary>
        /// クリア率の分布(表示用) 
        /// </summary>
        /// <return>目標イベントクリア率の分布を取得する(小数点有り)</return>
        public List<float> SuccsessRates
        {
            get 
            {
                List<float> ret = new List<float>();
                foreach (float value in _rateDistibution)
                {
                    ret.Add(value / _lotteryRange * _percentage);
                }
                return ret;
            } 
        }

        /// <summary>
        /// 現在ステータスから算出された目標イベントクリア分布を用いて判定を抽籤する
        /// </summary>
        /// <return>目標イベントのクリア判定</return>
        public TargetEventStatus GetLotteryResults
        {
            get
            {
                int pick = UnityEngine.Random.Range(0, _lotteryRange);
                TargetEventStatus ret = TargetEventStatus.NotStart;
                int range = 0;
                if(_rateDistibution.Count == 0)
                {
                    SuccessRateCalculation();
                }
                for (int i = _rateDistibution.Count - 1; i > 0; i--)
                {
                    ret++;
                    range += _rateDistibution[i];
                    if (pick < range)
                    {
                        break;
                    }
                }
                return ret;
            }
        }

        /// <summary>
        /// パラメータの計算を行なう
        /// </summary>
        /// <param name="selectValue">選択肢の変動情報</param>
        /// <returns>ステータス変化値</returns>
        public IEnumerable<(ParameterType, int)> CalculateParameterValue(SelectionStatus selectValue)
        {
            //計算後の値
            List<(ParameterType, int)> calculatedValue = new List<(ParameterType, int)>();
            //変化する値
            List<(ParameterType, int)> addValueList = new List<(ParameterType, int)>();
            List<Status> storageValues = _gameRepository.AbilityValues;
            List<Status> targetValues = _gameRepository.TargetEventInterface.TargetParameter;

            int total = 0;
            int storage = 0;
            float addValue = 0.0f;

            foreach (var selDictionary in selectValue.CalculationValue)
            {
                total = 0;
                storage = storageValues[(int)selDictionary.Key].Value;
                if (selDictionary.Value.Type == SelectionData.SelectionFluctuationValue.ValueType.Fixed)
                {
                    //固定値計算
                    addValue = selDictionary.Value.Value;
                    total = storage + (int)addValue;
                }
                else
                {
                    //目標ステータスから割合計算
                    if (selDictionary.Value.Value <= 0)
                    {
                        addValue = storageValues[(int)selDictionary.Key].Value * selDictionary.Value.Value * 0.01f;
                    }
                    else
                    {
                        if((int)selDictionary.Key == (int)ParameterType.Exitement)
                        {
                            addValue = 1000 * selDictionary.Value.Value * 0.01f;
                        }
                        else
                        {
                            addValue = targetValues[(int)selDictionary.Key].Value * selDictionary.Value.Value * 0.01f;
                        }
                    }
                    
                    total = storage + (int)addValue;
                }
                if(addValue == 0)
                {
                    continue;
                }
                calculatedValue.Add((selDictionary.Key, total));
                addValueList.Add((selDictionary.Key, (int)addValue));
            }

            //更新通知を送る
            _gameRepository.StoreParameterValueObserver.OnNext(calculatedValue);

            return addValueList;
        }

        /// <summary>
        /// 成功率の計算
        /// </summary>
        public void SuccessRateCalculation()
        {
            var targetValue = _gameRepository.TargetEventInterface.TargetParameter;
            var statusValue = _gameRepository.AbilityValues;

            List<float> achievementRate = new List<float>();
            float tempValue = 0;
            for (int i = 0; i < statusValue.Count; ++i)
            {
                if (targetValue[i].Value <= 0)
                {
                    achievementRate.Add(_evaluationMin);
                    continue;
                }
                tempValue = statusValue[i].Value / (float)targetValue[i].Value;
                int temp = i;
                var t = _targetWeight.Where(data => data.Item1 == targetValue[temp].ParameterType).ToList();
                if (t.Count != 0)
                {
                    tempValue += (tempValue - _evaluationMin) * t[0].Item2;
                }
                achievementRate.Add(Math.Max(0, Math.Min(tempValue, _evaluationMax)));
            }
            CreateSuccessDistribute(achievementRate);
        }

        /// <summary>
        /// 成功率の分布を作成する
        /// </summary>
        /// <param name="values">成功率の計算に使う数値データ</param>
        private void CreateSuccessDistribute(in List<float> values)
        {
            int typeLength = System.Enum.GetValues(typeof(TargetEventStatus)).Length - 1;
            List<List<float>> typeDistibution = new List<List<float>>();
            typeDistibution.Add(new List<float>()); //fail
            typeDistibution.Add(new List<float>()); //complete
            typeDistibution.Add(new List<float>()); //success
            float total = 0;

            foreach (float v in values)
            {
                total += v;
                if (v < _evaluationMin)
                {
                    typeDistibution[0].Add(v);
                    continue;
                }
                if (v < 1.0f)
                {
                    typeDistibution[1].Add(v);
                    continue;
                }
                if (1.0f <= v)
                {
                    typeDistibution[2].Add(v);
                }
            }
            float coefficient = total / values.Count;

            //データ分布数
            _rateDistibution = new List<int>();
            _rateDistibution.Add(0); //fail
            _rateDistibution.Add(0); //complete
            _rateDistibution.Add(0); //success
            _rateDistibution.Add(0); //greatSuccess

            //失敗率計算
            _rateDistibution[0] += (int)((coefficient * typeDistibution[0].Count / total) * _lotteryRange);
            _rateDistibution[0] += (int)((coefficient * (1.0f - _evaluationMin) / total) * _lotteryRange);

            //ギリ成功率計算
            float tempTotal = 0;
            foreach (var data in typeDistibution[1])
            {
                tempTotal += data;
            }
            _rateDistibution[1] = (int)((coefficient * _evaluationMin * tempTotal / total) * _lotteryRange);
            
            //大成功率計算
            tempTotal = 0;
            foreach (var data in typeDistibution[2])
            {
                tempTotal += data;
            }
            _rateDistibution[3] = (int)((coefficient * (_evaluationMax * typeDistibution[2].Count - tempTotal/total) / total) * _lotteryRange);
            
            //残りの確率が成功率
            _rateDistibution[2] = _lotteryRange - (_rateDistibution[0] + _rateDistibution[1] + _rateDistibution[3]);

            _rateDistibution.Reverse();
        }

        /// <summary>
        /// 目標ステータスの重み計算をする
        /// </summary>
        /// <param name="targets">目標ステータスデータ</param>
        public void WeightingTargetValue(in List<Parameter.Status> targets)
        {
            _targetWeight.Clear();
            var sorted = targets
                .Where(data => data.Value != 0)
                .Select(data =>
                {
                    int tempValue = data.Value;
                    if(data.ParameterType == ParameterType.Money)
                    {
                        tempValue = (int)(data.Value * 0.001f);
                    }
                    return new Parameter.Status(data.ParameterType, tempValue);
                })
                .OrderBy(data => data.Value)
                .ToList();

            if (sorted.Count == 0)
            {
                Debug.LogError("目標ステータスが設定されていません。");
                return;
            }

            const float defaultWeight = 1.0f;
            float weightValue = defaultWeight;
            int weightCount = 0;
            float before = sorted[0].Value;
            foreach (var data in sorted)
            {
                weightCount++;
                if (before != data.Value)
                {
                    weightValue = defaultWeight + 0.25f * weightCount;
                    before = data.Value;
                }
                _targetWeight.Add((data.ParameterType, weightValue));
            }
        }
    }
}

﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameScene.Parameter;
using Parameter;
using TitleScene.Saving;

namespace GameScene.Repository
{
    /// <summary>
    /// リポジトリ内にあるステータス関係のデータクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class DataStorage
    {
        /// <summary>
        /// ターン数
        /// </summary>
        private uint _turn = 0;

        /// <summary>
        /// 現在ステータス
        /// </summary>
        private List<Status> _status = new List<Status>();

        /// <summary>
        /// ステータス初期化用データ 
        /// </summary>
        private List<Status> _defaultValues = new List<Status>()
        {
            new Status(ParameterType.Money, 1000000),
            new Status(ParameterType.Influence, 3),
            new Status(ParameterType.Exitement, 2020),
            new Status(ParameterType.Leadership, 100),
            new Status(ParameterType.PoliticsPower, 100),
            new Status(ParameterType.RespondLevel, 100),
            new Status(ParameterType.Morality, 100),
            new Status(ParameterType.MediaReputation, 1000),
        };

        /// <summary>
        /// ターン数を取得する
        /// </summary>
        public uint Turn => _turn;

        /// <summary>
        /// ターン数を加算する
        /// </summary>
        public void AddTurn()
        {
            _turn++;
        }

        /// <summary>
        /// 現在ステータス一覧を取得する
        /// </summary>
        public List<Status> Parameters => _status;

        /// <summary>
        /// 初期化を行なう
        /// </summary>
        public void Initialize()
        {
            _status = _defaultValues;
            _turn = 0;
        }

        /// <summary>
        /// データ初期化を行なう(セーブデータから)
        /// </summary>
        /// <param name="save">セーブ時ステータス</param>
        public void InitializeFromSaveData(in UserData save)
        {
            foreach(var p in save.Parameter.Parameter)
            {
                _status.Add(new Status(p.Key, p.Value));
            }
            _turn = save.Turn;
        }

        /// <summary>
        /// パラメータの数値をデータストレージに書き込む
        /// </summary>
        /// <param name="updateValue">更新内容</param>
        public void Store(in (ParameterType type, int value) updateValue)
        {
            _status[(int)updateValue.Item1].Value = updateValue.value;
        }
    }
}

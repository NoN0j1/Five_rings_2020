﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Interface;
using GameScene.Parameter;
using Parameter;
using TitleScene.Saving;
using UniRx;

namespace GameScene.Repository.Interface
{
    /// <summary>
    /// ゲームリポジトリから取得するためのインターフェース
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public interface IGameRepository
    {
        /// <summary>
        /// リポジトリの初期化が完了したら実行されるものを登録する
        /// </summary>
        IObservable<Unit> InitializeEnd { get; }

        /// <summary>
        /// パラメータの更新
        /// </summary>
        IObserver<List<(ParameterType, int)>> StoreParameterValueObserver { get; }

        /// <summary>
        /// 非同期キャンセルトークン
        /// </summary>
        public CancellationTokenSource CancellationTokenSource { get; }

        /// <summary>
        /// 残りのターン数を取得する
        /// </summary>
        public uint RemainingTurn { get; }

        /// <summary>
        /// jsonファイルにセーブを行なう
        /// </summary>
        public void Save(SaveEventInfo save);

        /// <summary>
        /// 能力値を取得する
        /// </summary>
        public List<Status> AbilityValues { get; }

        /// <summary>
        /// イベントを取得する
        /// </summary>
        public UniTask<IEvent> GetEvent();

        /// <summary>
        /// イベント共通処理
        /// </summary>
        public IEventCommon CommonEventInterface { get; }

        /// <summary>
        /// 通常イベントInterface、通常イベント時に発生する通知を受けとることが出来る。
        /// </summary>
        public IEventNormalTurn NormalEventInterface { get; }

        /// <summary>
        /// 目標イベントInterface、目標ステータス値を取得できる。
        /// </summary>
        public IEventTargetTurn TargetEventInterface { get; }

        /// <summary>
        /// 次回目標イベントInterface、次回目標ステータス値を取得できる。
        /// </summary>
        public INextTarget NextTargetEventInterface { get; }

        /// <summary>
        /// ターン数を加算する
        /// </summary>
        public void AddTurn();
    }

}

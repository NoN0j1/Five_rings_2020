﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Event;
using GameScene.Interface;
using GameScene.Parameter;
using GameScene.Repository.Interface;
using GameScene.Usecase;
using Parameter;
using TitleScene.Saving;
using UniRx;
using VContainer.Unity;

namespace GameScene.Repository
{
    /// <summary>
    /// ゲームリポジトリクラス
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class GameRepository : IInitializable, IDisposable, IGameRepository
    {
        /// <summary>
        /// 初期化を通知するためのSubject
        /// </summary>
        private AsyncSubject<Unit> _initSubject = new AsyncSubject<Unit>();

        /// <summary>
        /// ストレージ内容を更新する通知を受け取る
        /// </summary>
        private Subject<List<(ParameterType, int)>> _storeParameterSubject = new Subject<List<(ParameterType, int)>>();

        /// <summary>
        /// パラメータストレージ
        /// </summary>
        private DataStorage _storage = new DataStorage();

        /// <summary>
        /// イベントストレージ
        /// </summary>
        private EventStorage _eventStorage = null;

        /// <summary>
        /// 後処理用
        /// </summary>
        private CompositeDisposable _disposable = new CompositeDisposable();

        /// <summary>
        /// 計算機
        /// </summary>
        private Calculator _calculator = null;

        /// <summary>
        /// asyncTaskのキャンセルトークン
        /// </summary>
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        /// <summary>
        /// 途中から再開か
        /// </summary>
        private bool _isContinue = false;

        /// <summary>
        /// 発生中イベント
        /// </summary>
        IEvent _pick = null;

        //interface実装---------------------------------------------------------
        public IObservable<Unit> InitializeEnd => _initSubject;

        public IObserver<List<(ParameterType, int)>> StoreParameterValueObserver => _storeParameterSubject;

        public CancellationTokenSource CancellationTokenSource => _cancellationTokenSource;

        public uint RemainingTurn => (uint)_eventStorage.NowTargetEvent.Turn - _storage.Turn;

        public List<Status> AbilityValues => _storage.Parameters;

        public IEventNormalTurn NormalEventInterface => _eventStorage.NormalEventProcessor;

        public IEventTargetTurn TargetEventInterface => _eventStorage.TargetEventProcessor;

        public IEventCommon CommonEventInterface => _eventStorage.EventCommons;

        public INextTarget NextTargetEventInterface => _eventStorage.ShowNextEvent;

        /// <summary>
        /// セーブを行なう
        /// </summary>
        /// <param name="save"></param>
        void IGameRepository.Save(SaveEventInfo save)
        {
            UserData saveData = SaveSystem.PubInstance.PubUserData;
            saveData.PickedEvent = save;
            saveData.IgnoreEvent = _eventStorage.IgnoreEventList;
            saveData.Parameter.UpdateValues(_storage.Parameters);
            saveData.Turn = _storage.Turn;
            saveData.TargetEventInfo.UpdateEventParameter(_eventStorage.TargetEventNames, _eventStorage.TargetEventStatusList);
            SaveSystem.PubInstance.Save();
        }

        /// <summary>
        /// イベントを取得する
        /// </summary>
        /// <returns>そのターンでのイベント</returns>
        async UniTask<IEvent> IGameRepository.GetEvent()
        {
            if (_isContinue)
            {
                _isContinue = false;
                _pick = await _eventStorage.GetSaveEvent();
                return _pick;
            }
            _pick = _eventStorage.GetNextEvent(_storage.Turn, _storage.Parameters[(int)ParameterType.Exitement].Value);
            return _pick;
        }

        void IGameRepository.AddTurn()
        {
            if (!(_pick is Ending || _pick is ShowNextTarget))
            {
                _storage.AddTurn();
            }
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }

        void IInitializable.Initialize()
        {
            UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
            _storeParameterSubject.Subscribe(values => StoreParameter(values));
            _calculator = new Calculator(this);
            _eventStorage = new EventStorage(this,_cancellationTokenSource, _calculator);
            InistializeRepository().Forget();
            _eventStorage.TargetEventProcessor.ClearStatusObservable
                .Subscribe(status => StoreExitement(status))
                .AddTo(_eventStorage.EventCommons.Disposables);
        }

        // ===============================================================================

        /// <summary>
        /// リポジトリの初期化
        /// </summary>
        public async UniTask InistializeRepository()
        {
            var loadData = SaveSystem.PubInstance.Load();
            
            if(CheckSaveData(loadData))
            {
                await InitializeFromSave(loadData);
            }
            else
            {
                await _eventStorage.Initialize();
                _storage.Initialize();
            }

            _initSubject.OnNext(Unit.Default);
            _initSubject.OnCompleted();
        }

        /// <summary>
        /// セーブデータの確認をする
        /// </summary>
        /// <param name="save">セーブデータ</param>
        /// <returns>true = 途中から復帰, false = はじめから</returns>
        private bool CheckSaveData(UserData save)
        {
            if (save == null)
            {       
                return false;
            }

            if (save.Turn == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// セーブデータから復元してゲームを始める
        /// </summary>
        /// <param name="save">セーブデータ</param>
        private async UniTask InitializeFromSave(UserData save)
        {
            _storage.InitializeFromSaveData(save);
            _isContinue = true;
            await _eventStorage.Initialize(save);
        }

        /// <summary>
        /// ストレージのステータス値を更新する
        /// </summary>
        /// <param name="storeValues">更新する値リスト</param>
        private void StoreParameter(List<(ParameterType, int)> storeValues)
        {
            foreach ((ParameterType, int) v in storeValues)
            {
                _storage.Store(v);
            }
        }

        private void StoreExitement(TargetEventStatus status)
        {
            (ParameterType, int) temp = (ParameterType.Exitement, _storage.Parameters[(int)ParameterType.Exitement].Value);
            switch(status)
            {
                case TargetEventStatus.Failed:
                    temp.Item2 -= 1000;
                    break;
                case TargetEventStatus.Completed:
                    temp.Item2 -= 300;
                    break;
                case TargetEventStatus.Success:
                    temp.Item2 += 100;
                    break;
                case TargetEventStatus.GreatSuccess:
                    temp.Item2 += 400;
                    break;
                default:
                    break;
            }
            _storage.Store(temp);
        }
    }
}
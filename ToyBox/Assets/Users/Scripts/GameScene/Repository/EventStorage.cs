﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Event;
using GameScene.Interface;
using GameScene.Resource;
using GameScene.Repository.Interface;
using GameScene.Usecase;
using Parameter;
using ScriptableObjects;
using ScriptableObjects.EventData;
using ScriptableObjects.TargetData;
using TitleScene.Saving;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UniRx;

namespace GameScene.Repository
{
    /// <summary>
    /// リポジトリ内のゲーム内イベント関係のデータクラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class EventStorage
    {
        /// <summary>
        /// 目標イベント番号
        /// </summary>
        private uint _targetNum = 0;

        /// <summary>
        /// 目標イベントリスト
        /// </summary>
        private List<TargetEventData> _targetEventList = null;

        /// <summary>
        /// 複数のDisposableを管理する
        /// </summary>
        private CompositeDisposable _disposables = new CompositeDisposable();

        /// <summary>
        /// プロローグイベント
        /// </summary>
        private Prologue _prologue = null;

        /// <summary>
        /// ターンイベント
        /// </summary>
        private TurnEvent _turnEvent = null;

        /// <summary>
        /// 目標イベント
        /// </summary>
        private TargetEvent _targetEvent = null;

        /// <summary>
        /// エンディングイベント
        /// </summary>
        private Ending _ending = null;

        /// <summary>
        /// 次回目標を提示するイベント
        /// </summary>
        private ShowNextTarget _next = null;

        /// <summary>
        /// 現在の目標イベントデータ
        /// </summary>
        private TargetEventData _nowTargetEvent = null;

        /// <summary>
        /// イベントで共通で使うもの
        /// </summary>
        private EventCommonsProcess _commonsProcessor = null;

        /// <summary>
        /// ターンイベントで共通で使うもの
        /// </summary>
        private TurnEventProcess _turnProcessor = new TurnEventProcess();

        /// <summary>
        /// 目標イベントで共通で使うもの
        /// </summary>
        private TargetEventProcess _targetProcessor = new TargetEventProcess();

        /// <summary>
        /// 次の目標イベントを提示するイベントで使うもの
        /// </summary>
        private ShowNextProcess _nextProcessor = new ShowNextProcess();

        // <summary>
        /// もう発生しないイベント一覧
        /// </summary>
        private List<string> _ignoreEvents = new List<string>();

        /// <summary>
        /// 目標イベントのAddressable一覧
        /// </summary>
        private List<string> _targetEventNames = new List<string>();

        /// <summary>
        /// 通常イベントのAddressable一覧
        /// </summary>
        private List<EventData> _normalEventList = new List<EventData>();

        /// <summary>
        /// 目標イベントの達成状況一覧
        /// </summary>
        private List<TargetEventStatus> _targetEventStatus = new List<TargetEventStatus>();

        /// <summary>
        /// 取得イベント
        /// </summary>
        private IEvent _pickEvent = null;

        /// <summary>
        /// 抽籤するイベントタグ
        /// </summary>
        private EventTag _tag = EventTag.Opening;

        /// <summary>
        /// 次に発生する目標を取得する
        /// </summary>
        public TargetEventData NowTargetEvent => _nowTargetEvent;

        /// <summary>
        /// イベントで共通で行う処理を取得する
        /// </summary>
        public EventCommonsProcess EventCommons => _commonsProcessor;

        /// <summary>
        /// 通常イベントで行う処理を取得する
        /// </summary>
        public TurnEventProcess NormalEventProcessor => _turnProcessor;

        /// <summary>
        /// 目標イベントで行う処理を取得する
        /// </summary>
        public TargetEventProcess TargetEventProcessor => _targetProcessor;

        /// <summary>
        /// 次目標イベントを提示するイベントで行なう処理を取得する
        /// </summary>
        public ShowNextProcess ShowNextEvent => _nextProcessor;

        /// <summary>
        /// 除外イベント一覧を取得する
        /// </summary>
        public List<string> IgnoreEventList => _ignoreEvents;

        /// <summary>
        /// 目標イベント一覧を取得する
        /// </summary>
        public List<string> TargetEventNames => _targetEventNames;

        /// <summary>
        /// 目標イベントの達成状況一覧
        /// </summary>
        public List<TargetEventStatus> TargetEventStatusList => _targetEventStatus;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="ct">キャンセルトークン</param>
        /// <param name="calculator">計算クラス</param>
        public EventStorage(IGameRepository repsotory, CancellationTokenSource ct, Calculator calculator)
        {
            _commonsProcessor = new EventCommonsProcess(_disposables, ct, calculator);
            _prologue = new Prologue(repsotory, _commonsProcessor);
            _turnEvent = new TurnEvent(repsotory, _commonsProcessor, _turnProcessor);
            _targetEvent = new TargetEvent(repsotory, _commonsProcessor, _targetProcessor);
            _ending = new Ending(repsotory, _commonsProcessor);
            _next = new ShowNextTarget(repsotory, _commonsProcessor, _nextProcessor);
            _targetProcessor.ClearStatusObservable
                .Subscribe(status =>
                {
                    _targetEventStatus[(int)_targetNum - 1] = status;
                })
                .AddTo(_disposables);

        }

        /// <summary>
        /// イベントの初期化をする
        /// </summary>
        public async UniTask Initialize()
        {
            var loadData = await Addressables.LoadAssetsAsync<EventData>("EventData", null);
            _normalEventList.AddRange(loadData);
            Debug.Log("EventData数:"+ _normalEventList.Count);

            await SortTargetEvent();
            _nowTargetEvent = _targetEventList[(int)_targetNum];

            TargetEventStatus[] status = new TargetEventStatus[_targetEventNames.Count];
            _targetEventStatus.AddRange(status);
        }

        /// <summary>
        /// セーブデータからイベントを初期化する
        /// </summary>
        /// <param name="save">セーブデータ</param>
        public async UniTask Initialize(UserData save)
        {
            var loadData = await Addressables.LoadAssetsAsync<EventData>("EventData", null);
            _normalEventList.AddRange(loadData);
            _ignoreEvents = save.IgnoreEvent;
            RemoveIgnoreEvent();
            await ConvertSaveData(save.TargetEventInfo);
            _nowTargetEvent = _targetEventList[(int)_targetNum];
            _targetEvent.Initialize(NowTargetEvent);
        }

        /// <summary>
        /// 次に発生させるイベントを取得する
        /// </summary>
        /// <param name="turn">現在のターン数</param>
        /// <returns>発生させるイベント</returns>
        public IEvent GetNextEvent(uint turn, int ExitementValue)
        {
            if(ExitementValue <= 0)
            {
                _ending.Initialize(0);
                return _ending;
            }
            if (_pickEvent == null)
            {
                _pickEvent = _prologue;
                return _pickEvent;
            }

            if(_pickEvent is ShowNextTarget)
            {
                _targetNum++;
                if (_targetEvent == null)
                {
                    _ending.Initialize(1);
                    return _ending;
                }
            }
            
            if (_pickEvent is TargetEvent || _pickEvent is Prologue)
            {
                var nextTarget = GetNextTargetEvent();
                if (nextTarget != null)
                {
                    _next.Initialize(nextTarget);
                }
                
                _pickEvent = _next;
                return _pickEvent;
            }

            if (NowTargetEvent.Turn <= (int)turn)
            {
                _pickEvent = _targetEvent;
                return _pickEvent;
            }

            _turnEvent.Initialize(GetNormalEvent());
            _pickEvent = _turnEvent;
            return _pickEvent;
        }

        /// <summary>
        /// セーブデータからイベントを取得する
        /// </summary>
        /// <returns>セーブ時に実行していたイベント</returns>
        public async UniTask<IEvent> GetSaveEvent()
        {
            UserData saveData = SaveSystem.PubInstance.PubUserData;
            switch (saveData.PickedEvent.EventType)
            {
                case GameEventType.Prologue:
                    _pickEvent = _prologue;
                    break;
                case GameEventType.Turn:
                    EventData data = await ResourceManager.LoadAsset<EventData>(saveData.PickedEvent.EventDataName);
                    _turnEvent.Initialize(data, saveData);
                    _pickEvent = _turnEvent;
                    break;
                case GameEventType.Target:
                    _targetEvent.Initialize(_nowTargetEvent, saveData);
                    _pickEvent = _targetEvent;
                    break;
                case GameEventType.Ending:
                    _ending.Initialize((uint)saveData.PickedEvent.Result);
                    _pickEvent = _ending;
                    break;
            }
            _targetNum++;
            _nextProcessor.SendTargetAbilityValue(_targetEvent.TargetValue);
            return _pickEvent;
        }

        /// <summary>
        /// 次に発生する通常イベントを取得する
        /// </summary>
        /// <returns>通常イベント</returns>
        public EventData GetNormalEvent()
        {
            var table = _normalEventList.Where(data => (data.Tag & _tag) != 0).ToList();
            int evNum = UnityEngine.Random.Range(0, table.Count());
            EventData getEvent = table[evNum];
            if ((getEvent.Tag & EventTag.GeneralPurpose) != EventTag.GeneralPurpose)
            {
                if (!_ignoreEvents.Contains(getEvent.DataName))
                {
                    _ignoreEvents.Add(getEvent.DataName);
                    _normalEventList.Remove(getEvent);
                }
            }
            return getEvent;
        }

        /// <summary>
        /// 次の目標イベントを取得する
        /// </summary>
        /// <returns>目標イベント</returns>
        public TargetEventData GetNextTargetEvent()
        {
            if (_targetEventList.Count <= _targetNum)
            {
                _targetEvent = null;
                return null;
            }
            TargetEventData ret = _targetEventList[(int)_targetNum];
            if(_targetNum == 2)
            {
                _tag = EventTag.Middle;
            }
            else if(_targetNum == 4)
            {
                _tag = EventTag.Final;
            }
            _nowTargetEvent = ret;
            _targetEvent.Initialize(_nowTargetEvent);
            return ret;
        }

        /// <summary>
        /// セーブデータから目標状況を復旧する
        /// </summary>
        /// <param name="save"></param>
        private async UniTask ConvertSaveData(SaveTargetEvent save)
        {
            bool isSet = false;
            for (int i = 0; i < save.EventList.Count; ++i)
            {
                _targetEventNames.Add(save.EventList[i].EventName);
                _targetEventStatus.Add(save.EventList[i].EventStatus);
                if(save.EventList[i].EventStatus == TargetEventStatus.NotStart && isSet == false)
                {
                    _targetNum = (uint)i;
                    isSet = true;
                }
            }
            _targetEventList = new List<TargetEventData>();
            _targetEventList.AddRange(await LoadTargetEvent());
        }

        /// <summary>
        /// 除外イベントに含まれるものを発生する一覧から取り除く
        /// </summary>
        private void RemoveIgnoreEvent()
        {
            List<EventData> tempDataList = new List<EventData>();
            tempDataList.AddRange(_normalEventList.ToArray());

            foreach (EventData data in tempDataList)
            {
                if (_ignoreEvents.Contains(data.DataName))
                {
                    _normalEventList.Remove(data);
                }
            }
        }

        /// <summary>
        /// 目標イベントを発生ターン順に並び変える。<br/>
        /// 同じターンに複数登録が場合はランダムに選択する
        /// </summary>
        private async UniTask SortTargetEvent()
        {
            var dataList = await Addressables.LoadAssetsAsync<TargetEventData>("TargetEventData", null);

            Dictionary<int, List<TargetEventData>> temp = new Dictionary<int, List<TargetEventData>>();
            foreach (TargetEventData data in dataList)
            {
                if (!temp.ContainsKey(data.Turn))
                {
                    temp.Add(data.Turn, new List<TargetEventData>());
                }
                temp[data.Turn].Add(data);
            }

            _targetEventList = new List<TargetEventData>();
            foreach (var v in temp.Values)
            {
                if (v.Count >= 2)
                {
                    while ((v.Count) > 1)
                    {
                        int remove = UnityEngine.Random.Range(0, v.Count);
                        v.RemoveAt(remove);
                    }
                }
                _targetEventList.Add(v[0]);
            }

            _targetEventList.Sort((a, b) => a.Turn - b.Turn);

            _targetEventNames.Clear();
            
            foreach (TargetEventData data in _targetEventList)
            {
                _targetEventNames.Add(data.DataName);
            }
        }

        /// <summary>
        /// 目標イベントデータを全て読み込む
        /// </summary>
        /// <returns>読み込んだ結果データ</returns>
        private async UniTask<TargetEventData[]> LoadTargetEvent()
        {
            List<UniTask<TargetEventData>> tasks = new List<UniTask<TargetEventData>>();
            foreach (string s in _targetEventNames)
            {
                tasks.Add(ResourceManager.LoadAsset<TargetEventData>(s));
            }
            return await UniTask.WhenAll(tasks);
        }
    }
}
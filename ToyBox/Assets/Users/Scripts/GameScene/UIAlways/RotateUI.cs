using UnityEngine;

namespace GameScene.UIAlways
{
    /// <summary>
    /// UIを回転させるだけのクラス
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-30
    /// </summary>
    public sealed class RotateUI : MonoBehaviour
    {
        public Vector3 RotateAngle = new Vector3(0, 0, -0.1f);
        private RectTransform _rectTransform = null;

        void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        private void FixedUpdate()
        {
            _rectTransform.Rotate(RotateAngle);
        }
    }
}
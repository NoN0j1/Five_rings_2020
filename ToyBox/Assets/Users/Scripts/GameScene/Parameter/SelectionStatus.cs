﻿using ScriptableObjects.SelectionData;

namespace GameScene.Parameter
{
    /// <summary>
    /// 選択肢に含まれるステータス情報クラス<br/>
    /// 作成者：川野
    /// </summary>
    public sealed class SelectionStatus
    {
        private string _text = "NoData";
        private SelectionData.SelectionFluctuationValueDictionary _calculationValue = null;

        /// <summary>
        /// 選択肢として表示する文章
        /// </summary>
        public string Text => _text;

        /// <summary>
        /// 選択肢に含まれる変動ステータス情報
        /// </summary>
        public SelectionData.SelectionFluctuationValueDictionary CalculationValue => _calculationValue;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="calValue">データベース上の変動ステータス情報</param>
        /// <param name="text">選択肢のテキスト</param>
        public SelectionStatus(SelectionData.SelectionFluctuationValueDictionary calValue, string text)
        {
            _calculationValue = calValue;
            _text = text;
        }
    }
}

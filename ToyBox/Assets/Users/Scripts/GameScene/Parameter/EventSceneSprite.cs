﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameScene.Resource;
using ScriptableObjects;
using UnityEngine;

namespace GameScene.Parameter
{
    /// <summary>
    /// イベントのシーン情報クラス<br/>
    /// 作成者 : 川野<br/>
    /// 最終更新 : 2021-07-28
    /// </summary>
    public sealed class EventSceneSprite
    {
        private Sprite _background = null;
        private Sprite _foreground = null;
        private Sprite _effect = null;

        /// <summary>
        /// 背景
        /// </summary>
        public Sprite Background => _background;

        /// <summary>
        /// 前景(小物など)
        /// </summary>
        public Sprite Foreground => _foreground;

        /// <summary>
        /// 演出用(エフェクトSprite画像をループ再生する等)
        /// </summary>
        public Sprite Accessory => _effect;

        /// <summary>
        /// イベントで使用されるリソースの事前読み込み
        /// </summary>
        /// <param name="loadName">EditorTool上で設定されたシーン名</param>
        public async UniTask LoadResource(EventDirectionScene loadName)
        {
            if(loadName == null)
            {
                return;
            }

            List<int> attachNum = new List<int>();
            List<UniTask<Sprite>> tasks = new List<UniTask<Sprite>>();

            if (loadName.Background != "Dummy" && loadName.Background != string.Empty)
            {
                tasks.Add(ResourceManager.LoadAsset<Sprite>(loadName.Background));
                attachNum.Add(0);
            }
            else
            {
                _background = null;
            }

            if (loadName.Foreground != "Dummy" && loadName.Foreground != string.Empty)
            {
                tasks.Add(ResourceManager.LoadAsset<Sprite>(loadName.Foreground));
                attachNum.Add(1);
            }
            else
            {
                _foreground = null;
            }

            if (loadName.EffectDirection != "Dummy" && loadName.EffectDirection != string.Empty)
            {
                tasks.Add(ResourceManager.LoadAsset<Sprite>(loadName.EffectDirection));
                attachNum.Add(2);
            }
            else
            {
                _effect = null;
            }

            var get = await UniTask.WhenAll(tasks);
            int elem = 0;
            foreach (Sprite sprite in get)
            {
                if (attachNum[elem] == 0)
                {
                    _background = sprite;
                }
                else if (attachNum[elem] == 1)
                {
                    _foreground = sprite;
                }
                else
                {
                    _effect = sprite;
                }
                elem++;
            }
        }
    }
}

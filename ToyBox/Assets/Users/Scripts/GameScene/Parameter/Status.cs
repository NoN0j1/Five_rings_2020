﻿using System;
using Parameter;

namespace GameScene.Parameter
{
    /// <summary>
    /// ゲーム内ステータス値クラス<br/>
    /// 作成者：川野
    /// </summary>
    [Serializable]
    public sealed class Status
    {
        private readonly ParameterType _parameterType;

        /// <summary>
        /// ステータス種別
        /// </summary>
        public ParameterType ParameterType => _parameterType;

        /// <summary>
        /// 値
        /// </summary>
        public int Value { get; set; } = 0;

        /// <summary>
        /// コンストラクタ、種別と値を決める
        /// </summary>
        /// <param name="type">ステータス種別</param>
        /// <param name="defaultValue">値</param>
        public Status(ParameterType type, int defaultValue)
        {
            _parameterType = type;
            Value = defaultValue;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using GameScene.Parameter;
using Parameter;
using TitleScene.Saving;
using UniRx;
using UnityEngine;

namespace GameScene.Interface
{
    /// <summary>
    /// 全てのイベントが共通して保持するイベント実行インターフェース<br/>
    /// 作成者 : 川野
    /// </summary>
    public interface IEvent
    {
        /// <summary>
        /// /// イベントを開始する<br/>
        /// </summary>
        public UniTask Run();
    }


    /// <summary>
    /// イベント内容に関わらず通知が発生するもののインターフェース<br/>
    /// 作成者 : 川野
    /// </summary>
    public interface IEventCommon
    {
        /// <summary>
        /// キャンセルトークンソースを取得する
        /// </summary>
        CancellationTokenSource CancellationToken { get; }

        /// <summary>
        /// 非同期タスク一覧
        /// </summary>
        List<UniTask> AsyncTasks { get; }

        /// <summary>
        /// セーブする項目を取得する
        /// </summary>
        SaveEventInfo SaveData { get; }

        /// <summary>
        /// イベント開始通知を受け取るだけ
        /// </summary>
        public IObservable<Unit> StartEventObservable { get; }

        /// <summary>
        /// イベント名を受け取った処理をしたい場合にSubscribeする
        /// </summary>
        public IObservable<string> EventNameObservable { get; }

        /// <summary>
        /// イベント内テキストを受け取りたい場合にSubscribeする
        /// </summary>
        public IObservable<string> TextObservable { get; }

        /// <summary>
        /// シーン情報を受け取りたい場合にSubscribeする
        /// </summary>
        public IObservable<EventSceneSprite> EventSceneObservable { get; }

        /// <summary>
        /// 横に流れるシーン情報を受け取りたい場合にSubscribeする
        /// </summary>
        public IObservable<Sprite> ScrollSceneObservable { get; }

        /// <summary>
        /// BGM名を受け取りたい場合にSubscribeする
        /// </summary>
        public IObservable<string> BgmObservable { get; }

        /// <summary>
        /// 効果音名を受け取りたい場合にSubscribeする
        /// </summary>
        public IObservable<string> SeObservable { get; }

        /// <summary>
        /// 非同期処理を待ってほしい場合に追加する
        /// </summary>
        /// <param name="task">追加したいタスク</param>
        public void AddAsyncTask(UniTask task);
    }

    /// <summary>
    /// 通常イベント時に発生する通知を受け取れるインターフェース<br/>
    /// 作成者 : 川野
    /// </summary>
    public interface IEventNormalTurn
    {
        /// <summary>
        /// ステータスの変更内容を通知する
        /// </summary>
        public IObservable<IEnumerable<(ParameterType, int)>> StatusChangeRegister { get; }
        /// <summary>
        /// ステータスの変動演出開始を通知する
        /// </summary>
        public IObservable<Unit> StatusChangeStart { get; }
        /// <summary>
        /// 選択肢情報の通知を受け取る
        /// </summary>
        public IObservable<List<SelectionStatus>> SelectionDataObservable { get; }
        /// <summary>
        /// 成功分布の通知を受け取る
        /// </summary>
        public IObservable<List<float>> SuccsessRateObservable { get; }
        /// <summary>
        /// 現在ステータスの通知を受け取る
        /// </summary>
        public IObservable<List<Status>> StatusObservable { get; }
        /// <summary>
        /// 選んだ選択肢番号をイベントに対して通知する
        /// </summary>
        public IObserver<int> SelectionNumObserver { get; }
    }

    /// <summary>
    /// 目標イベント時に通知が発生するもののインターフェース<br/>
    /// 作成者 : 川野
    /// </summary>
    public interface IEventTargetTurn
    {
        /// <summary>
        /// 目標ステータスを取得する
        /// </summary>
        List<Status> TargetParameter { get; }

        /// <summary>
        /// 抽籤されたクリア結果通知を受け取る
        /// </summary>
        public IObservable<TargetEventStatus> ClearStatusObservable { get; }
    }

    /// <summary>
    /// 次の目標を示して目標ステータス等を更新するインターフェース
    /// 作成者 : 川野
    /// </summary>
    public interface INextTarget
    {
        /// <summary>
        /// 次の目標ステータスを受け取る
        /// </summary>
        public IObservable<List<Status>> NextTargetStatusObservable { get; }
    }
}
using UnityEngine;

namespace Attribute
{
    /// <summary>
    /// 演出シーンをインスペクタ上でポップアップで選択できるようにするAttribute<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class DirectionScenePopupAttribute : PropertyAttribute
    {
        // ラベル
        private string _label = string.Empty;

        // ポップアップで表示したいグループ名
        private string _groupName = string.Empty;

        public string Label => _label;

        public string GroupName => _groupName;

        public DirectionScenePopupAttribute(string label, string groupName)
        {
            _label = label;
            _groupName = groupName;
        }
    }
}

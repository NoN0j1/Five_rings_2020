using UnityEngine;

namespace Attribute
{
    /// <summary>
    /// イベントタグをインスペクタ上でビットフィールドで選択できるようにするAttribute<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class EventTagAttribute : PropertyAttribute
    {
    }
}

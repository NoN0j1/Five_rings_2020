using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Common
{
    [RequireComponent(typeof(Image))]
    public sealed class CommonSceneCanvas : MonoBehaviour
    {
        private float _targetTime = 0.0f;
        private bool _isFinishFadeIn = false;
        private bool _isFinishFadeOut = false;
        private Sequence _sequence = null;
        private Image _image = null;

        public bool IsFinishFadeIn => _isFinishFadeIn;
        public bool IsFinishFadeOut => _isFinishFadeOut;

        void Awake()
        {
            _image = GetComponent<Image>();
            if(SceneManager.GetActiveScene().name == "GameScene")
            {
                _image.color = Color.white;
            }
            else
            {
                _image.color = Color.white;
            }
        }

        /// <summary>
        /// 白から段々と透明になる
        /// </summary>
        public void WhiteFadeIn(float time)
        {
            _isFinishFadeIn = false;
            _sequence = DOTween.Sequence();
            _image.color = Color.white;
            _sequence.Append(_image.DOFade(0, time));
            _sequence.OnComplete(() => _isFinishFadeIn = true);
            _sequence.Play();
        }

        /// <summary>
        /// 透明から段々と白になる
        /// </summary>
        public void WhiteFadeOut(float time)
        {
            _isFinishFadeOut = false;
            _sequence = DOTween.Sequence();
            _image.color = new Color(1, 1, 1, 0);
            _sequence.Append(_image.DOFade(1, time).SetEase(Ease.InQuart));
            _sequence.OnComplete(() => _isFinishFadeOut = true);
            _sequence.Play();
        }

        /// <summary>
        /// 黒から段々と透明になる
        /// </summary>
        public void BlackFadeIn(float time)
        {
            _isFinishFadeIn = false;
            _sequence = DOTween.Sequence();
            _image.color = Color.black;
            _sequence.Append(_image.DOFade(0, time));
            _sequence.OnComplete(() => _isFinishFadeIn = true);
            _sequence.Play();
        }

        /// <summary>
        /// 透明から段々と黒になる
        /// </summary>
        public void BlackFadeOut(float time)
        {
            _isFinishFadeOut = false;
            _sequence = DOTween.Sequence();
            _image.color = new Color(0, 0, 0, 0);
            _sequence.Append(_image.DOFade(1, time).SetEase(Ease.InQuart));
            _sequence.OnComplete(() => _isFinishFadeOut = true);
            _sequence.Play();
        }
    }

}
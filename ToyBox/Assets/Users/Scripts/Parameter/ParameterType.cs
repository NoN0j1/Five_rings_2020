using System;
using UnityEngine;

namespace Parameter
{
    // パラメータタイプ
    [Serializable]
    public enum ParameterType
    {
        [InspectorName("お金")]
        Money,
        [InspectorName("影響力")]
        Influence,
        [InspectorName("期待度")]
        Exitement,
        [InspectorName("統率力")]
        Leadership,
        [InspectorName("政治力")]
        PoliticsPower,
        [InspectorName("対応力")]
        RespondLevel,
        [InspectorName("モラル")]
        Morality,
        [InspectorName("マスコミ評価")]
        MediaReputation
    }
}
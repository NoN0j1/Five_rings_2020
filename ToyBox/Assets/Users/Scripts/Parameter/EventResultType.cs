﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parameter
{
    /// <summary>
    /// 目標イベントの進行状況
    /// </summary>
    public enum TargetEventStatus
    {
        /// <summary>
        /// 開始前
        /// </summary>
        NotStart,

        /// <summary>
        /// 失敗
        /// </summary>
        Failed,

        /// <summary>
        /// ひとまず達成
        /// </summary>
        Completed,

        /// <summary>
        /// 成功！
        /// </summary>
        Success,

        /// <summary>
        /// 大成功！
        /// </summary>
        GreatSuccess
    }
}

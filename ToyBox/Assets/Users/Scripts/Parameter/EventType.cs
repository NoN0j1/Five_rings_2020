﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parameter
{
    public enum GameEventType
    {
        /// <summary>
        /// プロローグ
        /// </summary>
        Prologue,

        /// <summary>
        /// 通常イベント
        /// </summary>
        Turn,

        /// <summary>
        /// 目標イベント
        /// </summary>
        Target,

        /// <summary>
        /// その他イベント
        /// </summary>
        Other,
        
        /// <summary>
        /// エンディング
        /// </summary>
        Ending
    }
}

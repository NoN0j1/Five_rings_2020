using UnityEngine;
using UniRx;

namespace CorrectionUiSafeArea
{
    /// <summary>
    /// 画面比率の補正を行うModelクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [ExecuteInEditMode]
    public sealed class CorrectionUiSafeAreaModel
    {
        //左上アンカーの値の監視用
        private Vector2ReactiveProperty _anchorMinSubject = new Vector2ReactiveProperty();

        //右下アンカーの値の監視用
        private Vector2ReactiveProperty _anchorMaxSubject = new Vector2ReactiveProperty();

        public IReadOnlyReactiveProperty<Vector2> AnchorMin => _anchorMinSubject;

        public IReadOnlyReactiveProperty<Vector2> AnchorMax => _anchorMaxSubject;

        /// <summary>
        /// 機種ごとにセーフエリアを補正
        /// </summary>
        public void CorrectionSafeArea()
        {
            var safeArea = UnityEngine.Screen.safeArea;

            //左上アンカーの値を計算
            Vector2 anchorsMin = safeArea.position;

            anchorsMin.x /= UnityEngine.Screen.width;
            anchorsMin.y /= UnityEngine.Screen.height;

            //右下アンカーの値を計算
            Vector2 anchorsMax = safeArea.position + safeArea.size;

            anchorsMax.x /= UnityEngine.Screen.width;
            anchorsMax.y /= UnityEngine.Screen.height;

            _anchorMinSubject.Value = anchorsMin;
            _anchorMaxSubject.Value = anchorsMax;
        }
    }
}
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace CorrectionUiSafeArea
{
    /// <summary>
    /// 画面比率の補正のDIを行うクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [ExecuteInEditMode]
    public sealed class CorrectionUiSafeAreaVContainer : LifetimeScope
    {
        [SerializeField]
        private CorrectionUiSafeAreaView _viewObject;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<CorrectionUiSafeAreaModel>(Lifetime.Scoped);

            builder.RegisterInstance(_viewObject);

            builder.RegisterEntryPoint<CorrectionUiSafeAreaPresenter>(Lifetime.Scoped);
        }
    }
}

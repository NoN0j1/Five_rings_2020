using System;
using UnityEngine;
using UniRx;
using VContainer;
using VContainer.Unity;

namespace CorrectionUiSafeArea
{
    /// <summary>
    /// 画面比率の補正を行うPresenterクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [ExecuteInEditMode]
    public sealed class CorrectionUiSafeAreaPresenter : IPostInitializable, IDisposable
    {
        // モデル
        private readonly CorrectionUiSafeAreaModel _model;

        // ビュー
        private readonly CorrectionUiSafeAreaView _view;

        private CompositeDisposable _disposables = new CompositeDisposable();

        [Inject]
        public CorrectionUiSafeAreaPresenter(CorrectionUiSafeAreaModel model, CorrectionUiSafeAreaView view)
        {
            _model = model;

            _view = view;
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }

        /// <summary>
        /// 初期化直後に実行
        /// </summary>
        public void PostInitialize()
        {
            _model.AnchorMin.Subscribe(
                anchorMin => _view.SetAnchorMin(anchorMin)
                ).AddTo(_disposables);

            _model.AnchorMax.Subscribe(
                anchorMax => _view.SetAnchorMax(anchorMax)
                ).AddTo(_disposables);

            _view.CorrectionSafeArea.Subscribe(
                unit => _model.CorrectionSafeArea()
                ).AddTo(_disposables);
        }
    }
}

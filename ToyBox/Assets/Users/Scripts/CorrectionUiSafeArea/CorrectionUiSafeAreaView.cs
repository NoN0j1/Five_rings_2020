using System;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace CorrectionUiSafeArea
{
    /// <summary>
    /// 画面比率の補正を行うViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Image))]
    public sealed class CorrectionUiSafeAreaView : MonoBehaviour
    {
        // セーフエリアパネルのトランスフォーム
        private RectTransform _rectTransform;

        // 機種ごとにセーフエリアを補正するイベント
        private Subject<Unit> _correctionSafeAreaSubject = new Subject<Unit>();

        public IObservable<Unit> CorrectionSafeArea => _correctionSafeAreaSubject.TakeUntilDestroy(this);

        private void Awake()
        {
            TryGetComponent(out _rectTransform);

            // 実行時にはセーフエリアパネルを透明にする
            if (Application.isPlaying)
            {
                Image image;
                TryGetComponent(out image);
                image.color = Color.clear;
            }
        }

        private void Start()
        {
            _correctionSafeAreaSubject.OnNext(Unit.Default);
        }

//#if UNITY_EDITOR
        private void Update()
        {
            _correctionSafeAreaSubject.OnNext(Unit.Default);
        }
//#endif

        /// <summary>
        /// 左上アンカーの値を設定
        /// </summary>
        /// <param name="anchorMin"> 設定したい左上アンカーの値</param>
        public void SetAnchorMin(in Vector2 anchorMin)
        {
            _rectTransform.anchorMin = anchorMin;
        }

        /// <summary>
        /// 右下アンカーの値を設定
        /// </summary>
        /// <param name="anchorMax"> 設定したい右下アンカーの値</param>
        public void SetAnchorMax(in Vector2 anchorMax)
        {
            _rectTransform.anchorMax = anchorMax;
        }
    }
}

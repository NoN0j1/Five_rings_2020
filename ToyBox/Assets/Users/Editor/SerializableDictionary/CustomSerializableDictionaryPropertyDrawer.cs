using UnityEditor;

namespace SerializableDictionary
{
    // インスペクタ上でDictionaryを編集できるようにする
    [CustomPropertyDrawer(typeof(GameScene.ViewManager.StatusChangeEffectViewDictionary))]
    [CustomPropertyDrawer(typeof(ScriptableObjects.EventTable.EventTable.ListByEventTagDictionary))]
    [CustomPropertyDrawer(typeof(ScriptableObjects.TargetData.TargetEventData.TargetParameterDictionary))]
    [CustomPropertyDrawer(typeof(ScriptableObjects.SelectionData.SelectionData.SelectionFluctuationValueDictionary))]
    public sealed class CustomSerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer
    {
    }

    [CustomPropertyDrawer(typeof(ScriptableObjects.EventTable.EventTable.EventDataListStorage))]
    public sealed class CustomSerializableDictionaryStoragePropertyDrawer : SerializableDictionaryStoragePropertyDrawer
    {
    }
}

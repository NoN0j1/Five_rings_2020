using System;
using UnityEngine;
using UniRx;
using VContainer;

namespace Tool
{
    /// <summary>
    /// データを作成するツールのPresenterクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateToolPresenter : IDisposable
    {
        private readonly ICreateToolModel _model;

        private readonly ICreateToolView _view;

        private CompositeDisposable _disposables = new CompositeDisposable();

        [Inject]
        public CreateToolPresenter(ICreateToolModel model, ICreateToolView view)
        {
            _model = model;

            _view = view;

            ConnectObserver();

            _model.Init();
        }

        /// <summary>
        /// オブザーバーをつなげる
        /// </summary>
        public void ConnectObserver()
        {
            _view.CreateNewData.Subscribe(author => _model.CreateNewData(author))
                .AddTo(_disposables);

            _view.Import.Subscribe(data => _model.Import(data))
                .AddTo(_disposables);

            _view.Export.Subscribe(viewData => _model.Export(viewData))
                .AddTo(_disposables);

            _view.Delete.Subscribe(data => _model.Delete(data))
                .AddTo(_disposables);

            _view.BatchRegistration.Subscribe(unit => _model.BatchRegistration())
                .AddTo(_disposables);

            _model.RefreshView.Where(data => !data.Equals(_view.ViewData))
                .Subscribe(data => _view.RefreshView(data))
                .AddTo(_disposables);

            _model.RefreshScrollView.DistinctUntilChanged()
                .Subscribe(list => _view.RefreshScrollView(list))
                .AddTo(_disposables);

            Debug.Log("接続");
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
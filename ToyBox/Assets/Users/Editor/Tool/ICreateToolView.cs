using System;
using System.Collections.Generic;
using ScriptableObjects;
using UniRx;

namespace Tool
{
    /// <summary>
    /// データを作成するツールのViewインターフェイス<br/>
    /// 制作者：宮川
    /// </summary>
    public interface ICreateToolView
    {
        public IData ViewData { get; }

        public IObservable<string> CreateNewData { get; }

        public IObservable<IData> Import { get; }

        public IObservable<IData> Export { get; }

        public IObservable<IData> Delete { get; }

        public IObservable<Unit> BatchRegistration { get; }

        public void RefreshView(in IData data);

        public void RefreshScrollView(in IEnumerable<IData> list);
    }

    /// <summary>
    /// 制作者名
    /// </summary>
    public enum Author
    {
        Kawano,
        Kitagawa,
        Miyagawa
    };
}

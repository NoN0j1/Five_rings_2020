using UnityEditor;
using VContainer;

namespace Tool.CreateSelectionTool
{
    /// <summary>
    /// 選択肢データを作成するツールのDIを行うクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateSelectionToolVContainer
    {
        public CreateSelectionToolVContainer()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();

            containerBuilder.Register<ICreateToolModel, CreateSelectionToolModel>(Lifetime.Scoped);

            containerBuilder.RegisterInstance<ICreateToolView, CreateSelectionToolView>(EditorWindow.GetWindow<CreateSelectionToolView>());

            containerBuilder.Register<CreateToolPresenter>(Lifetime.Scoped);

            IObjectResolver objectResolver = containerBuilder.Build();

            objectResolver.Resolve<CreateToolPresenter>();
        }
    }
}

using System;
using System.Collections.Generic;
using ScriptableObjects;
using ScriptableObjects.SelectionData;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Tool.CreateSelectionTool
{
    /// <summary>
    /// 選択肢データを作成するツールのViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateSelectionToolView : EditorWindow,ICreateToolView
    {
        // ウィンドウの最小サイズ
        private static readonly Vector2 _windowSizeMin = new Vector2(500, 500);

        // データ選択スクロールビューと新規イベントボタンの横幅
        private const int _selectViewLayoutWidth = 100;

        // データ表示エディタ
        private Editor _viewEditor = null;

        // 表示データ
        private SelectionData _viewData = null;

        // 選択中のデータ
        private SelectionData _selectData = null;

        // 選択肢データリスト
        private IEnumerable<SelectionData> _selectionDataList;

        // データ選択スクロールビュー用
        private Vector2 _selectScrollPos = Vector2.zero;

        // データ編集表示のスクロールビュー用
        private Vector2 _editScrollPos = Vector2.zero;

        // 制作者名
        private Author _author;

        // データを読み込むイベント
        private Subject<IData> _importSubject = new Subject<IData>();

        // データを保存するイベント
        private Subject<IData> _exportSubject = new Subject<IData>();

        // データを削除するイベント
        private Subject<IData> _deleteSubject = new Subject<IData>();

        // データを新規作成するイベント
        private Subject<string> _createNewDataSubject = new Subject<string>();

        // データを一括登録するイベント
        private Subject<Unit> _batchRegistrationSubject = new Subject<Unit>();

        public IData ViewData => _viewData;

        public IObservable<IData> Import => _importSubject;

        public IObservable<IData> Export => _exportSubject;

        public IObservable<IData> Delete => _deleteSubject;

        public IObservable<string> CreateNewData => _createNewDataSubject;

        public IObservable<Unit> BatchRegistration => _batchRegistrationSubject;

        /// <summary>
        /// 選択肢作成ツール用ウィンドウを作成
        /// </summary>
        [MenuItem("Editor/CreateSelectionTool")]
        private static void CreateWindow()
        {
            var window = GetWindow<CreateSelectionToolView>("選択肢作成ツール");

            window.minSize = _windowSizeMin;
        }

        private void OnEnable()
        {
            // VContainerを作成
            CreateSelectionToolVContainer createSelectionToolVContainer = new CreateSelectionToolVContainer();
            Debug.Log("VContainerを作成");
        }

        private void OnDestroy()
        {
            DestroyImmediate(_viewEditor);
        }

        private void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.VerticalScope(GUILayout.Width(_selectViewLayoutWidth)))
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        // データ一括登録ボタン
                        if (GUILayout.Button("一括登録"))
                        {
                            _batchRegistrationSubject.OnNext(Unit.Default);
                        }

                        // データ削除ボタン
                        if (GUILayout.Button("削除"))
                        {
                            if (_selectData == null)
                            {
                                Debug.LogError("データを選択してください");
                                return;
                            }

                            _deleteSubject.OnNext(_selectData);

                            _selectData = null;
                            _viewData = null;
                        }
                    }

                    EditorGUILayout.Space();

                    SelectScrollView();

                    EditorGUILayout.Space();

                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        GUILayout.Label("制作者");
                        _author = (Author)EditorGUILayout.EnumPopup(_author);

                        // データ新規作成ボタン
                        if (GUILayout.Button("新規作成"))
                        {
                            _createNewDataSubject.OnNext(_author.ToString());
                        }
                    }
                }

                using (new EditorGUILayout.VerticalScope())
                {
                    EditSelectionData();
                }
            }
        }

        /// <summary>
        /// データの表示を更新
        /// </summary>
        /// <param name="data"> データ</param>
        public void RefreshView(in IData data)
        {
            var inData = data.Object as SelectionData;

            if (inData == null)
            {
                Debug.LogError("データの表示を更新エラー");
            }

            if (_viewData == null)
            {
                _viewData = ScriptableObject.CreateInstance<SelectionData>();
            }

            DestroyImmediate(_viewEditor);

            // フォーカスを外す
            GUI.FocusControl(null);

            EditorUtility.CopySerialized(inData, _viewData);

            EditorGUILayout.EndScrollView();

            _selectData = inData;

            _viewEditor = Editor.CreateEditor(_viewData);

            Debug.Log("表示更新");
        }

        /// <summary>
        /// スクロールビューを更新
        /// </summary>
        /// <param name="list"> 選択肢リスト</param>
        public void RefreshScrollView(in IEnumerable<IData> list)
        {
            var dataList = list as IEnumerable<SelectionData>;
            if (dataList == null)
            {
                Debug.LogError("スクロールビュー更新エラー");
            }

            _selectionDataList = dataList;

            Debug.Log("スクロールビュー更新");
        }

        /// <summary>
        /// データの編集ビュー
        /// </summary>
        private void EditSelectionData()
        {
            if (_viewData == null)
            {
                return;
            }

            _editScrollPos = EditorGUILayout.BeginScrollView(_editScrollPos);

            _viewData.hideFlags = HideFlags.None;

            // デフォルトエディタを表示
            _viewEditor.DrawDefaultInspector();

            EditorGUILayout.EndScrollView();

            EditorGUILayout.Space();

            // データ書き込みボタン
            if (GUILayout.Button("書き込み"))
            {
                _exportSubject.OnNext(_viewData);
            }
        }

        /// <summary>
        /// データの選択スクロールビュー
        /// </summary>
        private void SelectScrollView()
        {
            if(ToolRepository.SelectDataScrollView(ref _selectData, _selectionDataList,ref _selectScrollPos))
            {
                _importSubject.OnNext(_selectData);

                Debug.Log($"{_selectData.DataName} : 選択");
            }
        }
    }
}

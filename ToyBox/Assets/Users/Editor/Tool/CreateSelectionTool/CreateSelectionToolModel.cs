using System;
using System.Collections.Generic;
using ScriptableObjects;
using ScriptableObjects.SelectionData;
using ScriptableObjects.SelectionList;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Tool.CreateSelectionTool
{
    /// <summary>
    /// 選択肢データを作成するツールのModelクラス<br/>
    /// データの新規作成,編集,削除,登録を行う<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateSelectionToolModel : ICreateToolModel
    {
        // 新しいデータの保存先フォルダのパス
        private const string _dataAssetDirectoryPath = @"Assets/Users/ScriptableObjects/SelectionData";

        // 現在選択中のデータのパス
        private string _assetPath = string.Empty;

        // 選択肢リストのアセットのパス
        private const string _selectionListPath = @"Assets/Users/ScriptableObjects/SelectionList/SelectionList.asset";

        // データ情報表示を更新するイベント
        private Subject<IData> _refreshViewSubject = new Subject<IData>();

        // データスクロールビューを更新するイベント
        private Subject<IEnumerable<IData>> _refreshScrollViewSubject = new Subject<IEnumerable<IData>>();

        public IObservable<IData> RefreshView => _refreshViewSubject;

        public IObservable<IEnumerable<IData>> RefreshScrollView => _refreshScrollViewSubject;

        /// <summary>
        /// 初期化
        /// </summary>
        public void Init()
        {
            RefreshDataList();
        }

        /// <summary>
        /// データを新規作成
        /// </summary>
        public void CreateNewData(in string author)
        {
            string fileName = CreateFileRepository.CreateSequentialNumberedFileName(nameof(SelectionData), _dataAssetDirectoryPath);

            _assetPath = $@"{_dataAssetDirectoryPath}/{author}{fileName}.asset";

            var data = ScriptableObject.CreateInstance<SelectionData>() as IData;

            var dataAsset = Export(in data);

            _refreshViewSubject.OnNext(dataAsset);

            Debug.Log("新規データ作成");

            RefreshDataList();
        }

        /// <summary>
        /// データを保存
        /// </summary>
        public IData Export(in IData viewData)
        {
            SelectionData data;
            ToolRepository.LoadAsset(out data, in _assetPath);

            CreateFileRepository.CreateAsset(data, _assetPath);

            EditorUtility.CopySerialized(viewData.Object, data);

            // 更新を通知
            ToolRepository.NotificationObjectUpdates(data);

            return data;
        }

        /// <summary>
        /// データを読み込み
        /// </summary>
        public void Import(in IData data)
        {
            _assetPath = AssetDatabase.GetAssetPath(data.Object);

            _refreshViewSubject.OnNext(data);

            Debug.Log(data.DataName + " : 読み込み\n" + _assetPath);
        }

        /// <summary>
        /// データを削除
        /// </summary>
        /// <param name="data"> 削除するデータ</param>
        public void Delete(in IData data)
        {
            var selectionData = data.Object as SelectionData;
            if (selectionData == null)
            {
                Debug.LogError("選択肢データ削除エラー");
                return;
            }

            // データアセット削除
            var assetPath = AssetDatabase.GetAssetPath(selectionData);
            AssetDatabase.DeleteAsset(assetPath);

            Debug.Log($"{data.DataName}を削除");

            RefreshDataList();
        }

        /// <summary>
        /// データの一括登録
        /// </summary>
        public void BatchRegistration()
        {
            var dataList = ToolRepository.CreateDataList<SelectionData>(nameof(SelectionData), _dataAssetDirectoryPath);

            List<string> nameList = new List<string>();

            string rename = string.Empty;
            string err = string.Empty;
            int index = 0;
            foreach (var data in dataList)
            {
                rename = $"{nameof(SelectionData)}_{index:d3}";

                var findAssets = AssetDatabase.FindAssets(rename, new[] { _dataAssetDirectoryPath });
                if (findAssets != null)
                {
                    foreach (var asset in findAssets)
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GUIDToAssetPath(asset), $"temp_{index:d3}");
                    }
                }

                err = AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(data), rename);
                if (err != string.Empty)
                {
                    Debug.LogError($"データ名変更エラー\n{err}");
                }
                index++;

                ToolRepository.NotificationObjectUpdates(data);

                // Addressableに登録
                AddressableRepository.RegisterDataAsset(data, DataAddressablesGroupName.SelectionDataGroupName);

                // ラベルを追加
                AddressableRepository.AddLabel(data.Object, nameof(SelectionData));

                nameList.Add(data.DataName);
            }

            SelectionList selectionList;

            ToolRepository.LoadAsset(out selectionList, _selectionListPath);

            CreateFileRepository.CreateAsset(selectionList, _selectionListPath);

            selectionList.SelectionNameList = nameList;

            ToolRepository.NotificationObjectUpdates(selectionList);

            Debug.Log("データ登録完了");

            RefreshDataList();
        }

        /// <summary>
        /// 表示データリストの更新
        /// </summary>
        private void RefreshDataList()
        {
            var dataList = ToolRepository.CreateDataList<SelectionData>(nameof(SelectionData), _dataAssetDirectoryPath);

            _refreshScrollViewSubject.OnNext(dataList);
        }
    }
}

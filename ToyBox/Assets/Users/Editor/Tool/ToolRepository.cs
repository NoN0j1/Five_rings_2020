using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace Tool
{
    /// <summary>
    /// データを作成するツールで使う機能をまとめたクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class ToolRepository
    {
        /// <summary>
        /// データ選択用スクロールビュー
        /// </summary>
        /// <typeparam name="T"> データの型</typeparam>
        /// <param name="selectData"> 選択されたデータ</param>
        /// <param name="dataList"> 選択されるデータ候補のリスト</param>
        /// <param name="scrollPos"> スクロールの位置</param>
        /// <param name="options"> スクロールビューのレイアウト</param>
        /// <returns> true : ボタンが押された</returns>
        public static bool SelectDataScrollView<T>(ref T selectData, in IEnumerable<T> dataList, ref Vector2 scrollPos, params GUILayoutOption[] options)
        {
            bool ret = false;

            string buttonText = string.Empty;

            IData iData = null;

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, EditorStyles.helpBox, options); 

            foreach (var data in dataList)
            {
                if (selectData != null)
                {
                    if (selectData.Equals(data))
                    {
                        GUI.backgroundColor = Color.yellow;
                    }
                }

                iData = data as IData;
                if(iData == null)
                {
                    buttonText = data.ToString();
                }
                else
                {
                    buttonText = iData.DataName;
                }

                if (GUILayout.Button(buttonText))
                {
                    selectData = data;

                    ret = true;
                }

                GUI.backgroundColor = Color.white;
            }

            EditorGUILayout.EndScrollView();

            return ret;
        }

        /// <summary>
        /// オブジェクトの変更を通知
        /// </summary>
        /// <param name="obj"> 変更を通知したいオブジェクト</param>
        public static void NotificationObjectUpdates(UnityEngine.Object obj)
        {
            // 更新通知
            EditorUtility.SetDirty(obj);

            // 保存
            AssetDatabase.SaveAssets();

            // エディタを最新の状態にする
            AssetDatabase.Refresh();
        }

        /// <summary>
        /// アセットを読み込む
        /// </summary>
        /// <param name="obj"> 読み込んだデータの保持用</param>
        /// <param name="assetPath"> 読み込むアセットのパス</param>
        public static void LoadAsset<T>(out T obj, in string assetPath) where T : ScriptableObject
        {
            obj = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (obj == null)
            {
                obj = ScriptableObject.CreateInstance<T>();
            }
        }

        /// <summary>
        /// フォルダからデータリストを作成
        /// </summary>
        /// <typeparam name="T"> データの型</typeparam>
        /// <param name="tagName"> タグ名</param>
        /// <param name="directoryName"> フォルダまでのパス</param>
        /// <returns> フォルダから作成されたデータリスト</returns>
        public static IEnumerable<T> CreateDataList<T>(in string tagName, in string directoryName) where T : ScriptableObject
        {
            var guidList = AssetDatabase.FindAssets($"{tagName}_???", new[] { directoryName });

            List<T> ret = new List<T>(guidList.Length);

            T data;
            foreach (var guid in guidList)
            {
                LoadAsset(out data, AssetDatabase.GUIDToAssetPath(guid));
                ret.Add(data);
            }

            // 文字列から整数値を抽出
            Func<string, int> Parse = (str) => int.Parse(Regex.Replace(str, @"[^0-9]", ""));

            // ファイル名の数値の昇順にソート
            ret.Sort((data1, data2) => Parse(data1.name) - Parse(data2.name));

            return ret;
        }
    }
}
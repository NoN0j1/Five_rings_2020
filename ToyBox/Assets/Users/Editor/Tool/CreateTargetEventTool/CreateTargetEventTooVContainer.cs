using UnityEditor;
using VContainer;

namespace Tool.CreateTargetEventTool
{
    /// <summary>
    /// 目標イベントデータを作成するツールのDIを行うクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateTargetEventTooVContainer
    {
        public CreateTargetEventTooVContainer()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();

            containerBuilder.Register<ICreateToolModel, CreateTargetEventToolModel>(Lifetime.Scoped);

            containerBuilder.RegisterInstance<ICreateToolView, CreateTargetEventToolView>(EditorWindow.GetWindow<CreateTargetEventToolView>());

            containerBuilder.Register<CreateToolPresenter>(Lifetime.Scoped);

            IObjectResolver objectResolver = containerBuilder.Build();

            objectResolver.Resolve<CreateToolPresenter>();
        }
    }
}

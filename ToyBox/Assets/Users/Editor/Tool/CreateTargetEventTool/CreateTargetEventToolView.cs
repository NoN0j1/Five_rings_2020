using System;
using System.Collections.Generic;
using ScriptableObjects;
using ScriptableObjects.TargetData;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Tool.CreateTargetEventTool
{
    /// <summary>
    /// 目標イベントデータを作成するツールのViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateTargetEventToolView : EditorWindow, ICreateToolView
    {
        // ウィンドウの最小サイズ
        private static readonly Vector2 _windowSizeMin = new Vector2(500, 500);

        // データ選択スクロールビューと新規イベントボタンの横幅
        private const int _selectViewLayoutWidth = 100;

        // データ表示エディタ
        private Editor _viewEditor = null;

        // 表示データ
        private TargetEventData _viewData = null;

        // 選択中のデータ
        private TargetEventData _selectData = null;

        // データリスト
        private IEnumerable<TargetEventData> _dataTable;
        
        // 編集スクロールビュー位置
        private Vector2 _editScrollPos = Vector2.zero;

        // データ選択スクロールビュー位置
        private Vector2 _selectDataScrollPos = Vector2.zero;

        // 制作者名
        private Author _author;

        // データ新規作成イベント
        private Subject<string> _createNewDataSubject = new Subject<string>();

        // データ読み込みイベント
        private Subject<IData> _importSubject = new Subject<IData>();
        
        // データ保存イベント
        private Subject<IData> _exportSubject = new Subject<IData>();

        // データを削除するイベント
        private Subject<IData> _deleteSubject = new Subject<IData>();

        // データを一括登録するイベント
        private Subject<Unit> _batchRegistrationSubject = new Subject<Unit>();

        public IData ViewData => _viewData;

        public IObservable<string> CreateNewData => _createNewDataSubject;

        public IObservable<IData> Import => _importSubject;

        public IObservable<IData> Export => _exportSubject;

        public IObservable<IData> Delete => _deleteSubject;

        public IObservable<Unit> BatchRegistration => _batchRegistrationSubject;

        /// <summary>
        /// 目標イベント作成ツール用ウィンドウを作成
        /// </summary>
        [MenuItem("Editor/CreateTargetEventTool")]
        private static void CreateWindow()
        {
            var window = GetWindow<CreateTargetEventToolView>("目標イベント作成ツール");

            window.minSize = _windowSizeMin;
        }

        private void OnEnable()
        {
            // VContainerを作成
            CreateTargetEventTooVContainer createTargetEventTooVContainer = new CreateTargetEventTooVContainer();
            Debug.Log("VContainerを作成");
        }

        private void OnDestroy()
        {
            DestroyImmediate(_viewEditor);
        }

        private void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.VerticalScope(GUILayout.Width(_selectViewLayoutWidth)))
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        // データ一括登録ボタン
                        if (GUILayout.Button("一括登録"))
                        {
                            _batchRegistrationSubject.OnNext(Unit.Default);
                        }

                        EditorGUILayout.Space();

                        // データ削除ボタン
                        if (GUILayout.Button("削除"))
                        {
                            if (_selectData == null)
                            {
                                Debug.LogError("データを選択してください");
                                return;
                            }

                            _deleteSubject.OnNext(_selectData);

                            _selectData = null;
                            _viewData = null;
                        }
                    }

                    EditorGUILayout.Space();

                    DataScrollView();

                    EditorGUILayout.Space();

                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        GUILayout.Label("制作者");
                        _author = (Author)EditorGUILayout.EnumPopup(_author);

                        // データ新規作成ボタン
                        if (GUILayout.Button("新規作成"))
                        {
                            _createNewDataSubject.OnNext(_author.ToString());
                        }
                    }
                }

                using (new EditorGUILayout.VerticalScope())
                {
                    EditData();
                }
            }
        }

        /// <summary>
        /// データ選択スクロールビュー更新
        /// </summary>
        /// <param name="list"> リスト</param>
        public void RefreshScrollView(in IEnumerable<IData> list)
        {
            var dataList = list as IEnumerable<TargetEventData>;
            if (dataList == null)
            {
                Debug.LogError("スクロールビュー更新エラー");
            }

            _dataTable = dataList;

            Debug.Log("スクロールビュー更新");
        }

        /// <summary>
        /// データの表示を更新
        /// </summary>
        /// <param name="data"> 更新データ</param>
        public void RefreshView(in IData data)
        {
            var inData = data.Object as TargetEventData;

            if (inData == null)
            {
                Debug.LogError("データの表示を更新エラー");
            }

            if (_viewData == null)
            {
                _viewData = ScriptableObject.CreateInstance<TargetEventData>();
            }

            DestroyImmediate(_viewEditor);

            // フォーカスを外す
            GUI.FocusControl(null);

            EditorUtility.CopySerialized(inData, _viewData);

            _selectData = inData;

            _viewEditor = Editor.CreateEditor(_viewData);

            Debug.Log("表示更新");
        }

        /// <summary>
        /// データ編集
        /// </summary>
        private void EditData()
        {
            if (_viewData == null)
            {
                return;
            }

            _editScrollPos = EditorGUILayout.BeginScrollView(_editScrollPos);

            _viewData.hideFlags = HideFlags.None;

            // デフォルトエディタを表示
            _viewEditor.DrawDefaultInspector();

            EditorGUILayout.EndScrollView();

            EditorGUILayout.Space();

            // イベントデータ書き込みボタン
            if (GUILayout.Button("書き込み"))
            {
                _exportSubject.OnNext(_viewData);
            }
        }

        /// <summary>
        /// データ選択スクロールビュー
        /// </summary>
        private void DataScrollView()
        {
            if (ToolRepository.SelectDataScrollView(ref _selectData, _dataTable, ref _selectDataScrollPos))
            {
                // イベントデータ読み込み
                _importSubject.OnNext(_selectData);

                Debug.Log(_selectData.DataName + " : 読み込み");
            }
        }
    }
}

using System;
using System.Collections.Generic;
using ScriptableObjects;
using ScriptableObjects.TargetData;
using ScriptableObjects.TargetEventList;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Tool.CreateTargetEventTool
{
    /// <summary>
    /// 目標イベントデータを作成するツールのModelクラス<br/>
    /// データの新規作成,編集,削除,登録を行う<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateTargetEventToolModel : ICreateToolModel
    {
        // 新しいデータの保存先フォルダのパス
        private const string _dataAssetDirectoryPath = @"Assets/Users/ScriptableObjects/TargetEventData";

        // 現在選択中のデータのパス
        private string _assetPath = string.Empty;

        // 全選択肢のリストアセットのパス
        private const string _targetEventListAssetPath = @"Assets/Users/ScriptableObjects/TargetEventList/TargetEventList.asset";

        // 表示データ更新イベント
        private Subject<IData> _refreshViewSubject = new Subject<IData>();

        // データ選択スクロールビュー更新イベント
        private Subject<IEnumerable<IData>> _refreshScrollViewSubject = new Subject<IEnumerable<IData>>();

        public IObservable<IData> RefreshView => _refreshViewSubject;

        public IObservable<IEnumerable<IData>> RefreshScrollView => _refreshScrollViewSubject;

        /// <summary>
        /// 初期化
        /// </summary>
        public void Init()
        {
            RefreshDataList();
        }

        /// <summary>
        /// データを新規作成
        /// </summary>
        /// <param name="author"> 制作者名</param>
        public void CreateNewData(in string author)
        {
            string fileName = CreateFileRepository.CreateSequentialNumberedFileName(nameof(TargetEventData), _dataAssetDirectoryPath);

            _assetPath = $@"{_dataAssetDirectoryPath}/{author}{fileName}.asset";

            var data = ScriptableObject.CreateInstance<TargetEventData>() as IData;

            var dataAsset = Export(in data);

            _refreshViewSubject.OnNext(dataAsset);

            Debug.Log("新規データ作成");

            RefreshDataList();
        }

        /// <summary>
        /// データの読み込み
        /// </summary>
        /// <param name="data"> 読み込むデータ</param>
        public void Import(in IData data)
        {
            _assetPath = AssetDatabase.GetAssetPath(data.Object);

            _refreshViewSubject.OnNext(data);

            Debug.Log(data.DataName + " : 読み込み\n" + _assetPath);
        }

        /// <summary>
        /// データの保存
        /// </summary>
        /// <param name="viewData"> 保存したいデータ</param>
        /// <returns> 保存したデータ</returns>
        public IData Export(in IData viewData)
        {
            TargetEventData data;
            ToolRepository.LoadAsset(out data, in _assetPath);

            CreateFileRepository.CreateAsset(data, _assetPath);

            EditorUtility.CopySerialized(viewData.Object, data);

            // 更新を通知
            ToolRepository.NotificationObjectUpdates(data);

            return data;
        }

        /// <summary>
        /// データを削除
        /// </summary>
        /// <param name="data"> 削除するデータ</param>
        public void Delete(in IData data)
        {
            var targetEventData = data.Object as TargetEventData;
            if (targetEventData == null)
            {
                Debug.LogError("目標イベントデータ削除エラー");
                return;
            }

            // データアセット削除
            var assetPath = AssetDatabase.GetAssetPath(targetEventData);
            AssetDatabase.DeleteAsset(assetPath);

            Debug.Log($"{data.DataName}を削除");

            RefreshDataList();
        }

        /// <summary>
        /// データの一括登録
        /// </summary>
        public void BatchRegistration()
        {
            var dataList = ToolRepository.CreateDataList<TargetEventData>(nameof(TargetEventData), _dataAssetDirectoryPath);

            List<string> nameList = new List<string>();

            string rename = string.Empty;
            string err = string.Empty;
            int index = 0;
            foreach (var data in dataList)
            {
                rename = $"{nameof(TargetEventData)}_{index:d3}";

                var findAssets = AssetDatabase.FindAssets(rename, new[] { _dataAssetDirectoryPath });
                if (findAssets != null)
                {
                    foreach (var asset in findAssets)
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GUIDToAssetPath(asset), $"temp_{index:d3}");
                    }
                }

                err = AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(data), rename);
                if (err != string.Empty)
                {
                    Debug.LogError($"データ名変更エラー\n{err}");
                }

                index++;

                ToolRepository.NotificationObjectUpdates(data);

                // Addressableに登録
                AddressableRepository.RegisterDataAsset(data, DataAddressablesGroupName.TargetEventDataGroup);

                // ラベルを追加
                AddressableRepository.AddLabel(data.Object, nameof(TargetEventData));

                nameList.Add(data.DataName);
            }

            TargetEventList targetEventList;

            ToolRepository.LoadAsset(out targetEventList, _targetEventListAssetPath);

            CreateFileRepository.CreateAsset(targetEventList, _targetEventListAssetPath);

            targetEventList.TargetEventNameList = nameList;

            ToolRepository.NotificationObjectUpdates(targetEventList);

            Debug.Log("データ登録完了");

            RefreshDataList();
        }

        /// <summary>
        /// 表示データリストの更新
        /// </summary>
        private void RefreshDataList()
        {
            var dataList = ToolRepository.CreateDataList<TargetEventData>(nameof(TargetEventData), _dataAssetDirectoryPath);

            _refreshScrollViewSubject.OnNext(dataList);
        }
    }
}

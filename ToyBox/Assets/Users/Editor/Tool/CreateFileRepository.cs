using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Tool
{
    /// <summary>
    /// データファイルを作成する機能をまとめたクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateFileRepository
    {
        /// <summary>
        /// 連番のファイル名を作成
        /// </summary>
        /// <param name="tagName"> 連番にしたいファイル名</param>
        /// <param name="directoryName"> フォルダまでのパス</param>
        /// <returns> 連番のファイル名</returns>
        public static string CreateSequentialNumberedFileName(in string tagName,in string directoryName)
        {
            var directoryInfo = new DirectoryInfo(Path.GetFullPath(directoryName));

            var max = directoryInfo.GetFiles($"*{tagName}_???.asset")
                .Select(file => Regex.Match(file.Name, @"(?i)_(\d{3})"))
                .Where(m => m.Success)
                .Select(m => int.Parse(m.Groups[1].Value))
                .DefaultIfEmpty(0)
                .Max();

            return $"{tagName}_{max + 1:d3}";
        }

        /// <summary>
        /// フォルダを作成
        /// </summary>
        /// <param name="path"> パス</param>
        public static void CreateDirectory(in string path)
        {
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        /// <summary>
        /// アセットを作成
        /// </summary>
        /// <param name="data"> アセットにするデータ</param>
        /// <param name="assetPath"> 作成先のパス</param>
        public static void CreateAsset(in Object data, in string assetPath)
        {
            // dataがアセットであるかチェック
            if (!AssetDatabase.Contains(data))
            {
                // フォルダチェック
                string directory = Path.GetDirectoryName(assetPath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                // アセット作成
                AssetDatabase.CreateAsset(data, assetPath);
            }
        }
    }
}

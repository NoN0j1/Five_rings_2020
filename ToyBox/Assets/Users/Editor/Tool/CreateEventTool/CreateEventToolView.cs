using System;
using System.Collections.Generic;
using System.Linq;
using Extension;
using ScriptableObjects.EventData;
using UnityEditor;
using UnityEngine;
using UniRx;
using ScriptableObjects;

namespace Tool.CreateEventTool
{
    /// <summary>
    /// イベントデータを作成するツールのViewクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateEventToolView : EditorWindow, ICreateToolView
    {
        // ウィンドウの最小サイズ
        private static readonly Vector2 _windowSizeMin = new Vector2(500, 500);

        // データ選択スクロールビューと新規イベントボタンの横幅
        private const int _selectViewLayoutWidth = 100;

        // イベントデータ表示情報
        private EventData _viewData = null;

        // データ表示エディタ
        private Editor _viewEditor = null;

        // イベントデータを新規作成するイベント
        private Subject<string> _createNewDataSubject = new Subject<string>();

        // イベントデータを読み込むイベント
        private Subject<IData> _importSubject = new Subject<IData>();

        // イベントデータを保存するイベント
        private Subject<IData> _exportSubject = new Subject<IData>();

        // イベントデータを削除するイベント
        private Subject<IData> _deleteSubject = new Subject<IData>();

        // イベントデータを一括登録するイベント
        private Subject<Unit> _batchRegistrationSubject = new Subject<Unit>();

        // イベントデータをスクロールビューで表示する用
        private IEnumerable<EventData> _eventTable = null;

        // 選択中のイベントデータ
        private EventData _selectEventData = null;

        // データ選択スクロールビューのスクロール現在位置
        private Vector2 _selectDataScrollPos = Vector2.zero;

        // データ編集スクロールビューのスクロール現在位置
        private Vector2 _editScrollPos = Vector2.zero;

        // 絞り込み用イベントタグ
        private EventTag _selectEventTag = EventTag.None;

        // 制作者名
        private Author _author;

        public IData ViewData => _viewData;

        public IObservable<string> CreateNewData => _createNewDataSubject;

        public IObservable<IData> Import => _importSubject;

        public IObservable<IData> Export => _exportSubject;

        public IObservable<IData> Delete => _deleteSubject;

        public IObservable<Unit> BatchRegistration => _batchRegistrationSubject;

        /// <summary>
        /// イベント作成ツール用ウィンドウを作成
        /// </summary>
        [MenuItem("Editor/CreateEventTool")]
        private static void CreateWindow()
        {
            var window = GetWindow<CreateEventToolView>("イベント作成ツール");

            window.minSize = _windowSizeMin;
        }

        private void OnEnable()
        {
            // VContainerを作成
            CreateEventToolVContainer createEventToolVContainer = new CreateEventToolVContainer();
            Debug.Log("VContainerを作成");
        }

        private void OnDestroy()
        {
            DestroyImmediate(_viewEditor);
        }

        private void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                using (new EditorGUILayout.VerticalScope(GUILayout.Width(_selectViewLayoutWidth)))
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        // イベントデータ一括登録ボタン
                        if (GUILayout.Button("一括登録"))
                        {
                            _batchRegistrationSubject.OnNext(Unit.Default);
                        }

                        EditorGUILayout.Space();

                        // イベントデータ削除ボタン
                        if (GUILayout.Button("削除"))
                        {
                            if (_selectEventData == null)
                            {
                                Debug.LogError("データを選択してください");
                                return;
                            }

                            _deleteSubject.OnNext(_selectEventData);

                            _selectEventData = null;
                            _viewData = null;
                        }
                    }

                    EditorGUILayout.Space();

                    EventDataScrollView();

                    EditorGUILayout.Space();

                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        GUILayout.Label("制作者");
                        _author = (Author)EditorGUILayout.EnumPopup(_author);

                        // イベントデータ新規作成ボタン
                        if (GUILayout.Button("新規作成"))
                        {
                            _createNewDataSubject.OnNext(_author.ToString());
                        }
                    }
                }

                using (new EditorGUILayout.VerticalScope())
                {
                    EditData();
                }
            }
        }

        /// <summary>
        /// イベントデータの表示を更新
        /// </summary>
        /// <param name="data"> イベントデータ</param>
        public void RefreshView(in IData data)
        {
            var inData = data.Object as EventData;

            if (inData == null)
            {
                Debug.LogError("データの表示を更新エラー");
            }

            if (_viewData == null)
            {
                _viewData = ScriptableObject.CreateInstance<EventData>();
            }

            DestroyImmediate(_viewEditor);

            // フォーカスを外す
            GUI.FocusControl(null);

            EditorUtility.CopySerialized(inData, _viewData);

            _selectEventData = inData;

            _viewEditor = Editor.CreateEditor(_viewData);

            Debug.Log("表示更新");
        }

        /// <summary>
        /// スクロールビューを更新
        /// </summary>
        /// <param name="list"> イベントリスト</param>
        public void RefreshScrollView(in IEnumerable<IData> list)
        {
            var dataList = list as IEnumerable<EventData>;
            if (dataList == null)
            {
                Debug.LogError("スクロールビュー更新エラー");
            }

            _eventTable = dataList;

            Debug.Log("スクロールビュー更新");
        }

        /// <summary>
        /// データ編集ビュー
        /// </summary>
        private void EditData()
        {
            if (_viewData == null)
            {
                return;
            }

            _editScrollPos = EditorGUILayout.BeginScrollView(_editScrollPos);

            _viewData.hideFlags = HideFlags.None;

            // デフォルトエディタを表示
            _viewEditor.DrawDefaultInspector();

            EditorGUILayout.EndScrollView();

            EditorGUILayout.Space();

            // イベントデータ書き込みボタン
            if (GUILayout.Button("書き込み"))
            {
                _exportSubject.OnNext(_viewData);
            }
        }

        /// <summary>
        /// イベントデータリストをスクロールビューで表示
        /// </summary>
        private void EventDataScrollView()
        {
            GUILayout.Label("絞り込みタグ");
            _selectEventTag = (EventTag)EditorGUILayout.EnumPopup(_selectEventTag);

            EditorGUILayout.Space();

            var narrowedList = _eventTable;
            if (_selectEventTag != EventTag.None)
            {
                narrowedList = _eventTable.Where(data => data.Tag.Included(_selectEventTag));
            }

            if (ToolRepository.SelectDataScrollView(ref _selectEventData, narrowedList, ref _selectDataScrollPos))
            {
                // イベントデータ読み込み
                _importSubject.OnNext(_selectEventData);

                Debug.Log(_selectEventData.EventName + " : 読み込み");
            }
        }
    }
}
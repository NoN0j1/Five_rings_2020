using UnityEditor;
using VContainer;

namespace Tool.CreateEventTool
{
    /// <summary>
    /// イベントデータを作成するツールのDIを行うクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateEventToolVContainer
    {
        public CreateEventToolVContainer()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();

            containerBuilder.Register<ICreateToolModel, CreateEventToolModel>(Lifetime.Scoped);

            containerBuilder.RegisterInstance<ICreateToolView, CreateEventToolView>(EditorWindow.GetWindow<CreateEventToolView>());

            containerBuilder.Register<CreateToolPresenter>(Lifetime.Scoped);

            IObjectResolver objectResolver = containerBuilder.Build();

            objectResolver.Resolve<CreateToolPresenter>();
        }
    }
}
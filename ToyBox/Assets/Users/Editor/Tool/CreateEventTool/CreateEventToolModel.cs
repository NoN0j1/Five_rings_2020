using System;
using System.Collections.Generic;
using ScriptableObjects.EventData;
using Extension;
using UnityEditor;
using UnityEngine;
using UniRx;
using ScriptableObjects;
using ScriptableObjects.EventTable;

namespace Tool.CreateEventTool
{
    /// <summary>
    /// イベントデータを作成するツールのModelクラス<br/>
    /// データの新規作成,編集,削除,登録を行う<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class CreateEventToolModel : ICreateToolModel
    {
        // 新しいデータの保存先フォルダのパス
        private const string _dataAssetDirectoryPath = @"Assets/Users/ScriptableObjects/EventData";

        // 現在選択中のデータのパス
        private string _assetPath;

        // イベントデータリストのアセットのパス
        private const string _eventTablePath = @"Assets/Users/ScriptableObjects/EventTable/EventTable.asset";

        // イベントデータリスト
        private EventTable _eventTable;

        // イベントデータ情報表示を更新するイベント
        private Subject<IData> _refreshViewSubject = new Subject<IData>();

        // イベントデータスクロールビューを更新するイベント
        private Subject<IEnumerable<IData>> _refreshScrollViewSubject = new Subject<IEnumerable<IData>>();

        public IObservable<IData> RefreshView => _refreshViewSubject;

        public IObservable<IEnumerable<IData>> RefreshScrollView => _refreshScrollViewSubject;

        /// <summary>
        /// 初期化
        /// </summary>
        public void Init()
        {
            ToolRepository.LoadAsset(out _eventTable, _eventTablePath);

            CreateFileRepository.CreateAsset(_eventTable, _eventTablePath);

            RefreshDataList();
        }

        /// <summary>
        /// イベントデータを新規作成
        /// </summary>
        /// <param name="author"> 制作者名</param>
        public void CreateNewData(in string author)
        {
            string fileName = CreateFileRepository.CreateSequentialNumberedFileName(nameof(EventData), _dataAssetDirectoryPath);

            _assetPath = $@"{_dataAssetDirectoryPath}/{author}{fileName}.asset";

            var data = ScriptableObject.CreateInstance<EventData>() as IData;

            var dataAsset = Export(in data);

            _refreshViewSubject.OnNext(dataAsset);

            RefreshDataList();

            Debug.Log("新規データ作成");
        }

        /// <summary>
        /// イベントデータを読み込み
        /// </summary>
        /// <param name="data"> 読み込むデータ</param>
        public void Import(in IData data)
        {
            _assetPath = AssetDatabase.GetAssetPath(data.Object);

            _refreshViewSubject.OnNext(data);

            Debug.Log(data.DataName + " : 読み込み\n" + _assetPath);
        }

        /// <summary>
        /// イベントデータを保存
        /// </summary>
        /// <param name="viewData"> 保存したいデータ</param>
        /// <returns> 保存したデータ</returns>
        public IData Export(in IData viewData)
        {
            EventData eventData;
            ToolRepository.LoadAsset(out eventData, in _assetPath);

            // アセット作成
            CreateFileRepository.CreateAsset(eventData, _assetPath);

            EditorUtility.CopySerialized(viewData.Object, eventData);

            // 更新を通知
            ToolRepository.NotificationObjectUpdates(eventData);

            return eventData;
        }

        /// <summary>
        /// イベントデータを削除
        /// </summary>
        /// <param name="data"> 削除するイベントデータ</param>
        public void Delete(in IData data)
        {
            var eventData = data.Object as EventData;
            if(eventData == null)
            {
                Debug.LogError("イベントデータ削除エラー");
                return;
            }

            // データアセット削除
            var assetPath = AssetDatabase.GetAssetPath(eventData);
            AssetDatabase.DeleteAsset(assetPath);

            Debug.Log($"{data.DataName}を削除");

            RefreshDataList();
        }

        /// <summary>
        /// イベントデータを一括登録
        /// </summary>
        public void BatchRegistration()
        {
            // タグ別のリストをクリア
            _eventTable.ClearListByTag();

            // フォルダからイベントデータリストを取得
            var dataList = ToolRepository.CreateDataList<EventData>(nameof(EventData), _dataAssetDirectoryPath);

            List<string> nameList = new List<string>();

            string rename = string.Empty;
            string err = string.Empty;
            int index = 0;
            foreach (var data in dataList)
            {
                // イベントデータのファイル名を整理
                rename = $"{nameof(EventData)}_{index:d3}";

                var findAssets = AssetDatabase.FindAssets(rename, new[] { _dataAssetDirectoryPath });
                if(findAssets != null)
                {
                    foreach (var asset in findAssets)
                    {
                        AssetDatabase.RenameAsset(AssetDatabase.GUIDToAssetPath(asset), $"temp_{index:d3}");
                    }
                }

                err = AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(data), rename);
                if (err != string.Empty)
                {
                    Debug.LogError($"データ名変更エラー\n{err}");
                }

                index++;

                ToolRepository.NotificationObjectUpdates(data);

                // Addressableに登録
                AddressableRepository.RegisterDataAsset(data, DataAddressablesGroupName.EventDataGroupName);

                // ラベルを追加
                AddressableRepository.AddLabel(data.Object, nameof(EventData));

                foreach (var list in _eventTable.ListByTag)
                {
                    if (data.Tag.Included(list.Key))
                    {
                        AddressableRepository.AddLabel(data.Object, list.Key.ToString());
                    }
                }

                // イベントデータをタグ別のリストに登録
                RegistrationListByTag(data);

                nameList.Add(data.DataName);
            }

            _eventTable.EventNameList = nameList;

            ToolRepository.NotificationObjectUpdates(_eventTable);

            Debug.Log("データ登録完了");

            RefreshDataList();
        }

        /// <summary>
        /// イベントデータをタグ別のリストに登録
        /// </summary>
        /// <param name="data"> 登録するイベントデータ</param>
        private void RegistrationListByTag(in EventData data)
        {
            foreach (var list in _eventTable.ListByTag)
            {
                if (data.Tag.Included(list.Key))
                {
                    list.Value.Add(data);
                }
            }
        }

        /// <summary>
        /// 表示データリストの更新
        /// </summary>
        private void RefreshDataList()
        {
            var dataList = ToolRepository.CreateDataList<EventData>(nameof(EventData), _dataAssetDirectoryPath);

            _refreshScrollViewSubject.OnNext(dataList);
        }
    }
}
using ScriptableObjects;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace Tool
{
    /// <summary>
    /// データをAddressableに登録する機能をまとめたクラス<br/>
    /// 制作者：宮川
    /// </summary>
    public sealed class AddressableRepository
    {
        /// <summary>
        /// データアセットをAddressableに登録
        /// </summary>
        /// <param name="data"> 登録するデータ</param>
        /// <param name="groupName"> グループ名</param>
        public static void RegisterDataAsset(in IData data, in string groupName)
        {
            // Addressableの設定を読み込む
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            // グループの指定
            var group = settings.FindGroup(groupName);
            if (group == null)
            {
                // グループがなかったらグループを作成する
                group = CreateGroup(groupName);
            }

            // データアセットのパスを取得
            var assetPath = AssetDatabase.GetAssetPath(data.Object);

            // パスからGUIDを取得
            var guid = AssetDatabase.AssetPathToGUID(assetPath);

            // GUIDを指定のグループに登録しEntryを作成
            var entry = settings.CreateOrMoveEntry(guid, group);

            // ロードする際に使う名前を登録
            entry.SetAddress(data.DataName);

            // 変更を保存する
            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// グループを作成
        /// </summary>
        /// <param name="groupName"> グループ名</param>
        /// <returns> 作成したグループ</returns>
        public static AddressableAssetGroup CreateGroup(in string groupName)
        {
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            // AddressableAssetGroupTemplateを取得
            var groupTemplate = settings.GetGroupTemplateObject(0) as AddressableAssetGroupTemplate;

            // グループを作成
            AddressableAssetGroup newGroup = settings.CreateGroup(groupName, false, false, true, null, groupTemplate.GetTypes());
            groupTemplate.ApplyToAddressableAssetGroup(newGroup);

            // 変更を保存
            AssetDatabase.SaveAssets();

            return newGroup;
        }

        /// <summary>
        /// エントリにラベルを追加
        /// </summary>
        /// <param name="obj"> 追加対象</param>
        /// <param name="label"> 追加したいラベル</param>
        public static void AddLabel(in Object obj,in string label)
        {
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            // データアセットのパスを取得
            var assetPath = AssetDatabase.GetAssetPath(obj);

            // パスからGUIDを取得
            var guid = AssetDatabase.AssetPathToGUID(assetPath);

            // エントリを取得
            var entry = settings.FindAssetEntry(guid);

            // エントリにラベルを追加
            entry.SetLabel(label, true, true);

            // 変更を保存
            AssetDatabase.SaveAssets();
        }
    }
}

using System;
using System.Collections.Generic;
using ScriptableObjects;

namespace Tool
{
    /// <summary>
    /// データを作成するツールのModelインターフェイス<br/>
    /// 制作者：宮川
    /// </summary>
    public interface ICreateToolModel
    {
        public IObservable<IData> RefreshView { get; }

        public IObservable<IEnumerable<IData>> RefreshScrollView { get; }

        public void Init();

        public void CreateNewData(in string author);

        public void Import(in IData data);

        public IData Export(in IData viewData);

        public void Delete(in IData data);

        public void BatchRegistration();
    }
}

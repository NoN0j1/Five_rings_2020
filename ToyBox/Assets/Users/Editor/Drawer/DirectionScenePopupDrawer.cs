using System.Collections.Generic;
using Attribute;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEngine;

namespace Drawer
{
    /// <summary>
    /// 演出シーンをインスペクタ上でポップアップで選択できるようにするクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [CustomPropertyDrawer(typeof(DirectionScenePopupAttribute))]
    public sealed class DirectionScenePopupDrawer : PropertyDrawer
    {
        // 選択しているポップアップの番号
        private int _selectedIndex = 0;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // ラベル表示
            DirectionScenePopupAttribute popupAttribute = (DirectionScenePopupAttribute)attribute;
            EditorGUI.LabelField(
                new Rect(
                    position.x,
                    position.y,
                    position.width,
                    EditorGUIUtility.singleLineHeight
                    ),
                popupAttribute.Label,
                EditorStyles.boldLabel
                );

            position.y += EditorGUIUtility.singleLineHeight;

            // Addressableの設定を読み込む
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            var group = settings.FindGroup(popupAttribute.GroupName);

            var entries = group.entries;

            // エントリ名のリストを作成
            List<string> entryName = new List<string>(entries.Count);
            foreach(var entry in entries)
            {
                entryName.Add(entry.address);
            }

            if (property.stringValue != string.Empty)
            {
                _selectedIndex = entryName.FindIndex(name => name == property.stringValue);
                if(_selectedIndex == -1)
                {
                    _selectedIndex = 0;
                }
            }

            // エントリ名のポップアップ
            EditorGUI.LabelField(
                new Rect(
                    position.x,
                    position.y,
                    position.width,
                    EditorGUIUtility.singleLineHeight
                    ),
                property.stringValue,
                EditorStyles.boldLabel
                );

            _selectedIndex = EditorGUI.Popup(
                new Rect(
                    EditorGUIUtility.labelWidth,
                    position.y,
                    position.width,
                    EditorGUIUtility.singleLineHeight
                    ),
                _selectedIndex,
                entryName.ToArray()
                );

            property.stringValue = entryName[_selectedIndex];
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + EditorGUIUtility.singleLineHeight;
        }
    }
}

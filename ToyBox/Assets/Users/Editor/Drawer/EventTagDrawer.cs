using System;
using Attribute;
using Extension;
using ScriptableObjects.EventData;
using UnityEngine;
using UnityEditor;

namespace Drawer
{
    /// <summary>
    /// イベントタグをインスペクタ上でビットフィールドで選択できるようにするクラス<br/>
    /// 制作者：宮川
    /// </summary>
    [CustomPropertyDrawer(typeof(EventTagAttribute))]
    public sealed class EventTagDrawer : PropertyDrawer
    {
        // 含まれているイベントタグ表示領域の1文字ごとの横幅
        private const int _displayWidth = 20;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // イベントタグの編集
            var enumValue = (EventTag)property.intValue;

            EditorGUI.LabelField(
                new Rect(
                    position.x,
                    position.y,
                    position.width,
                    EditorGUIUtility.singleLineHeight
                    ),
                "EventTag",
                EditorStyles.boldLabel
                );
            enumValue = (EventTag)EditorGUI.EnumFlagsField(
                new Rect(
                    EditorGUIUtility.labelWidth,
                    position.y,
                    position.width - EditorGUIUtility.labelWidth,
                    EditorGUIUtility.singleLineHeight
                    ),
                enumValue
                );

            property.intValue = (int)enumValue;

            position.y += EditorGUIUtility.singleLineHeight;

            // 含まれているイベントタグの表示
            EditorGUI.LabelField(
                new Rect(
                    position.x,
                    position.y,
                    position.width,
                    EditorGUIUtility.singleLineHeight
                ),
                "含まれているタグ",
                EditorStyles.helpBox
                );

            float offsetX = 0.0f;
            var EventTagList = Enum.GetValues(typeof(EventTag));
            foreach (EventTag eventTag in EventTagList)
            {
                DrawIncludedEventTag(enumValue, eventTag, position,ref offsetX);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + EditorGUIUtility.singleLineHeight;
        }

        /// <summary>
        /// イベントタグが含まれていたら表示
        /// </summary>
        /// <param name="enumValue"> チェック対象Enum</param>
        /// <param name="eventTag"> このイベントタグが含まれているかチェック</param>
        /// <param name="position"> レイアウト情報</param>
        /// <param name="offsetX"> タグ表示オフセット</param>
        private void DrawIncludedEventTag(in EventTag enumValue, in EventTag eventTag, in Rect position, ref float offsetX)
        {
            if (enumValue.Included(eventTag))
            {
                var tagDisplayName = eventTag.GetDisplayName();
                EditorGUI.LabelField(
                    new Rect(
                        EditorGUIUtility.labelWidth + offsetX,
                        position.y,
                        _displayWidth * tagDisplayName.Length,
                        EditorGUIUtility.singleLineHeight
                    ),
                    tagDisplayName
                    );

                offsetX += _displayWidth * tagDisplayName.Length;
            }
        }
    }
}
